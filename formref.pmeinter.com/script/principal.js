jQuery(document).ready(function() {
	jQuery('form .date').datepicker({ dateFormat: 'yy-mm-dd', minDate: 0, onSelect: function(date) {
        	$(this).nextAll('input, textarea, select').first().focus();
        }
    });
	jQuery(".tel").mask("(999) 999-9999");

	jQuery('.autocomt_employe').autocomplete({
		source: '/ajax/employes',
		minLength: 0,
		select: function(event, ui ) {
			var $input = jQuery(event.target);

			jQuery('#' + $input.attr('data-ref')).val(ui.item.id).trigger('change');

			$input.val(ui.item.label);

			return false;
		}
	}).on('click', function(){
		 $(this).autocomplete('search', '');
	});

	jQuery('.autocomt_etude').autocomplete({
		source: '/ajax/etudes',
		minLength: 0,
		select: function(event, ui ) {
			var $input = jQuery(event.target);

			jQuery('#' + $input.attr('data-ref')).val(ui.item.id).trigger('change');

			$input.val(ui.item.label);

			return false;
		}
	}).on('click', function(){
		 $(this).autocomplete('search', '');
	});

	// $("#formRef").validate();
});

jQuery(document).on('change', '#iRefA_Etude', function() {
	if(jQuery(this).val() > 0)
	{
		jQuery.get('/ajax/employes_etude/' + jQuery(this).val(), null, function(data) {
			jQuery('#iRefA_Employe option[value]').detach();
			jQuery('#iRefA_EmployeCourriel').val('');

			jQuery.each(data, function(key, value) {
				var nOption = jQuery('<option></option>');
				nOption.attr('data-courriel', value.courriel);
				nOption.attr('value', value.key);
				nOption.text(value.prenom + ' ' + value.nom);

				jQuery('#iRefA_Employe').append(nOption);
			});

			jQuery('#zRefA_Employe').show();
		}, 'json');
	}
});

jQuery(document).on('change', '#iRefA_Employe', function() {
	jQuery('.iRefA_NomAutre').hide().removeAttr('requiered');
	jQuery('#iRefA_EmployeCourriel').val('');

	if(jQuery(this).val() > 0)
	{
		jQuery('#iRefA_EmployeCourriel').val(jQuery(this).find('option:selected').attr('data-courriel'));
	}
	else if(jQuery(this).find('option:selected').attr('data-autre') == 1)
	{
		jQuery('.iRefA_NomAutre').show().attr('requiered', '');
	}
});