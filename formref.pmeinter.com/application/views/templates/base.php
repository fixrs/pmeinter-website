<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>PME Inter - Formulaire de référence</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/style.css">
	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/themes/smoothness/jquery-ui.css" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/jquery-ui.min.js"></script>
	<!--<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js" type="text/javascript"></script>-->
	<script src="/script/principal.js"></script>
	<script src="/script/jquery.maskedinput.js" type="text/javascript"></script>

	<style>
		#myform {
			width: auto;
		}
		#myform label.error {
			margin-left: 10px;
			width: auto;
			display: inline;
		}
	</style>

</head>
<body>

<div id="container">
	<div id="body">
		<div id="branding">
			<a href="<?php echo base_url(); ?>" class="logo">
			</a>
			<div class="titre">
				<h1><?=$titre?></h1>
				<div class="copyright">Développé par F.I.L. Informatique Inc.</div>
			</div>
			<div class="clear"></div>
		</div>
		<?=$body?>
		<div class="clear"></div>
		<div class="pied">

		</div>
	</div>
</div>

</body>
</html>