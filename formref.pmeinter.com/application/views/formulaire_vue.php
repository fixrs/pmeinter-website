<p>
	<strong>Date :</strong> <?=issetor($contenu['dDate'], date('Y-m-d'))?>
</p>

<p><strong>R&eacute;f&eacute;r&eacute; par :</strong>
	<?=issetor($contenu['aRefPar']->prenom)?> <?=issetor($contenu['aRefPar']->nom)?>
</p>

<p><strong>R&eacute;f&eacute;r&eacute; &agrave; l’étude de  :</strong>
	<?=issetor($contenu['aRefA_Etude']->nom)?>
</p>

<div id="zRefA_Employe">
	<p><strong>Personne responsable :</strong>
		<?php 
		if(empty($contenu['iRefA_NomAutre']))
		{
			echo issetor($contenu['aRefA_Employe']->prenom) . ' ' . issetor($contenu['aRefA_Employe']->nom);
		}
		else
		{
			echo $contenu['iRefA_NomAutre'];
		}
		?>
	</p>
	<p><strong>Courriel :</strong>
		<?=issetor($contenu['iRefA_EmployeCourriel'])?>
	</p>
</div>

<p><strong>Suivi &agrave; faire d'ici le :</strong>
	<?=issetor($contenu['dDateSuivi'])?>
</p>

<p><strong>Description du mandat :</strong> <br/>
	<?=nl2br(issetor($contenu['sDescMandat']))?>
</p>

<p><strong>Personne contact :</strong>
	<?=issetor($contenu['sPersonneContact'])?>
</p>

<p><strong>Cat&eacute;gorie d'&acirc;ge :</strong>
	<?=$groupes_age[issetor($contenu['iCatAge'], 0)]?> 
</p>

<p><strong>Nom de l'entreprise :</strong>
	<?=issetor($contenu['sNomEntreprise'])?>
</p>

<p><strong>Numéro de téléphone :</strong>
	<?=issetor($contenu['sTelEntreprise'])?>
</p>

<p><strong>Adresse courriel :</strong>
	<?=issetor($contenu['sAdresseCourriel']) ?>
</p>

<?php 
if(isset($contenu['iResponsableSuivi']))
{
 ?>
<p><strong>Date de suivi pr&eacute;vue par l'&eacute;tude :</strong>
	<?=issetor($contenu['dDateSuiviEtude']) ?>
</p>

<p><strong>Personne responsable du suivi :</strong>
	<?=issetor($contenu['aResponsableSuivi_Employe']->prenom)?> <?=issetor($contenu['aResponsableSuivi_Employe']->nom)?>
</p>
<?php
}
?>