<?php
if(!isset($contenu))
	$contenu = array();

if(!isset($mode))
	$mode = 'creation';

//$attributes = array('id' => 'myform');

?>

	<?php echo validation_errors(); ?>

	<?=form_open('formulaire/envoyer', array('id' => 'formRef')); ?>
		<label for="dDate">Date :</label>
		<input type="text" name="dDate" id="dDate" class="date large" value="<?=issetor($contenu['dDate'], date('Y-m-d'))?>" required />

		<label for="iRefPar">R&eacute;f&eacute;r&eacute; par :</label>
		<input type="hidden" name="iRefPar" id="iRefPar" class="large" />
		<input type="text" data-ref="iRefPar" id="iRefPar_autocomp" class="large autocomt_employe" />

		<!-- <select name="iRefPar" id="iRefPar" class="large" required >
			<option value="">Choisir un employé ...</option>
			<?php 
			$valRefPar = issetor($contenu['iRefPar']);

			foreach($employes as $employe)
			{
				$sel = ($valRefPar == $employe->key ? 'selected="selected"' : '');
				?>
				<option value="<?=$employe->key?>"><?=$employe->prenom?> <?=$employe->nom?></option>
				<?php
			}
			?>
		</select> -->

		<label for="iRefA_Etude">R&eacute;f&eacute;r&eacute; &agrave; l’étude de :</label>
		<input type="hidden" name="iRefA_Etude" id="iRefA_Etude" required >
		<input type="text" data-ref="iRefA_Etude" id="iRefA_Etude_autocomp" class="large autocomt_etude" />

		<!-- <select name="iRefA_Etude" id="iRefA_Etude" class="large" required >
			<option value="">Choisir une étude ...</option>
			<?php 
			$valRefPar = issetor($contenu['iRefA_Etude']);

			foreach($etudes as $etude)
			{
				$sel = ($valRefPar == $etude->key ? 'selected="selected"' : '');
				?>
				<option value="<?=$etude->key?>"><?=$etude->nom?></option>
				<?php
			}
			?>
		</select> -->

		<div id="zRefA_Employe">
			<label for="iRefA_Employe" class="floatLeft">Personne responsable :</label>
			<select name="iRefA_Employe" id="iRefA_Employe" class="floatLeft large" required >
				<option>Choisir un responsable ...</option>
				<option data-autre="1">Autre</option>
				<?php 
				// TODO: Charger la liste adéquate
				?>
			</select>
			<div class="clear"></div>

			<p style="margin-top:5px;">
				<label for="iRefA_NomAutre" class="iRefA_NomAutre floatLeft masquer">Nom :</label>
				<input type="text" name="iRefA_NomAutre" id="iRefA_NomAutre" class="floatLeft large masquer iRefA_NomAutre" value="<?=issetor($contenu['iRefA_NomAutre'])?>" />
				
				<label for="iRefA_EmployeCourriel" class="floatLeft">Courriel :</label>
				<input type="text" name="iRefA_EmployeCourriel" id="iRefA_EmployeCourriel" class="floatLeft large" value="<?=issetor($contenu['iRefA_EmployeCourriel'])?>" />
			</p>
			<div class="clear"></div>
		</div>

		<label for="dDateSuivi">Suivi &agrave; faire d'ici le :</label>
		<input type="text" name="dDateSuivi" id="dDateSuivi" class="date large" value="<?=issetor($contenu['dDateSuivi'])?>" required />

		<label for="sDescMandat">Description du mandat :</label>
		<textarea name="sDescMandat" id="sDescMandat" class="xlarge" required ><?=issetor($contenu['sDescMandat'])?></textarea>

		<label for="sPersonneContact">Personne contact :</label>
		<input type="text" name="sPersonneContact" id="sPersonneContact" class="large"  value="<?=issetor($contenu['dDateSuivi'])?>" required />

		<label for="">Cat&eacute;gorie d'&acirc;ge :</label>
		<fieldset>
			<?php 
			foreach($groupes_age as $key => $value)
			{
				$checked = (issetor($contenu['iCatAge']) == $key ? 'checked="checked"' : '');
			
				if($key != 0){

			?>
			<input type="radio" name="iCatAge" id="iCatAge_<?=$key?>" value="<?=$key?>" <?=$checked?> />
			<label for="iCatAge_<?=$key?>"><?=$value?></label>
			<?php

				}
			
			}
			?>
		</fieldset>

		<label for="sNomEntreprise">Nom de l'entreprise :</label>
		<input type="text" name="sNomEntreprise" id="sNomEntreprise" class="large" value="<?=issetor($contenu['sNomEntreprise'])?>" required />

		<label for="sTelEntreprise">Numéro de t&eacute;l&eacute;phone :</label>
		<input type="text" name="sTelEntreprise" id="sTelEntreprise" class="large tel" value="<?=issetor($contenu['sTelEntreprise'])?>" required />

		<label for="sAdresseCourriel">Adresse courriel :</label>
		<input type="email" name="sAdresseCourriel" id="sAdresseCourriel" class="large" value="<?=issetor($contenu['sAdresseCourriel'])?>" required />

		<?php 
		if($mode == 'vue')
		{
		?>
		<input type="hidden" name="id" value="<?=$donnees->IDFormulaire?>"/>
		<input type="hidden" name="validate" value="<?=md5($donnees->IDFormulaire . 'jeanguybob')?>" />

		<label for="dDateSuiviEtude">Date de suivi pr&eacute;vue par l'&eacute;tude :</label>
		<input type="text" name="dDateSuiviEtude" id="dDateSuiviEtude" class="date large" value="<?=issetor($contenu['dDateSuiviEtude'])?>" />

		<label for="iResponsableSuivi">Personne responsable du suivi :</label>
		<select name="iResponsableSuivi" id="iResponsableSuivi" class="large">

		</select>
		<?php
		}
		?>
		<p class="envoyer alDroite">
			<button type="submit" name="envoyer">Envoyer</button>
		</p>
	</form>