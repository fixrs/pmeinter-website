<?php
if(!isset($contenu))
	$contenu = array();

if(!isset($mode))
	$mode = 'creation';

//$attributes = array('id' => 'myform');

?>

	<?php echo validation_errors('<p class="error">', '</p>'); ?>

	<?=form_open('formulaire/envoyer_suivi', array('id' => 'myform')); ?>
		<input type="hidden" name="id" value="<?=$donnees->IDFormulaire?>"/>
		<input type="hidden" name="clef" value="<?=$clef?>" />

		<?=$detail_form?>

		<label for="dDateSuiviEtude">Date de suivi pr&eacute;vue par l'&eacute;tude :</label>
		<input type="text" name="dDateSuiviEtude" id="dDateSuiviEtude" class="date large" value="<?=issetor($contenu['dDateSuiviEtude'])?>" />

		<label for="iResponsableSuivi">Personne responsable du suivi :</label>
		<select name="iResponsableSuivi" id="iResponsableSuivi" class="large">
			<option value="">Choisir un employé ...</option>
			<?php 
			$valRefPar = issetor($contenu['iRefPar']);

			foreach($employes as $employe)
			{
				$sel = ($valRefPar == $employe->key ? 'selected="selected"' : '');
				?>
				<option value="<?=$employe->key?>"><?=$employe->prenom?> <?=$employe->nom?></option>
				<?php
			}
			?>
		</select>

		<p class="envoyer alDroite">
			<button type="submit" name="envoyer">Envoyer</button>
		</p>
	</form>
	<script>
		$("#myform").validate();
	</script>