<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Formulaire extends CI_Controller 
{
	private static $arrGroupeAge = array(1 => '20 - 35 ans',
										 2 => '36 - 45 ans',
										 3 => '46 ans et plus',
										 0 => 'N/D');


	public function __construct()
	{
		parent::__construct();
		$this->load->library('template');
		$this->template->setTitre('Formulaire de référence');
	}

	private function prepDonnees(&$params = null, &$arrDonnees = null)
	{
		if(!is_array($params))
			$params = array();

		$this->load->model('Etudes');
		$this->load->model('Employes');

		if(is_array($arrDonnees) && isset($arrDonnees['iRefA_Etude']))
			$arrDonnees['aRefA_Etude'] = $this->Etudes->obtenir($arrDonnees['iRefA_Etude']);
		if(is_array($arrDonnees) && isset($arrDonnees['iRefPar']))
			$arrDonnees['aRefPar'] = $this->Employes->obtenir($arrDonnees['iRefPar']);
		if(is_array($arrDonnees) && isset($arrDonnees['iRefA_Employe']))
			$arrDonnees['aRefA_Employe'] = $this->Employes->obtenir($arrDonnees['iRefA_Employe']);

		if(is_array($arrDonnees) && isset($arrDonnees['iResponsableSuivi']))
			$arrDonnees['aResponsableSuivi_Employe'] = $this->Employes->obtenir($arrDonnees['iResponsableSuivi']);

		$params['etudes'] = $this->Etudes->obtenirListe();

		if(is_array($arrDonnees) && isset($arrDonnees['iRefA_Etude']))
			$params['employes'] = $this->Employes->obtenirListeEtude($arrDonnees['iRefA_Etude']);
		else
			$params['employes'] = $this->Employes->obtenirListe();

		$params['groupes_age'] = self::$arrGroupeAge;

		return $params;
	}

/**
 * Fonction pour générer ou valider la clef md5 du formulaire
 * @param  int $id   l'identifiant du formulaire
 * @param  string $clef la clef reçue (pour validation)
 * @return bool|string       un bool si la clef est présente en fonction de sa validité
 */
	private function clefForm($id, $clef = null)
	{
		$md5 = md5((int)$id . 'PME_Inter_clef_7M8ugG2QhFKD9EfJDz4CPQCXCuTJfgr5SbYBdDeZ');

		if($clef === null)
			return $md5;
		else
			return $clef == $md5;
	}

	public function index()
	{
		$this->template->load('base', 'formulaire_1', $this->prepDonnees());
	}

	public function envoyer() 
	{
		$this->load->model('Formulaires');
		$this->load->model('Employes');

		//$this->load->helper(array('Formulaires', 'url'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('dDate', 'Date', 'trim|required');
		$this->form_validation->set_rules('iRefPar', 'Choisir un employé', 'trim|required|xss_clean');
		$this->form_validation->set_rules('iRefA_Etude', 'Choisir une étude', 'trim|required|xss_clean');
		$this->form_validation->set_rules('iRefA_Employe', 'Employé', 'trim|required|xss_clean');
		$this->form_validation->set_rules('iRefA_EmployeCourriel', 'Courriel', 'trim|required|xss_clean');
		$this->form_validation->set_rules('dDateSuivi', 'Suivi à faire d\'ici le', 'trim|required|xss_clean');
		$this->form_validation->set_rules('sDescMandat', 'Description du mandat', 'trim|required|xss_clean');
		$this->form_validation->set_rules('sPersonneContact', 'Personne contact', 'trim|required|xss_clean');
		$this->form_validation->set_rules('iCatAge', 'Catégorie d\'âge', 'trim|required|xss_clean');
		$this->form_validation->set_rules('sNomEntreprise', 'Nom de l\'entreprise', 'trim|required|xss_clean');
		$this->form_validation->set_rules('sTelEntreprise', 'Numéro de téléphones', 'trim|required|xss_clean');
		$this->form_validation->set_rules('sAdresseCourriel', 'Email', 'trim|required|valid_email|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			$this->template->load('base', 'formulaire_1', $this->prepDonnees());
			//$this->load->view('formulaire_1');
			//die();
		
		}
		else
		{
			$form_id = false;
			//$this->load->view('confirmation');

			// TODO: Validation
			if($this->input->post('id'))
			{
				// Modifier un formulaire
				//$this->Formulaires->modifier();
			}
			else
			{
				// Ajouter un formulaire
				$form_id = $this->Formulaires->ajouter();
			}

			// Envoyer le formulaire
			$this->load->library('email');

			$config['mailtype'] = 'html';
			$config['wordwrap'] = false;

			$this->email->initialize($config);

			$data = array();
			$data = $this->prepDonnees($data, $_REQUEST);
			$data['contenu'] = $_REQUEST;

			$referer_par = $this->Employes->obtenir($this->input->post('iRefPar'));
			$msg = $this->load->view('formulaire_vue', $data, TRUE);

			if(!empty($referer_par->courriel))
			{

				$this->email->from('web@pmeinter.com', 'Web PME Inter');
				// $this->email->to('mp@filinformatique.com'); //$referer_par //mdelorimier@openmindt.com
				// $this->email->cc('mp@filinformatique.com'); // mdelorimier@openmindt.com
				$this->email->to($referer_par->courriel); 
				$this->email->cc('lgravel@pmeinter.com');

				$this->email->subject('Formulaire de référence');
				$this->email->message($msg);

				//var_dump($referer_par->courriel);	
				//die();

				$this->email->send();
			}


			// Envoyer message contenant le lien pour accéder au formulaire avec infos présentes selon la clé
			$this->email->initialize($config); 

			//$data = array();
			//$data = $this->prepDonnees($data, $_REQUEST);
			//$data['contenu'] = $_REQUEST; 

			$referer_a = $this->Employes->obtenir($this->input->post('iRefA_Employe'));

			if (!empty($referer_a->courriel)) 
			{
				$md5_key = $this->clefForm($form_id);
				$form_url = site_url('/formulaire/voir/' .  $form_id . '/' . $md5_key);

				$msgPre = '<br/><br/>Un confrère de PME INTER Notaires vous a référé un client, <br/>vous devez compléter le formulaire de référence en cliquant sur le lien ci-dessous :<br/>';
				$msgPre .=  '<a href="' . $form_url . '">' . $form_url . '</a><br/><br/>';

				$msg = $msgPre . $msg;

				$this->email->from('web@pmeinter.com', 'Web PME Inter');
				$this->email->to($referer_a->courriel);
				//$this->email->to('mp@filinformatique.com'); //$referer_a //mdelorimier@openmindt.com

				$this->email->subject('Formulaire de référence');
				$this->email->message($msg);

				$this->email->send();
			}



			// Page de remerciement
			redirect('/formulaire/confirmation');

		}
	}

	public function envoyer_suivi() 
	{
		$this->load->model('Formulaires');
		$this->load->model('Employes');

		$this->load->library('form_validation');

		$this->form_validation->set_rules('dDateSuiviEtude', 'Date de suivi prévue par l\'étude', 'trim|required');
		$this->form_validation->set_rules('iResponsableSuivi', 'Personne responsable du suivi', 'trim|required|xss_clean');

		$clef = $this->input->post('clef');
		$id = $this->input->post('id');

		if( $clef === null || !$this->clefForm($id, $clef))
		{
			$this->template->load('base', 'erreur');
		}
		else if ($this->form_validation->run() == FALSE)
		{
			$donnees = $this->Formulaires->lire($id);

			if($donnees)
			{
				$contenu = json_decode($donnees->sContenu, true);

				$data = array();
				$data = $this->prepDonnees($data, $contenu);
				$data['contenu'] = $contenu;

				$detail_form = $this->load->view('formulaire_vue', $data, TRUE);

				$params = array('donnees' => $donnees, 'contenu' => $contenu, 'mode' => 'vue', 'id' => $id, 'clef' => $clef, 'detail_form' => $detail_form);

				$this->template->load('base', 'formulaire_2', $this->prepDonnees($params, $contenu));
			}
		}
		else
		{
			$form_id = false;
			//$this->load->view('confirmation');

			// Modifier un formulaire
			$contenu = $this->Formulaires->modifier($id);
			
			$data = array();
			$data['contenu'] = json_decode($contenu->sContenu, true);
			$data = $this->prepDonnees($data, $data['contenu']);

			// TODO: Envoyer le formulaire
			$this->load->library('email');

			$config['mailtype'] = 'html';
			$config['wordwrap'] = false;

			$this->email->initialize($config);

			$msg = $this->load->view('formulaire_vue', $data, TRUE);

			$this->email->from('web@pmeinter.com', 'Web PME Inter');
			$this->email->to('mp@filinformatique.com'); //$referer_par //mdelorimier@openmindt.com

			$this->email->subject('Formulaire de référence - complété par l\'étude');
			$this->email->message($msg);

			$this->email->send();


			// Page de remerciement
			redirect('/formulaire/confirmation');

		}
	}

	public function confirmation()
	{
		$this->template->load('base', 'confirmation');
	}

	public function voir($id = null, $clef = null) 
	{
		
		$this->load->model('Formulaires');
		
		$donnees = $this->Formulaires->lire($id);

		if(($donnees) && $clef !== null && $this->clefForm($id, $clef))
		{
			$contenu = json_decode($donnees->sContenu, true);

			$data = array();
			$data = $this->prepDonnees($data, $contenu);
			$data['contenu'] = $contenu;

			$detail_form = $this->load->view('formulaire_vue', $data, TRUE);

			$params = array('donnees' => $donnees, 'contenu' => $contenu, 'mode' => 'vue', 'id' => $id, 'clef' => $clef, 'detail_form' => $detail_form);

			$this->template->load('base', 'formulaire_2', $this->prepDonnees($params, $contenu));
		}
		else
		{
			$this->template->load('base', 'erreur');
		}
	}

	private static $arrChampsExport = array('dDate' => array('l' => 12, 'titre' => 'Date'),
											 'aRefPar_Nom' => array('l' => 20, 'titre' => 'Référé par'),
											 'aRefA_Etude_Nom' => array('l' => 25, 'titre' => 'Référé à'), 
											 'aRefA_Employe_Nom' => array('l' => 20, 'titre' => 'Personne responsable'), 
											 'iRefA_EmployeCourriel' => array('l' => 25, 'titre' => 'Courriel'),
											 'dDateSuivi' => array('l' => 12, 'titre' => 'Suivi à faire d\'ici le'),
											 'sPersonneContact' => array('l' => 20, 'titre' => 'Personne contact'),
											 'iCatAge_Nom' => array('l' => 12, 'titre' => 'Catégorie d\'âge'), 
											 'sNomEntreprise' => array('l' => 25, 'titre' => 'Nom de l\'entreprise'),
											 'sTelEntreprise' => array('l' => 14, 'titre' => 'Numéro de téléphone'),
											 'sAdresseCourriel' => array('l' => 25, 'titre' => 'Adresse courriel'),
											 'dDateSuiviEtude' => array('l' => 12, 'titre' => 'Date de suivi prévue'),
											 'aResponsableSuivi_Employe_Nom' => array('l' => 20, 'titre' => 'Personne responsable du suivi'), 
											 'sDescMandat' => array('l' => 30, 'titre' => 'Description du mandat'),
											 );

	public function Exporter()
	{
		/*if(!isset($_SERVER['PHP_AUTH_USER']))
        {
			header('WWW-Authenticate: Basic realm="PME Inter Exportation"');
			header('HTTP/1.0 401 Unauthorized');
			echo("Entrez un usager valide");
			exit();
        }
        else if ( ($_SERVER['PHP_AUTH_USER'] == 'admin') && ($_SERVER['PHP_AUTH_PW'] == 'pass123.'))
        {*/
        	$this->load->model('Formulaires');
			$this->load->model('Employes');
			$this->load->model('Etudes');

        	// Faire l'exportation
			$this->load->library('excel');

			$this->excel->setActiveSheetIndex(0);

			$this->excel->getActiveSheet()->setTitle('Clients référés');

			$rowIdx = 1;
			$colStrIdx = 'A';

			foreach(self::$arrChampsExport as $arrOpt)
			{
				$this->excel->getActiveSheet()->setCellValue($colStrIdx . $rowIdx, $arrOpt['titre']);
				$this->excel->getActiveSheet()->getStyle($colStrIdx . $rowIdx)->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getColumnDimension($colStrIdx)->setWidth($arrOpt['l']);
				$colStrIdx++;
			}
			
			$formulaires = $this->Formulaires->obtenirListe();

			foreach($formulaires as $formulaire)
			{
				$arrDonnees = get_object_vars($formulaire);

				$arrDonnees = array_merge($arrDonnees, json_decode($arrDonnees['sContenu'], true));

				$arrDonnees['aRefA_Etude_Nom'] 				= $this->Etudes->obtenir($arrDonnees['iRefA_Etude'], true);
				$arrDonnees['aRefA_Employe_Nom'] 			= (empty($arrDonnees['iRefA_NomAutre']) ? $this->Employes->obtenir($arrDonnees['iRefA_Employe'], true) : $arrDonnees['iRefA_NomAutre']);
				$arrDonnees['aRefPar_Nom'] 					= $this->Employes->obtenir($arrDonnees['iRefPar'], true);
				$arrDonnees['iCatAge_Nom'] 					= self::$arrGroupeAge[$arrDonnees['iCatAge']];
				if(isset($arrDonnees['iResponsableSuivi']))
					$arrDonnees['aResponsableSuivi_Employe_Nom']= $this->Employes->obtenir($arrDonnees['iResponsableSuivi'], true);
				
				//var_dump($arrDonnees);
				//die();

				$rowIdx++;
				$colStrIdx = 'A';

				foreach(self::$arrChampsExport as $clef => $SNomChamp)
				{
					$val = isset($arrDonnees[$clef]) ? $arrDonnees[$clef] : '';
					$this->excel->getActiveSheet()->setCellValue($colStrIdx . $rowIdx, $val);
					$colStrIdx++;

				}
			}
			 
			$filename='exportation-' . date('Y-m-d') . '.xls'; //save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache

			$this->excel->getActiveSheet()->setAutoFilter($this->excel->getActiveSheet()->calculateWorksheetDimension());

			$this->excel->getActiveSheet()->freezePane('A2');
			             
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			$objWriter->save('php://output');

			exit();
/*        }
        else
        {
			echo("Please enter a valid username and password");
			exit();
        }*/
	}
}
