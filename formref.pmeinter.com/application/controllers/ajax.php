<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('output');
	}

	private function prepDonnees(&$params = null)
	{
		if(!is_array($params))
			$params = array();

		$this->load->model('Etudes');
		$this->load->model('Employes');

		$params['etudes'] = $this->Etudes->obtenirListe();
		$params['employes'] = $this->Employes->obtenirListeEtude(1);

		return $params;
	}

	public function index()
	{
		header('HTTP/1.1 404 Not permited');
	}

	public function employes_etude($idEtude)
	{
		$this->load->model('Employes');

		$ret = $this->Employes->obtenirListeEtude($idEtude);

		echo json_encode($ret);
	}

	public function employes()
	{
		$this->load->model('Employes');

		$ret = $this->Employes->obtenirAutocomplete($this->input->get('term'));

		echo json_encode($ret);
	}

	public function etudes()
	{
		$this->load->model('Etudes');

		$ret = $this->Etudes->obtenirAutocomplete($this->input->get('term'));

		echo json_encode($ret);
	}

}
