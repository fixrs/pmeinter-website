<?php
class Etudes extends CI_Model 
{
	var $key = null;
	var $nom = null;
	var $description = null;

	public $arrChamps = array();


    function __construct()
    {
		parent::__construct();
    }

    function obtenirListe()
    {
		$query = $this->db->select('key, nom, description')->get('etudes', 100);
		return $query->result();
    }

	function obtenirAutocomplete($term)
	{
		$query = $this->db->select("`key` AS `id`, nom AS `label`, nom AS `value`", false)
					->order_by('label', 'ASC')
					->get_where('etudes', 'actif = 1' . (!empty($term) ? " AND nom LIKE '%{$term}%'" : ''), 20);
		return $query->result();
	}

	function obtenir($id, $nomSeulement = false)
	{
		$query = $this->db->select('key, nom, description')->get_where('etudes', 'key = ' . (int)$id, 1);

		if(count($query->result()) == 0)
			return false;
		else
			return $nomSeulement ? $query->result()[0]->nom : $query->result()[0];
	}
}