<?php
class Employes extends CI_Model 
{
	public $arrChamps = array();


	function __construct()
	{
		parent::__construct();
	}

	function obtenirListe()
	{
		$query = $this->db->select('key, prenom, nom, courriel')
					->order_by('prenom', 'asc')
					->get_where('employes', 'actif = 1', 250);
		return $query->result();
	}

	function obtenirAutocomplete($term)
	{
		$query = $this->db->select("`key` AS `id`, CONCAT(prenom, ' ', nom) AS `label`, CONCAT(prenom, ' ', nom) AS `value`", false)
					->order_by('label', 'ASC')
					->get_where('employes', 'actif = 1' . (!empty($term) ? " AND (prenom LIKE '%{$term}%' OR nom LIKE '%{$term}%') " : ''), 20);
		return $query->result();
	}

	function obtenirListeEtude($id_etude)
	{
		$query = $this->db->select('employes.key, employes.prenom, employes.nom, employes.courriel')
					->order_by('prenom', 'asc')
					->join('employes_etudes_succursales', 'employes_etudes_succursales.employes_key = employes.key')
					->join('etudes_succursales', 'employes_etudes_succursales.etudes_succursales_key = etudes_succursales.key')
					->get_where('employes', 'employes.actif = 1 AND etudes_succursales.etudes_key = ' . (int)$id_etude, 250);
		return $query->result();
	}

	function obtenir($id, $nomSeulement = false)
	{
		$query = $this->db->select('key, nom, prenom, description, courriel')->get_where('employes', 'key = ' . (int)$id, 1);

		if(count($query->result()) == 0)
			return false;
		else
			return $nomSeulement ? $query->result()[0]->prenom . ' ' . $query->result()[0]->nom : $query->result()[0];
	}
}