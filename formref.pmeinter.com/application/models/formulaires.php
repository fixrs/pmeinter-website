<?php
class Formulaires extends CI_Model 
{
    var $IDFormulaire = null;
    var $dDateModification = null;
    var $dDateCreation = null;
    var $sContenu = '';

    public $arrChamps = array();


    function __construct()
    {
        parent::__construct();
    }
    
    function get_last_ten_entries()
    {
        $query = $this->db->get('tblFormulaires', 10);
        return $query->result();
    }

    public function lire($id)
    {
        $query = $this->db->get_where('tblFormulaires', array('IDFormulaire' => $id));
        
        if($query->num_rows > 0)
            return $query->result()[0];

        return false;
    }

    /**
     * Ajouter un nouveau formulaire
     * @return int ID d'insertion du nouveau formulaire
     */
    public function ajouter()
    {
        $this->sContenu = json_encode($_POST);
        
        $this->db->insert('tblFormulaires', $this);
        
        $form_id = $this->db->insert_id();

        return $form_id;
    }    

    public function modifier($id)
    {
        $form = $this->lire($id);

        $contenu = json_decode($form->sContenu, true);

        $contenu['dDateSuiviEtude'] = $_REQUEST['dDateSuiviEtude'];
        $contenu['iResponsableSuivi'] = (int)$_REQUEST['iResponsableSuivi'];

        $arrData = array('sContenu' => json_encode($contenu), 'dDateModification' => date('Y-m-d H:i:s'));
        
        $form->sContenu = json_encode($contenu);
        
        $this->db->where('IDFormulaire', $id);
        $this->db->update('tblFormulaires', $arrData);

        return $form;
    }

    function obtenirListe()
    {
        $query = $this->db->select('*')->get('tblFormulaires', 100);
        return $query->result();
    }

}