<?php
/**
 * The template used for displaying page content in page.php
 *
 * Template Name: Home
 *
 * @package pmeinter
 * @since pmeinter 1.0
 */
//setcookie( "DEMO2018", 1, time()+3600, "/");
//$form = get_content(945);//do_shortcode('');

// $content_post = get_post(945);
// $content = $content_post->post_content;
// $content = apply_filters('the_content', $content);
// $form = str_replace(']]>', ']]&gt;', $content);
// file_put_contents("test.html", $form);
//$form = file_get_contents("test.html");

get_header();



while ( have_posts() ) {
    the_post();

?>
    <div class="header">
        <?php


	        the_post_thumbnail("full");
        

        //echo "<div class='intro-video'>" . get_the_title() . "</div>";

            $facebook = get_field("facebook");
            $linkedin = get_field("linkedin");
            $youtube = get_field("youtube");

            echo "<h1>" . get_the_title() . "</h1>";
            echo "<h2>" . get_field("sous-titre") . "</h2>";

            echo
                '<div class="social">
                ' . ($facebook != '' ? '<a href="' . $facebook . '" target="_blank" class="facebook"><i class="fa fa-facebook"></i> <span>Facebook</span></a> ' : "") . '
                ' . ($linkedin != '' ? '<a href="' . $linkedin . '" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i> <span>LinkedIn</span></a> ' : "") . '
                ' . ($youtube != '' ? '<a href="' . $youtube . '" target="_blank" class="youtube"><i class="fa fa-youtube"></i> <span>Youtube</span></a> ' : "") . '
                </div>';

            echo
                '<div id="map">
                    <h3>' . __("Trouvez un notaire") . '</h3>
                    <div class="content">
                        <form action="' . __("/notaires") . '" method="post">
                            <select id="citydd" name="city">
                                <option value="">' . __("Ville") . '</option>
                                ' . getCityDD() . '
                            </select>
                            <input type="text" id="scity" name="scity" value="" placeholder="' . __("Notaire ou &Eacute;tude") . '" />
                            <input type="submit" id="mapsubmit" value="OK" />
                            <a href="' . __("/notaires-carte") . '" id="linktomap">' . __("Carte") . '</a>
                        </form>
                    </div>
                </div>';

        ?>
    </div>

    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">

            <?php

                $video_1 = get_field("video_1");
                $video_2 = get_field("video_2");
                $video_3 = get_field("video_3");
                $video_4 = get_field("video_4");
                $video_5 = get_field("video_5");
                $video_6 = get_field("video_6");
                $video_7 = get_field("video_7");
                $video_8 = get_field("video_8");
                $video_9 = get_field("video_9");
                $video_10 = get_field("video_10");

                $flbg = get_field("background_format_legal");
                if (trim($flbg) != "") { $flbg = ' style="background-image: url(\'' . $flbg . '\');" '; }

                //ob_start();

                //gravity_form(8, false, false, false, '', true);
                //$form = ob_get_contents();
                //ob_end_clean();


                echo
                    '<div class="row">
                        <h2>' . get_field("titre_avantages") . '</h2>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 col-xs-12">
                            <div class="avantage" style="background-image: url(\'' . get_field("avantage_bloc_1_image") . '\');">
                                <h3><a href="' . get_field("avantage_bloc_1_lien") . '">' . get_field("avantage_bloc_1_titre") . '</a></h3>
                                <a href="' . get_field("avantage_bloc_1_lien") . '" class="more">En savoir plus</a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <div class="avantage" style="background-image: url(\'' . get_field("avantage_bloc_2_image") . '\');">
                                <h3><a href="' . get_field("avantage_bloc_2_lien") . '">' . get_field("avantage_bloc_2_titre") . '</a></h3>
                                <a href="' . get_field("avantage_bloc_2_lien") . '" class="more">En savoir plus</a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <div class="avantage" style="background-image: url(\'' . get_field("avantage_bloc_3_image") . '\');">
                                <h3><a href="' . get_field("avantage_bloc_3_lien") . '">' . get_field("avantage_bloc_3_titre") . '</a></h3>
                                <a href="' . get_field("avantage_bloc_3_lien") . '" class="more">En savoir plus</a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <div class="avantage" style="background-image: url(\'' . get_field("avantage_bloc_4_image") . '\');">
                                <h3><a href="' . get_field("avantage_bloc_4_lien") . '">' . get_field("avantage_bloc_4_titre") . '</a></h3>
                                <a href="' . get_field("avantage_bloc_4_lien") . '" class="more">En savoir plus</a>
                            </div>
                        </div>
                        <br clear="all" />
                    </div>
                    <div class="row videos">
                        <div class="cycle-slideshow" data-cycle-swipe="true" data-cycle-swipe-fx="scrollHorz" data-cycle-fx="scrollHorz" data-cycle-timeout="0" data-cycle-prev="#prev" data-cycle-next="#next" data-cycle-slides="> div.video">
                            ' . ($video_1 != '' ? '<div class="video video1">' . $video_1 . '</div>' : '') . '
                            ' . ($video_2 != '' ? '<div class="video video2">' . $video_2 . '</div>' : '') . '
                            ' . ($video_3 != '' ? '<div class="video video3">' . $video_3 . '</div>' : '') . '
                            ' . ($video_4 != '' ? '<div class="video video4">' . $video_4 . '</div>' : '') . '
                            ' . ($video_5 != '' ? '<div class="video video5">' . $video_5 . '</div>' : '') . '
                            ' . ($video_6 != '' ? '<div class="video video6">' . $video_6 . '</div>' : '') . '
                            ' . ($video_7 != '' ? '<div class="video video7">' . $video_7 . '</div>' : '') . '
                            ' . ($video_8 != '' ? '<div class="video video8">' . $video_8 . '</div>' : '') . '
                            ' . ($video_9 != '' ? '<div class="video video9">' . $video_9 . '</div>' : '') . '
                            ' . ($video_10 != '' ? '<div class="video video10">' . $video_10 . '</div>' : '') . '
                        </div>

                        <div class="center">
                            <a href=# id="prev"><i class="fa fa-chevron-left"></i></a>
                            <a href=# id="next"><i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                    <div class="row format_legal" ' . $flbg . '>
                        ' . get_field("format_legal") . '
                        <br clear="all" />
                    </div>';

            ?>

        </div><!-- #content .site-content -->
    </div><!-- #primary .content-area -->

<?php
   }

    //get_sidebar();
    get_footer();
?>
