<?php
/**
 * pmeinter functions and definitions
 *
 * @package pmeinter
 * @since pmeinter 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since pmeinter 1.0
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

/*
 * Load Jetpack compatibility file.
 */
require( get_template_directory() . '/inc/jetpack.php' );

if ( ! function_exists( 'pmeinter_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * @since pmeinter 1.0
 */
function pmeinter_setup() {

	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/inc/template-tags.php' );

	/**
	 * Custom functions that act independently of the theme templates
	 */
	require( get_template_directory() . '/inc/extras.php' );

	/**
	 * Customizer additions
	 */
	require( get_template_directory() . '/inc/customizer.php' );

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on pmeinter, use a find and replace
	 * to change 'pmeinter' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'pmeinter', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'pmeinter' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );
}
endif; // pmeinter_setup
add_action( 'after_setup_theme', 'pmeinter_setup' );

/**
 * Setup the WordPress core custom background feature.
 *
 * Use add_theme_support to register support for WordPress 3.4+
 * as well as provide backward compatibility for WordPress 3.3
 * using feature detection of wp_get_theme() which was introduced
 * in WordPress 3.4.
 *
 * @todo Remove the 3.3 support when WordPress 3.6 is released.
 *
 * Hooks into the after_setup_theme action.
 */
function pmeinter_register_custom_background() {
	$args = array(
		'default-color' => 'ffffff',
		'default-image' => '',
	);

	$args = apply_filters( 'pmeinter_custom_background_args', $args );

	if ( function_exists( 'wp_get_theme' ) ) {
		add_theme_support( 'custom-background', $args );
	} else {
		define( 'BACKGROUND_COLOR', $args['default-color'] );
		if ( ! empty( $args['default-image'] ) )
			define( 'BACKGROUND_IMAGE', $args['default-image'] );
		add_custom_background();
	}
}
add_action( 'after_setup_theme', 'pmeinter_register_custom_background' );

/**
 * Register widgetized area and update sidebar with default widgets
 *
 * @since pmeinter 1.0
 */
function pmeinter_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar', 'pmeinter' ),
		'id' => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h1 class="widget-title">',
		'after_title' => '</h1>',
	) );
}
add_action( 'widgets_init', 'pmeinter_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function pmeinter_scripts() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	//wp_enqueue_style( 'sidebar', get_stylesheet_directory_uri() . '/sidebar.css', array('style') );

	wp_enqueue_script( 'small-menu', get_template_directory_uri() . '/js/small-menu.js', array( 'jquery' ), '20120206', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}
}
add_action( 'wp_enqueue_scripts', 'pmeinter_scripts' );

/**
 * Implement the Custom Header feature
 */
//require( get_template_directory() . '/inc/custom-header.php' );


function getCityDD() {
	$DB = new DB(DB_HOSTNAME_EXTRANET, DB_USER_EXTRANET, DB_PASSWORD_EXTRANET, DB_NAME_EXTRANET);
	$DB->query(
		"SELECT DISTINCT es.ville ".
		"FROM etudes_succursales es INNER JOIN etudes e ON e.key = es.etudes_key ".
		"WHERE e.actif = 1 ".
		"AND es.actif = 1 ".
		//"AND es.siege_social = '1' ".
		"AND e.nombre_notaires_associes > 0 ".
		"ORDER BY es.ville ASC");

	$dd = "";

	while ($DB->next_record()) {
		$dd .= "<option value=\"" . addslashes($DB->getField("ville")) . "\">" . $DB->getField("ville") . "</option>";
	}

	return $dd;
}

function getCPDD() {
	$DB = new DB(DB_HOSTNAME_EXTRANET, DB_USER_EXTRANET, DB_PASSWORD_EXTRANET, DB_NAME_EXTRANET);
	$DB->query("SELECT DISTINCT e.expertise, e.key FROM expertises e ORDER BY e.expertise ASC");

	$dd = "";

	while ($DB->next_record()) {
		$dd .= "<option value=\"" . addslashes($DB->getField("expertise")) . "\">" . $DB->getField("expertise") . "</option>";
	}

	return $dd;
}

function getCityList($filter = "", $page = "") {
	$DB = new DB(DB_HOSTNAME_EXTRANET, DB_USER_EXTRANET, DB_PASSWORD_EXTRANET, DB_NAME_EXTRANET);
	$DB->query(
		"SELECT DISTINCT es.ville ".
		"FROM etudes_succursales es INNER JOIN etudes e ON e.key = es.etudes_key ".
		"WHERE e.nombre_notaires_associes > 0 " .
		"AND e.actif = 1 AND es.actif = 1 ".
		($filter != "" ? " AND es.siege_social = '1' " : "") .
		"AND es.key != 67 ".
		"ORDER BY es.ville ASC");

	$list = "";
	while ($DB->next_record()) {
		if ($list != "") {
			$list .= " - <a href=\"" . __("/notaires-") . asciionly($DB->getField("ville")) . ($page == "rv" ? "/prendre-rendez-vous" : ($page == "di" ? "/obtenir-information" : "")) . "\">" . $DB->getField("ville") . "</a>";
		} else {
			$list .= "<a href=\"" . __("/notaires-") . asciionly($DB->getField("ville")) . ($page == "rv" ? "/prendre-rendez-vous" : ($page == "di" ? "/obtenir-information" : "")) . "\">" . $DB->getField("ville") . "</a>";
		}
	}

	return $list;
}

function asciionly($string) {
	$string = htmlentities($string, ENT_QUOTES, "UTF-8");
	$string = preg_replace("/ &amp; /", " et ", $string);
	$string = preg_replace("/&([a-z])[^\;]+\;/", "\$1", strtolower(trim($string)));
	$string = preg_replace("/[^a-z0-9]/", "-", $string);
	$string = preg_replace("/-+/", "-", $string);
	$string = preg_replace("/-$/", "", $string);
	$string = preg_replace("/-039/", "", $string);
	return $string;
}

function toHtml($str) {
	return htmlentities(trim(stripslashes($str)), ENT_QUOTES, "UTF-8");
}


add_filter( 'gform_pre_render_9', 'populate_choices' );
add_filter( 'gform_pre_validation_9', 'populate_choices' );
add_filter( 'gform_pre_submission_filter_9', 'populate_choices' );
add_filter( 'gform_admin_pre_render_9', 'populate_choices' );

function populate_choices( $form ) {
    //wp_die('non');
    if($form['id'] != 9){
      return $form;
    }
  
    $choices = array();

    $DB = new DB(DB_HOSTNAME_EXTRANET, DB_USER_EXTRANET, DB_PASSWORD_EXTRANET, DB_NAME_EXTRANET);

    	$DB->query(
		    "SELECT DISTINCT".
		          "   e.nom AS nom, ".
		          "   e.courriel AS courriel ".
		          "FROM ".
		          "   `etudes` AS e, ".
		          "   `etudes_succursales` AS es ".
		          "WHERE ".
		          "   es.etudes_key = e.key ".
		          "   AND es.actif = '1' ".
		          "   AND e.actif = '1';"
		);

    while ($record = $DB->next_record()) {

      $etude_nom = $DB->getField('nom');
      $choices[] = array( 'value' => $etude_nom, 'text' => $etude_nom );

    }
 
    


    $fields = $form['fields'];

    foreach($form['fields'] as &$field){
      if($field->id == 13){
        $field->placeholder = 'Sélectionnez une étude';
        $field->choices = $choices;
      }
    }
 
    return $form;
}











