<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package pmeinter
 * @since pmeinter 1.0
 */
?>

			</div><!-- #main .site-main -->
			<footer id="colophon" class="site-footer" role="contentinfo">

				<div id="menu-footer-thematiques">
					<?php
						if (ICL_LANGUAGE_CODE == 'en') {
							wp_nav_menu( array( 'theme_location' => 'primary', 'menu' => 'Footer-Thematiques-EN' ) );
						} else {
							wp_nav_menu( array( 'theme_location' => 'primary', 'menu' => 'Footer-Thematiques-FR' ) );
						}

						$content = "";
						if (ICL_LANGUAGE_CODE == 'en') {
							$url = "http://" . $_SERVER["HTTP_HOST"] . "/en/expertises/";
						} else {
							$url = "http://" . $_SERVER["HTTP_HOST"] . "/expertises/";
						}
						//$content = file_get_contents($url);
						//$secteurs = preg_replace("^.*?<ul id=\"secteurs\">(.*?)<\/ul>.*$/s", "$1", $content);
						$secteurs =
							"<li>Environnement, Fiscal, Autochtone, Minier, Maritime, Construction, Urbanisme, Municipal</li>";

						//echo "<div id=\"menu-footer-secteurs\"><a href=\"" . $url . "\">" . __("Expertise sectorielle") . "</a> <ul id=\"menu-footer-secteurs-" . ICL_LANGUAGE_CODE . "\" class=\"menu\">" . $secteurs . "</ul></div>";

						echo "<div class=\"menu-footer-secteurs\"><a href=\"/entreprises/droit-agricole/\"><strong>" . __("Droit agricole") . "</strong></a> <ul class=\"menu-footer-secteurs-" . ICL_LANGUAGE_CODE . " menu\"><li>Demandes de représentations auprès de la Commission de protection du territoire agricole</li><li>Acquisition, transfert et financement d'une entreprise agricole</li><li>Transfert intergénérationnel</li></ul> <span class=\"spacer\"></span> <a href=\"" . $url . "\"><strong>" . __("Expertise sectorielle") . "</strong></a> <ul class=\"menu-footer-secteurs-" . ICL_LANGUAGE_CODE . " menu\">" . $secteurs . "</ul></div>";

					?>
				</div>
				<?php

					if (isset($_COOKIE["DEMO2018"])) {

						echo
							"<div class=\"footer-ville-facebook\">\n".
							"	<div id=\"menu-footer-city\">\n".
							"		<h3>" . __("Nous sommes présents partout au Québec!") . "</h3>\n".
							"		<div class=\"content\">" . getCityList() . "</div>\n".
							"	</div>";



						if (ICL_LANGUAGE_CODE == 'en') {
							wp_nav_menu( array( 'theme_location' => 'primary', 'menu' => 'Footer-Form-EN' ) );
						} else {
							wp_nav_menu( array( 'theme_location' => 'primary', 'menu' => 'Footer-Form-FR' ) );
						}

						echo '<div class="fb-page" data-href="https://www.facebook.com/R%C3%A9seau-PME-INTER-Notaires-1444799325801880/" data-tabs="timeline" data-width="300" data-height="420" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/R%C3%A9seau-PME-INTER-Notaires-1444799325801880/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/R%C3%A9seau-PME-INTER-Notaires-1444799325801880/">Réseau PME INTER Notaires</a></blockquote></div></div>';



					} else {

						echo
							"<div id=\"menu-footer-city\">\n".
							"	<h3>" . __("Villes") . "</h3>\n".
							"	<div class=\"content\">" . getCityList() . "</div>\n".
							"</div>";

						if (ICL_LANGUAGE_CODE == 'en') {
							wp_nav_menu( array( 'theme_location' => 'primary', 'menu' => 'Footer-Form-EN' ) );
						} else {
							wp_nav_menu( array( 'theme_location' => 'primary', 'menu' => 'Footer-Form-FR' ) );
						}

					}
				?>

				<div class="coordonnees">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="/wp-content/themes/pmeinter/img/logo-pmeinter.jpg" alt="Logo PME Inter Notaires" id="logo-pmeinter-footer" /></a>
					<h3>Pour rejoindre<br />le siège social de<br />PME INTER NOTAIRES :<br /><a href="mailto:info@pmeinter.com">info@pmeinter.com</a></h3>
					<div class="address">100, boul. Alexis-Nihon<br />Bureau 985<br />Montréal (St-Laurent), Québec<br />H4M 2P5</div>
					<div class="email"><a href="mailto:info@pmeinter.com">info@pmeinter.com</a></div>
					<div class="credits"><a href="http://www.leeroy.ca/" target="_blank">Design - LEEROY</a><br /><a href="http://www.biffusion.com/" target="_blank">Réalisation - biffusion.com</a></div>
				</div>
			</footer><!-- #colophon .site-footer -->
		</div><!-- #page .hfeed .site -->

		<?php wp_footer(); ?>


		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-23306924-1']);
			_gaq.push(['_trackPageview']);
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
	</body>
</html>
