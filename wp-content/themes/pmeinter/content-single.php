<?php
/**
 * @package pmeinter
 * @since pmeinter 1.0
 */

if (!function_exists("colourBrightness")) {
	function colourBrightness($hex, $percent) {
		// Work out if hash given
		$hash = '';
		if (stristr($hex,'#')) {
			$hex = str_replace('#','',$hex);
			$hash = '#';
		}
		/// HEX TO RGB
		$rgb = array(hexdec(substr($hex,0,2)), hexdec(substr($hex,2,2)), hexdec(substr($hex,4,2)));
		//// CALCULATE
		for ($i=0; $i<3; $i++) {
			// See if brighter or darker
			if ($percent > 0) {
				// Lighter
				$rgb[$i] = round($rgb[$i] * $percent) + round(255 * (1-$percent));
			} else {
				// Darker
				$positivePercent = $percent - ($percent*2);
				$rgb[$i] = round($rgb[$i] * $positivePercent) + round(0 * (1-$positivePercent));
			}
			// In case rounding up causes us to go to 256
			if ($rgb[$i] > 255) {
				$rgb[$i] = 255;
			}
		}
		//// RBG to Hex
		$hex = '';
		for($i=0; $i < 3; $i++) {
			// Convert the decimal digit to hex
			$hexDigit = dechex($rgb[$i]);
			// Add a leading zero if necessary
			if(strlen($hexDigit) == 1) {
			$hexDigit = "0" . $hexDigit;
			}
			// Append to the hex string
			$hex .= $hexDigit;
		}
		return $hash.$hex;
	}
}

$couleur = get_field("couleur");
$category_list = strip_tags(get_the_category_list( __( ‘, ’, ‘pmeinter’ ) ));
$picture = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );
$auteur = get_field("auteur");
$darken_color = colourBrightness($couleur, -0.6 );
$lighter_color = colourBrightness($couleur, 0.1 );

echo "<!-- Darken = " . $darken_color . "-->";
echo "<!-- Lighter = " . $lighter_color . "-->";
/*

<style>
	::selection {
		background: <?php echo $darken_color; ?>;
	}
	.meta-top,
	.contact,
	.reseau,
	.intro,
	.entry-content table tr:first-child td,
	nav.article a:hover,
	.back:hover,
	.entry-content h3:before {
		background: <?php echo $color; ?>;
	}
	.contact:hover,
	.reseau:hover {
		background: <?php echo $darken_color; ?>;
	}
	.contact:after,
	.reseau:after {
		border-color: <?php echo $darken_color; ?> rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) rgba(0, 0, 0, 0);
	}
	.contact:hover:after {
		border-color: <?php echo $color; ?> rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) rgba(0, 0, 0, 0);
	}
	.reseau:hover:after {
		border-color: <?php echo $color; ?> rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) rgba(0, 0, 0, 0);
	}
	.entry-content h3,
	.highlight,
	.entry-content li:before,
	blockquote:before,
	blockquote:after {
		color: <?php echo $color; ?>;
	}
	.exergue,

	.share a:hover {
		color: <?php echo $color; ?>;
	}
	.contactForm input:focus,
	.contactForm textarea:focus {
		border-color: <?php echo $color; ?>;
	}

</style>
*/




?>
<style>
	div.intro { background-color: <?php echo $couleur; ?>; }
	div.entry-content h3 { color: <?php echo $couleur; ?>; }
	div.entry-content h3:before { background-color: <?php echo $couleur; ?>; }
	div.barre-haut { background-color: <?php echo $couleur; ?>; }
	header.entry-header h1.entry-title {background-color: <?php echo $couleur; ?>;}
	#nav-below a {color: <?php echo $couleur; ?>;}
	.entry-content table tr:first-child td {background-color: <?php echo $couleur; ?>;}
	.entry-content td ul li:before { color: <?php echo $couleur; ?>; }
	.entry-content table, .entry-content th, .entry-content td { border-color: <?php echo $couleur; ?>; }
	.exergue,.entry-content table td {
		border: 1px solid <?php echo $couleur; ?>;
		background: <?php echo $lighter_color; ?>;
	}
	::selection {
		background: <?php echo $darken_color; ?>; color: #FFF;
	}
	blockquote:before, blockquote:after, body.single-post div#primary article .entry-content .highlight strong, body.single-post div#primary p strong, .entry-content li:before, .highlight { color: <?php echo $couleur; ?>; }
</style>


<!--
<?php echo get_field("surtitre"); ?>
<?php echo get_field("auteur"); ?>
<?php echo get_field("couleur"); ?>
-->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="barre-haut">
			<div class="category"><?php echo $category_list; ?></div>
			<div class="auteur"><?php if (trim($auteur) != '') { echo "Par " . $auteur; } ?></div>
			<br clear="all">
		</div>	
		<div class="feature-img" style="background-image:url('<?php echo $picture;?>');"></div>
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'pmeinter' ), 'after' => '</div>' ) ); ?>

	</div><!-- .entry-content -->

	<footer class="entry-meta">
		<?php
			/* translators: used between list items, there is a space after the comma */


			/* translators: used between list items, there is a space after the comma */
			//$tag_list = get_the_tag_list( '', __( ', ', 'pmeinter' ) );


			// printf(
			// 	$meta_text,
			// 	$category_list,
			// 	$tag_list,
			// 	get_permalink(),
			// 	the_title_attribute( 'echo=0' )
			// );
		?>

		<?php edit_post_link( __( 'Edit', 'pmeinter' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->
