<?php
/**
 * The template used for displaying page content in page.php
 *
 * Template Name: Carriere
 *
 * @package pmeinter
 * @since pmeinter 1.0
 */

get_header(); ?>

		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php
						$DB = new DB(DB_HOSTNAME_EXTRANET, DB_USER_EXTRANET, DB_PASSWORD_EXTRANET, DB_NAME_EXTRANET);

						ob_start();
						get_template_part( 'content', 'page' );
						$content = ob_get_contents();
						ob_end_clean();
						$filter = htmlspecialchars($_GET["ville"]);
						//AND es.ville = 'alma'
						//echo "le filtre est : ". $filter;
						//if($filter){
							$DB->query(
								"SELECT DISTINCT ee.*, e.nom, es.ville, e.courriel ".
								"FROM `etudes_emplois` ee INNER JOIN etudes e ON ee.etudes_key = e.key INNER JOIN etudes_succursales es ON e.key = es.etudes_key AND es.siege_social = 1 ".
								"WHERE ee.afficher = '1' ".
								"ORDER BY es.ville ASC, e.nom ASC, ee.date_entree DESC;"
							);
						//}else{
							
						// $DB->query(
						// 	"SELECT DISTINCT ee.*, e.nom, es.ville, e.courriel ".
						// 	"FROM `etudes_emplois` ee INNER JOIN etudes e ON ee.etudes_key = e.key INNER JOIN etudes_succursales es ON e.key = es.etudes_key AND es.siege_social = 1 ".
						// 	"WHERE ee.afficher = '1' AND es.ville = '$filter'".
						// 	"ORDER BY es.ville ASC, e.nom ASC, ee.date_entree DESC;"
						// );
						// }

						$html = "";
				$html .= '<div class="bannerCarrousel">

				<div class="bannerTop">
					<div class="bannerTag">
						<h1>Vous avez la fibre entreprenariale?</h1>
						<h2>Devenez notaire au sein du plus important réseau de notaires au Québec</h2>
						<a class="btn" href="#jobsBanner">Trouver une offre</a>
					</div>
				</div>
				<div class="bannerTop top2">
					<div class="bannerTag">
						<h1>Vous souhaitez évoluer
							au sein d’une équipe
							dynamique?</h1>
						<h2>Devenez technicien(ne) juridique au sein d’un bureau membre PME INTER Notaires</h2>
						<a class="btn" href="#jobsBanner">Trouver une offre</a>
					</div>
				</div>
			</div>
			<div class="bannerPastil">
				<div class="pastil"><img src="https://pmeinter.com/wp-content/uploads/2020/02/handshake-100.png" alt=""><div class="text">+ de 55 places d\'affaires</div></div>
				<div class="pastil"><img src="https://pmeinter.com/wp-content/uploads/2020/02/people-100.png" alt=""><div class="text">+ de 200 notaires</div></div>
				<div class="pastil"><img src="https://pmeinter.com/wp-content/uploads/2020/02/scales-100.png" alt=""><div class="text">+ de 500 professionnels du droit</div></div>
			</div>';
			

				$html .= '<div id="jobsBanner" class="bannerVideo"><div data-youtube="https://www.youtube.com/watch?v=Pb2uMKXdnZk"></div></div>';

				$html .= '<div id="jobsList"><div class="searchBox">
						<h2>Trouver une offre</h2><input class="search" placeholder="Entrez votre ville"/></div><ul class="list">';
						$last_city = "";
						$last_etude = "";

						while ($DB->next_record()) {
							$poste = stripslashes($DB->getField("poste"));
							$courriel = $DB->getField("courriel");
							$email = stripslashes($DB->getField("contact"));
							$date_entree = stripslashes($DB->getField("date_entree"));

							$email = preg_replace("/&nbsp;/", " ", $email);
							$email = preg_replace("/^.*?([a-z0-9\-\_\.]+\@[a-zA-Z0-9\-\_\.]+)(\s|<).*?$/is", "$1", $email);

							if (!preg_match("/\@/is", $email)) {
								$email = "info@pmeinter.com";
							}

							if ($last_city != $DB->getField("ville")) {
								if ($last_city != "") { $html .= "</ul>"; }
								$html .=
									"<li><h2 class=\"city\">" . stripslashes($DB->getField("ville")) . "</h2>\n".
									"<h4>" . stripslashes($DB->getField("nom")) . "</h4>\n".
									"<ul class=\"jobs\">\n";
							} else if ($last_etude != $DB->getField("nom")) {
								if ($last_etude != "") { $html .= "</ul>"; }
								$html .=
									"<br />".
									"<h4>" . stripslashes($DB->getField("nom")) . "</h4>\n".
									"<ul class=\"jobs\">\n";
							}

							$html .=
								"<li>\n".
								"	<a href=\"#\" class=\"job\">" . $poste . "</a>\n".
								"	<div class=\"description\">\n".
								//($poste == "Notaire" ?
										"<p><img src=\"/wp-content/themes/pmeinter/img/offre-d-emploi-notaire.jpg\" alt=\"Notaire\"/></p>" .
								//	"<p><img src=\"/wp-content/themes/pmeinter/img/offre-d-emploi-collaboratrice.jpg\" alt=\"Col\"/></p>").
								"		<div class=\"text\">\n".
								"			<a class=\"button\" href=\"#gform_wrapper_6\">Postuler <input type=\"hidden\" name=\"email_job\" class=\"email_job\" value=\"" . $email . "\" /><input type=\"hidden\" name=\"email_etude\" class=\"email_etude\" value=\"" . $courriel . "\" /></a>\n".
								"			<div class=\"titre\"><p><strong>Titre du poste :</strong></p>\n".
								"			<div>" . stripslashes($DB->getField("poste")) . "</div></div>\n".
								"			<div class=\"autre\"><br /><p><strong>Description et exigences requises :</strong></p>\n".
								"				" . stripslashes($DB->getField("exigences")) . "\n".
								"				<br /><p><strong>Rémunération et avantages :</strong></p>\n".
								"				<div>" . stripslashes($DB->getField("remuneration")) . "</div>\n".
								"				<br /><p><strong>Date d'entrée en fonction :</strong></p>\n".
								"				<div>" . stripslashes($DB->getField("date_entree")) . "</div>\n".
								"				<br /><p><strong>Adresse du contact :</strong></p>\n".
								"			<p>" . stripslashes($DB->getField("contact")) . "</p></div>\n".
								"	<a class=\"button\" href=\"#gform_wrapper_6\">Postuler <input type=\"hidden\" name=\"email_job\" class=\"email_job\" value=\"" . $email . "\" /><input type=\"hidden\" name=\"email_etude\" class=\"email_etude\" value=\"" . $courriel . "\" /></a>\n".
								"	</div><div class=\"form\"></div></div>\n".
								"</li>\n";

							$last_city = $DB->getField("ville");
							$last_etude = $DB->getField("nom");
						}

						if ($last_etude != "") { $html .= "</ul>"; }

						if ($html == "") {
							$html = "<p>Aucun poste n’est disponible pour le moment. Faites-nous parvenir votre CV que nous conserverons pour des besoins futurs.</p>";
						}
						$html .='</li></ul></div>';
						$html .= "<br />";

						$html = preg_replace("/\n\n/", " ", $html);
						$html = preg_replace("/<font[^>]+?>((&nbsp;\s*)+)?<\/font>/", "", $html);
						$html = preg_replace("/<span[^>]+?>((&nbsp;\s*)+)?<\/span>/", "", $html);
						$html = preg_replace("/<o:p[^>]+?>((&nbsp;\s*)+)?<\/o:p>/", "", $html);
						$html = preg_replace("/<p[^>]+?>((&nbsp;\s*)+)?<\/p>/", "", $html);
						$html = preg_replace("/<ul[^>]+?><\/ul>/", "", $html);
						$html = preg_replace("/<font[^>]+?>((&nbsp;\s*)+)?<\/font>/", "", $html);
						$html = preg_replace("/<span[^>]+?>((&nbsp;\s*)+)?<\/span>/", "", $html);
						$html = preg_replace("/<o:p[^>]+?>((&nbsp;\s*)+)?<\/o:p>/", "", $html);
						$html = preg_replace("/<p[^>]+?>((&nbsp;\s*)+)?<\/p>/", "", $html);
						$html = preg_replace("/<ul[^>]+?><\/ul>/", "", $html);
						$html = preg_replace("/<font[^>]+?>((&nbsp;\s*)+)?<\/font>/", "", $html);
						$html = preg_replace("/<span[^>]+?>((&nbsp;\s*)+)?<\/span>/", "", $html);
						$html = preg_replace("/<o:p[^>]+?>((&nbsp;\s*)+)?<\/o:p>/", "", $html);
						$html = preg_replace("/<p[^>]+?>((&nbsp;\s*)+)?<\/p>/", "", $html);
						$html = preg_replace("/<ul[^>]+?><\/ul>/", "", $html);


						$content = preg_replace("/{carriere_content}/", $html, $content);

						echo $content;
					?>


				<?php endwhile; // end of the loop. ?>
			<!-- listjs for a better sorting -->

<style>
	body{
		font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS',
	}
.bannerCarrousel{
	height: 400px;
	
}
.slick-list,.slick-track{height: 100%;}
.bannerTop{
	height: auto;
	background: url('https://pmeinter.com/wp-content/uploads/2020/02/duo.jpg');
	position: relative;
	background-position: center center;
	background-size:cover;
}
	
	@media screen and (min-width:768px){
		.bannerTop{
			height:600px;
		}
		.bannerCarrousel{
			height: 600px;
		}		
	}
.bannerVideo{
	height: 600px;
	width:100%;
	position:relative;
}
	
	
	@media screen and (max-width:1000px){
		.bannerVideo{
			height:415px;
		}
	}
	@media screen and (max-width:400px){
		.bannerVideo{
			height:155px;
		}
	}
	@media screen and (max-width:680px){
		.bannerVideo{
			height:255px;
		}
	}
.bannerTop .bannerTag{
	box-sizing: border-box;
	position:absolute;
	width:100%;
	padding: 20px;
	height: 100%;
	color:#fff;
	background-color: rgba(15, 36, 62, 0.5);
	display: flex;
	align-items: flex-start;
	justify-content: flex-end;
	flex-direction: column;
}
@media screen and (min-width: 1024px){
	.bannerTop .bannerTag{
		width:45%;
		padding: 40px;
	}
}
.top2{
	background-image: url('https://pmeinter.com/wp-content/uploads/2020/02/men-women-glass-e1582658982867.jpg');
}
.bannerTag .btn{
	background-color: #0F243E;
	color:#fff;
	border:none;
	box-shadow: none;
	text-shadow: none;
	text-transform: uppercase;
	padding:15px 20px;
	border-radius: 2px;
	outline: none;
}
.bannerTag .btn:hover{
	text-decoration: none;
	background-color: rgb(18, 47, 82);
	filter: drop-shadow(2px 2px 5px rgba(0,0,0,0.5));
	cursor: pointer;
}
.bannerPastil{
	display: flex;
	flex-direction: row;
	justify-content: space-evenly;
	align-items: center;
	padding: 100px 0;
	color:#000;
	
}
	@media screen and (max-width: 750px){
		.bannerPastil{
		flex-direction:column;
		}
		.pastil{
			margin:20px auto;
		}
	}
.pastil{
	background-color: grey;
	width: 200px;
	min-height: 200px;
	border-radius: 100%;
	box-sizing: border-box;
	padding: 30px;
	position: relative;
	display: flex;
	justify-content: space-evenly;
	flex-direction: column;
	align-items: center;
}
.pastil .text{
	text-align: center;
	color:#fff;
	line-height: 1em;
}
.pastil img{
	width: 50%;
	height: auto;
}

.bannerTop .bannerTag h1{
font-size: 28px; margin-bottom: 20px; text-transform: uppercase; line-height: 1em; color:#fff;}
.bannerTop .bannerTag h2{
	color:#fff;
	font-size:16px!important;
	line-height:1em!important;
}
	@media screen and (min-width: 768px){
		.bannerTop .bannerTag h1{
			font-size:40px;
			
		}
		.bannerTop .bannerTag h2{
			color:#fff;
			font-size:24px;
		}
	}


#jobsList{
	width: 100% ;
 	

}
#jobsList .searchBox{
	padding:60px 40px;
	background-color: #0F243E;
}
#jobsList .searchBox h2{
	color:#fff;
	margin-bottom: 20px;
}
#jobsList .searchBox input{
	font-size: 16px;
    padding: 8px;
    width: calc(100% - 20px);
    margin-top: -10px;
	}
	@media screen and (min-width:500px){
		#jobsList .searchBox input{
			width:300px;
		}
	}
#jobsList .list{
 list-style: none;
 margin-top: 30px;
 /* padding:40px; */
}

#jobsList .list li li{ padding:10px; background: transparent;}
	@media screen and (max-width: 768px){
		body.page-template-content-carriere-php div.entry-content{
			padding:40px 0;
		}
	}
	body.page-template-content-carriere-php ul.jobs li div.description{
		width:calc(100% - 60px)!important;
	}
	body.page-template-content-carriere-php a.button{
		margin:0 0 0 auto !important;
	}
</style>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/youtube-background@1.0.2/jquery.youtube-background.min.js"></script>
<script>

var options = {
    valueNames: [ 'city' ]
};

var hackerList = new List('jobsList', options);

jQuery('#jobsList .search').on('keyup',function(){
	if(jQuery('#jobsList .search').val() == ''){
		
		hackerList.search('')

	}
});

jQuery(document).ready(function(){
	var search = getUrlParameter("ville");
	hackerList.search(search, searchFunction);
});

function searchFunction(listObj) {

	var search = listObj.replaceAll("\\", "");
	var search_array = search.split(',');


	hackerList.items.map(item => {
		var nom_ville = item.values().city.toLowerCase();
		var index = nom_ville.indexOf('(');
		var substr = index != -1 ? nom_ville.substr(0, index) : nom_ville;


		item.found = false;
		search_array.map(search_item =>{
			if(search_item.includes(substr)){
				item.found = true;
			}
		});
	})

};

function getUrlParameter(name){
	name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}


jQuery('.bannerCarrousel').slick({
	autoplay:true,
	arrows:false,
	autoplaySpeed:5000,
	cssEase: 'ease-in-out'
});
	
jQuery('[data-youtube]').youtube_background();
</script>


			</div><!-- #content .site-content -->


		</div><!-- #primary .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
