<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package pmeinter
 * @since pmeinter 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<!--meta name="viewport" content="width=device-width" /-->
		<title><?php global $_CUSTOM_TITLE; echo $_CUSTOM_TITLE; wp_title( '|', true, 'right' ); ?></title>
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
		<link rel="stylesheet" id="style-css" href="/wp-content/themes/pmeinter/ie.css" type="text/css" media="all">
		<![endif]-->
		<!--[if IE 9]>
		<link rel="stylesheet" id="style-css" href="/wp-content/themes/pmeinter/ie9.css" type="text/css" media="all">
		<![endif]-->
		<?php
			wp_head();

			echo '<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">';
			echo '<link rel="stylesheet" id="mobile-css" href="/wp-content/themes/pmeinter/mobile.css" type="text/css" media="all">';
			echo '<link rel="stylesheet" id="mobile-css" href="/wp-content/themes/pmeinter/d2018.css" type="text/css" media="all">';
			echo '<link rel="stylesheet" id="mobile-css" href="/wp-content/themes/pmeinter/layout18.css" type="text/css" media="all">';

			echo '<meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />';

		?>
		<!--script src="https://malsup.github.com/jquery.cycle2.js"></script-->
		<script type="text/javascript" src="/wp-content/themes/pmeinter/js/script.js"></script>


		<meta name="google-site-verification" content="G48BuJpWlbCmO69irSfWx4m9tmldfunW0kRIY9BptGg" />
	</head>

	<?php
		$demo2018 = "";
		//if (isset($_COOKIE["DEMO2018"])) {
		$demo2018 = "demo2018";
		//}
	?>

	<body <?php body_class($demo2018); ?>>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=70896600733";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>


		<img src="/wp-content/themes/pmeinter/img/body-bg.jpg" alt="" id="body-bg" />
		<div id="page" class="hfeed site">
			<?php do_action( 'before' ); ?>
			<header id="masthead" class="site-header" role="banner">
				<hgroup>
					<!--h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1-->
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="/wp-content/themes/pmeinter/img/logo-pmeinter.jpg" alt="Logo PME Inter Notaires" id="logo-pmeinter" /></a>
					<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
				</hgroup>

				<?php
					if (ICL_LANGUAGE_CODE == 'en') {
						wp_nav_menu( array( 'theme_location' => 'primary', 'menu' => 'Top-EN' ) );
					} else {
						wp_nav_menu( array( 'theme_location' => 'primary', 'menu' => 'Top-FR' ) );
					}
				?>

				<div id="lang-switcher"><?php do_action('icl_language_selector'); ?></div>

				<?php
					if (ICL_LANGUAGE_CODE == 'en') {
						wp_nav_menu( array( 'theme_location' => 'primary', 'menu' => 'Thematiques-EN' ) );
					} else {
						wp_nav_menu( array( 'theme_location' => 'primary', 'menu' => 'Thematiques-FR' ) );
					}

				?>

				<nav role="navigation" class="site-navigation main-navigation">
					<h1 class="assistive-text"><span class="fa fa-bars">&nbsp;</span> <?php _e( 'Menu', 'pmeinter' ); ?></h1>
					<div class="assistive-text skip-link"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'pmeinter' ); ?>"><?php _e( 'Skip to content', 'pmeinter' ); ?></a></div>

					<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
				</nav><!-- .site-navigation .main-navigation -->
				<div class="phone-free"><span>Pour être dirigé au notaire le plus près de chez vous :</span> <a href="tel:1 866 321 0455">+1 866-321-0455</a></div>
			</header><!-- #masthead .site-header -->

			<div id="main" class="site-main <?php global $dynamic_class; echo $dynamic_class; ?>">

			<?php /*if (is_front_page()) { ?>

				<div class="social mobile" style="display: none;">
					<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: fr_FR</script>
					<script type="IN/FollowCompany" data-id="6607420"></script>

					<div class="fb-page" data-href="https://www.facebook.com/R&#xe9;seau-PME-INTER-Notaires-1444799325801880/" data-small-header="true" data-adapt-container-width="false" data-hide-cover="true" data-show-facepile="false"><blockquote cite="https://www.facebook.com/R&#xe9;seau-PME-INTER-Notaires-1444799325801880/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/R&#xe9;seau-PME-INTER-Notaires-1444799325801880/">Réseau PME INTER Notaires</a></blockquote></div>
				</div>

				<div id="slideshow">
					<div class="nav">
						<?php
							if (ICL_LANGUAGE_CODE == 'en') {
								wp_nav_menu( array( 'theme_location' => 'primary', 'menu' => 'Slideshow-EN' ) );
							} else {
								wp_nav_menu( array( 'theme_location' => 'primary', 'menu' => 'Slideshow-FR' ) );
							}
						?>
					</div>
					<ul class="photo">
						<!--li class="b1"><h3>Où l'équipe fait la force</h3><img src="/wp-content/themes/pmeinter/img/banners/1.jpg" alt="" /></li-->
						<li class="b2"><h3><span>Confiez</span>-nous vos tracas juridiques</h3><img src="/wp-content/themes/pmeinter/img/banners/2.jpg" alt="" /></li>
						<li class="b3"><h3>Confiez-nous la protection de votre patrimoine</h3><img src="/wp-content/themes/pmeinter/img/banners/3.jpg" alt="" /></li>
						<li class="b4"><h3>Pour réussir vos démarches de financement et vos transactions immobilières</h3><img src="/wp-content/themes/pmeinter/img/banners/4.jpg" alt="" /></li>
						<li class="b6"><h3>Des notaires spécialisés qui maîtrisent les particularités de votre situation</h3><img src="/wp-content/themes/pmeinter/img/banners/6.jpg" alt="" /></li>
						<li class="b5"><h3>Un savoir-faire collectif très spécialisé et hautement diversifié</h3><img src="/wp-content/themes/pmeinter/img/banners/5.jpg" alt="" /></li>
					</ul>
				</div>
				<div id="map">
					<h3><?php echo __("Vous cherchez un notaire?"); ?></h3>
					<div class="content">
						<form action="<?php echo __("/notaires"); ?>" method="post">
							<select id="citydd" name="city">
								<option value=""><?php echo __("Ville"); ?></option>
								<?php echo getCityDD(); ?>
							</select>
							<input type="text" id="scity" name="scity" value="" placeholder="<?php echo __("Notaire ou &Eacute;tude"); ?>" />
							<input type="submit" id="mapsubmit" value="OK" />
							<a href="<?php echo __("/notaires-carte"); ?>" id="linktomap"><?php echo __("Carte"); ?></a>
						</form>
					</div>
				</div>
			<?php }*/ ?>
