<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package pmeinter
 * @since pmeinter 1.0
 */
?>
		<div id="secondary" class="widget-area" role="complementary">

			<?php if (!isset($_COOKIE["DEMO2018"])) { ?>

				<div class="fb-page" data-href="https://www.facebook.com/R%C3%A9seau-PME-INTER-Notaires-1444799325801880/" data-tabs="timeline" data-width="300" data-height="1020" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/R%C3%A9seau-PME-INTER-Notaires-1444799325801880/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/R%C3%A9seau-PME-INTER-Notaires-1444799325801880/">Réseau PME INTER Notaires</a></blockquote></div>

			<?php } ?>

			<div id="souspages">
					<?php
						if ($post->post_parent != 0) {
							$children = wp_list_pages('title_li=&child_of='.$post->post_parent.'&echo=0&depth=1&sort_column=menu_order');
						} else {
							$children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0&depth=1');
						}
					?>
				  <?php if ($children) : ?>
				  <ul>
				  <?php echo $children; ?>
				  </ul>
	  			<?php endif ;?>
	  			<br />
			</div>

			<div id="sidenews">
				<div class="content"></div>
			</div>

			<div id="submenu-map">
				<ul>
					<li><a href="<?php echo __("/notaires-liste"); ?>"><?php echo __("Accès par liste"); ?></a></li>
					<li><a href="<?php echo __("/notaires-carte"); ?>"><?php echo __("Accès par carte"); ?></a></li>
				</ul>
				<br clear="all" />
			</div>

			<?php global $submenumap; echo $submenumap; ?>

			<?php
				global $nouvelles, $videos, $videos_ss;

				if (is_array($videos_ss) && count($videos_ss) > 0) {
					foreach ($videos_ss as $video) {
						echo "<div style=\"text-align: center; width: 298px; margin-left: 10px;\">" . $video . "</div>";
					}
				}

				if (is_array($videos) && count($videos) > 0) {
					foreach ($videos as $video) {
						echo "<div style=\"text-align: center; width: 298px; margin-left: 10px;\">" . $video . "</div>";
					}

				}

				if ($nouvelles != "" && !preg_match("/<ul>.*?<li><\/li>.*?<\/ul>/s", $nouvelles) && $nouvelles != "<ul></ul>") {
					echo "<div id=\"etude-nouvelles\"><h3>Nouvelles de l'heure</h3>" . $nouvelles . "</div>";
				}
			?>

			<?php do_action( 'before_sidebar' ); ?>
			<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>

				<aside id="search" class="widget widget_search">
					<?php get_search_form(); ?>
				</aside>

				<aside id="archives" class="widget">
					<h1 class="widget-title"><?php _e( 'Archives', 'pmeinter' ); ?></h1>
					<ul>
						<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
					</ul>
				</aside>

				<aside id="meta" class="widget">
					<h1 class="widget-title"><?php _e( 'Meta', 'pmeinter' ); ?></h1>
					<ul>
						<?php wp_register(); ?>
						<li><?php wp_loginout(); ?></li>
						<?php wp_meta(); ?>
					</ul>
				</aside>

			<?php endif; // end sidebar widget area ?>

			<div id="sidemap">
				<h3><?php echo __("Vous cherchez un notaire?"); ?></h3>
				<div class="content">
					<form action="<?php echo __("/notaires"); ?>" method="post">
						<select id="sidecitydd" name="city">
							<option value=""><?php echo __("Ville"); ?></option>
							<?php echo getCityDD(); ?>
						</select>
						<input type="text" id="sidescity" name="scity" value="" placeholder="<?php echo __("Notaire ou &Eacute;tude"); ?>" />
						<input type="submit" id="mapsubmit" value="OK">
						<!--select id="sidecpdd" name="cp">
							<option value=""><?php echo __("Champs de pratique"); ?></option>
							<?php echo getCPDD(); ?>
						</select-->
						<div class="action">
							<a href="<?php echo __("/notaires-carte"); ?>" id="linktomap"><?php echo __("Carte"); ?></a>
							<!--a href="<?php echo __("/notaires-liste"); ?>" id="sidelinktolist"><?php echo __("Liste"); ?></a-->
							<!--input type="submit" id="sidemapsubmit" value="<?php echo __("Chercher"); ?>" /-->
						</div>
					</form>
				</div>
			</div>
			<br />





		</div><!-- #secondary .widget-area -->
