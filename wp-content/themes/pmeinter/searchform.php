<?php
/**
 * The template for displaying search forms in pmeinter
 *
 * @package pmeinter
 * @since pmeinter 1.0
 */
?>
	<form method="post" id="searchformpmei" action="/notaires" role="search">
		<label for="s" class="assistive-text"><?php _e( 'Search', 'pmeinter' ); ?></label>
		<input type="text" class="field" name="scity" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php __(esc_attr_e( 'Search &hellip;', 'pmeinter' )); ?>" />
		<input type="submit" class="submit" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'pmeinter' ); ?>" />
	</form>
