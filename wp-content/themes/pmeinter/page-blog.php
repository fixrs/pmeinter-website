<?php
/**
 * The template used for displaying page content in page.php
 *
 * Template Name: Blogue
 *
 * @package pmeinter
 * @since pmeinter 1.0
 */

get_header(); ?>

    <nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
        <?php if (is_single()): ?>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="back"><span class="icon icon-arrow-1"></span> Retour aux articles</a>
            <div class="share">
                Partager l’article

                <?php
                $title = get_the_title();
                $link = get_permalink();
                $img =  wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
                $img = $img[0];
                ?>

                <a href="http://www.facebook.com/share.php?u=<?php echo $link; ?>" target="_blank"><span class="icon icon-facebook"></span></a>
                <a href="https://twitter.com/share?url=<?php echo $link; ?>&amp;text=<?php echo $title; ?>" target="_blank"><span class="icon icon-twitter"></span></a>
                <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $link; ?>&title=<?php echo $title; ?>" target="_blank"><span class="icon icon-linkedin"></span></a>
                <a href="http://pinterest.com/pin/create/button/?url=<?php echo $link; ?>&media=<?php echo $img; ?>" target="_blank"><span class="icon icon-pinterest"></span></a>
                <a href="https://plus.google.com/share?url=<?php echo $link; ?>" target="_blank"><span class="icon icon-google" ></span></a>
                <a href="mailto:?subject=Je%20vous%20recommande%20cet%20article%20!&body=<?php echo $title; ?> - <?php echo $link; ?>" target="_blank"><span class="icon icon-email"></span></a>
            </div>
        <?php elseif(is_home() || is_search()): ?>
            <h2>Filtres</h2>
            <ul>
                <li class="active"><a data-filter="*">Tout</a></li>
                <?php
                $args = array(
                    'exclude' => array(1)
                );
                $cats = get_terms( 'category', $args );

                foreach ($cats as $cat) { ?>
                    <li><a data-filter="<?php echo $cat->term_id; ?>"><?php echo $cat->name; ?></a></li>
                <?php }

                ?>
            </ul>

            <div class="btn">
                <a class="more">+</a>
                <a class="close"><span class="icon icon-close"></span></a>
            </div>
        <?php endif; ?>
    </nav>


    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
            <ul>
            <?php
                $numPosts = (isset($_GET['numPosts'])) ? $_GET['numPosts'] : 18;
                $cat = (isset($_GET['cat'])) ? $_GET['cat'] : '';
                $featCount = (isset($_GET['featCount'])) ? $_GET['featCount'] : 6;
                $ids = (isset($_GET['ids'])) ? $_GET['ids'] : '';
                ?>

                <li>

                <?php

                /* -------------------------------------------------------------------------- */
                                   /** ∆∆      FEATURED      ∆∆ **/
                /* -------------------------------------------------------------------------- */

                $argsf = array(
                    'posts_per_page' => 1,
                    'post_status' => 'publish',
                    'orderby' => 'date',

                    'meta_query' => array(
                        array(
                            'key' => 'en_vedette',
                            'value' => '1',
                            'compare' => 'LIKE'
                        )
                    )
                );

                if (!empty($cat)) {
                    $argsf['category__in'] = $cat;
                }

                if (!empty($ids)) {
                    $argsf['post__in'] = $ids;
                }

                $queryf = new WP_Query($argsf);
                $featID = array();

                if( $queryf->have_posts() ) { ?>
                    <?php while ( $queryf->have_posts() ) : $queryf->the_post();
                        $featID = get_the_id();
                        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
                    ?>
                        <article class="featured">
                            <a href="<?php the_permalink(); ?>" class="inner">
                                <span class="bg img" style="background-image: url(<?php echo $thumb['0']; ?>)"></span>
                                <span class="bg color" style="background: <?php echo get_field("couleur") ?>"></span>

                                <hgroup>
                                    <h2><?php echo get_field("surtitre"); ?></h2>
                                    <h1><?php the_title(); ?></h1>
                                    <span class="auteur">par <?php echo get_field("auteur"); ?></span>


                                    <div class="intro"><?php
                                        $classname = 'intro';
                                        $dom = new DOMDocument;
                                        $dom->loadHTML(get_the_content());
                                        $xpath = new DOMXPath($dom);
                                        $results = $xpath->query("//*[@class='" . $classname . "']");

                                        if ($results->length > 0) {
                                            echo utf8_decode($results->item(0)->nodeValue);
                                        }
                                        ?>
                                    </div>
                                </hgroup>
                                <span class="more">Lire la suite <span class="icon icon-arrow-2"></span></span>
                            </a>
                        </article>
                    <?php endwhile; ?>
                <?php } else {
                    $featCount = 0;
                } ?>

                <?php wp_reset_query();


                /* -------------------------------------------------------------------------- */
                                   /** ∆∆      ARTICLES      ∆∆ **/
                /* -------------------------------------------------------------------------- */

                $args = array(
                  'posts_per_page' => -1,
                  'orderby' => 'date',
                  'order' => 'ASC',
                  'post_status' => 'publish'
                );

                if (!empty($cat)) {
                    $args['category__in'] = $cat;
                }

                if (!empty($ids)) {
                    if(($key = array_search($featID, $ids)) !== false) {
                        unset($ids[$key]);
                    }
                    $args['post__in'] = $ids;
                } else {
                    $args['post__not_in'] = array($featID);
                }

                $wp_query = new WP_Query( $args );

                $i = $featCount + 1;
                while ( $wp_query->have_posts() ) : $wp_query->the_post();
                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );

                    if (is_array($thumb) && $thumb['0'] != "") {
                ?>
                    <article>
                        <a href="<?php the_permalink(); ?>" class="inner">
                            <span class="bg img" style="background-image: url(<?php echo $thumb['0']; ?>)"></span>
                            <span class="bg color" style="background: <?php echo get_field("couleur") ?>"></span>

                            <h2><?php echo get_field("surtitre"); ?></h2>
                            <h1><?php the_title(); ?></h1>

                            <?php
                            $auteur = trim(get_field('auteur'));
                            if (!empty($auteur)) : ?>
                                <span class="auteur"><?php echo __("Par ") . $auteur; ?></span>
                            <?php endif; ?>
                        </a>
                    </article>

                    <?php
                    }
                    if ( $i != 1 && $i % $numPosts == 0) : ?>
                        </li><li>
                    <?php endif; ?>

                    <?php $i++; ?>
                <?php endwhile; ?>

                </li>
                <?php wp_reset_query(); ?>

            </ul>
            <br clear="all" />
        </div><!-- #content .site-content -->
    </div><!-- #primary .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
