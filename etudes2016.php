<?php
    require('wp-load.php');

    $show_new = true;

    $NORMALIZED_SORTING_TABLE = array(
        'á' => 'a', 'à' => 'a', 'â' => 'a', 'ä' => 'a', 'ã' => 'a', 'å' => 'a',
        'Á' => 'A', 'À' => 'A', 'Â' => 'A', 'Ä' => 'A', 'Ã' => 'A', 'Å' => 'A',
        'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ȩ' => 'e',
        'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ȩ' => 'E',
        'í' => 'i', 'ì' => 'i', 'î' => 'i', 'ï' => 'i',
        'Í' => 'I', 'Ì' => 'I', 'Î' => 'I', 'Ï' => 'I',
        'ó' => 'o', 'ò' => 'o', 'ô' => 'o', 'ö' => 'o', 'õ' => 'o',
        'Ó' => 'O', 'Ò' => 'O', 'Ô' => 'O', 'Ö' => 'O', 'Õ' => 'O',
        'ú' => 'u', 'ù' => 'u', 'û' => 'u', 'ü' => 'u',
        'Ú' => 'U', 'Ù' => 'U', 'Û' => 'U', 'Ü' => 'U',
        'ñ' => 'n', 'Ñ' => 'N', 'ç' => 'c', 'Ç' => 'C', 'ý' => 'y', 'Ý' => 'Y',
        'æ' => 'ae', 'Æ' => 'AE', 'œ' => 'oe'
    );

    foreach ($NORMALIZED_SORTING_TABLE as $key => $value) {
        $NORMALIZED_SORTING_TABLE_UTF8[$key] = $value;
    }

    //PRISE DE RENDEZ-VOUS
    //information de contact et liens reseau sociaux
    //document utile - knowledge


    // Mise en valeur de l'equipe
    // Mise en valeur de l'etude - nouvelle - banniere
    // Mise en valeur de l'emplacement Google map
    // Inscription bulletin

    $_CUSTOM_TITLE = "";
    $html = "";
    $title = "";
    $submenumap = "";
    $nouvelles = "";

    $DB = new DB(DB_HOSTNAME_EXTRANET, DB_USER_EXTRANET, DB_PASSWORD_EXTRANET, DB_NAME_EXTRANET);

    if ($_GET["etude_dd"] == 1) {
        $DB->query(
            "SELECT ".
            "   e.nom AS nom, ".
            "   e.courriel AS courriel, ".
            "   es.ville AS ville, ".
            "   es.code_postal AS code_postal ".
            "FROM ".
            "   `etudes` AS e, ".
            "   `etudes_succursales` AS es ".
            "WHERE ".
            "   es.etudes_key = e.key ".
            "   AND es.actif = '1' ".
            "   AND e.actif = '1' ".
            "ORDER BY ".
            "   ville ASC, ".
            "   nom ASC, ".
            "   code_postal ASC;"
        );

        $dd = "";
        $succursale_ville_prev = "";
        while ($record = $DB->next_record()) {
            $etude_nom = $DB->getField('nom');
            $etude_courriel = $DB->getField('courriel');
            $succursale_ville = $DB->getField('ville');
            $succursale_code_postal = $DB->getField('code_postal');

            if ($succursale_ville != $succursale_ville_prev) {
                if ($succursale_ville_prev != "") {
                    $dd .=  "</optgroup>";
                }
                $dd .= "<optgroup label=\"" . toHtml($succursale_ville) . "\">";
            }

            $succursale_ville_prev = $succursale_ville;

            $dd .= "<option value=\"" . $etude_courriel . "\" " . $selected . ">" . toHtml($etude_nom . " [" . $succursale_code_postal . "]") . "</option>";
        }

        echo "<option value=\"\"></option>" . $dd;

        exit;
    }


    add_filter('body_class','dynamic_body_class');

    if (count($_POST) > 0) {
        $title = __("Trouver un <span>notaire</span>");
        $html .= "<h2>" . __("Recherche") . "</h2>";

        if ($_POST['scity'] != "") {
            $html = getEtudesByKeyword($_POST["scity"]);
            $show_new = false;
        } else if ($_POST["cp"] != "") {
            $html = getEtudesByKeyword($_POST["scity"]);
            $show_new = false;
        } else if ($_POST["city"] != "") {
            //ON CONNAIT LA VILLE
            $html = getEtudesByCity($_POST["city"]);
        }

        if (trim($html) == "") {
            $html = "<p>Votre requête n'a retourné aucune étude.</p>";
        }
    } else if (count($_GET) > 0) {
        if ($_GET["p"] == "equipe" || $_GET["p"] == "demande" || $_GET["p"] == "rendez-vous" || $_GET["e"] != "") {
            //ON CONNAIT L'ETUDE ET ON VEUT L'EQUIPE
            global $title;

            $html = getEtude(getEtudeIDByURL($_GET["e"]));

            $html .= "<div class=\"formulaire\">" . getDemandeInfo($_GET["e"], "rendez-vous") . "</div>";
            //$html = getDemandeInfo($_GET["e"]);

        } else if ($_GET["v"] != "") {
            //ON CONNAIT LA VILLE
            $html = getEtudesByCity($_GET["v"]);
            $show_new = false;

        } else if ($_GET["l"] == 1) {
            //ACCES PAR LISTE
            $title = __("Trouver un <span>notaire</span>");
            $html .= "<h2>" . __("Accès par liste") . "</h2>";

            $html .= getEtudesList();

            $show_new = false;

        } else if ($_GET["c"] == 1) {
            //ACCES PAR CARTE

            if ($_GET["p"] == "rv") {
                $title = __("Prendre <span>rendez-vous</span>");
                $html .= "<h2>" . __("Accès par carte") . "</h2>";
                $html .= getEtudesMap("rv");
            } else if ($_GET["p"] == "di") {
                $title = __("Demande <span>d'informations</span>");
                $html .= "<h2>" . __("Accès par carte") . "</h2>";
                $html .= getEtudesMap("di");
            } else if ($_GET["p"] == "stage") {
                $title = __("Prendre <span>rendez-vous</span>");
                $html .= "<h2>" . __("Accès par carte") . "</h2>";
                $html .= getEtudesMap("rv");
            } else {
                $title = __("Trouver un <span>notaire</span>");
                $html .= "<h2>" . __("Accès par carte") . "</h2>";
                $html .= getEtudesMap();
            }

            $show_new = false;
        }
    } else {

    }

    $dynamic_class = "page-dynamique";

    ob_start();
    get_header();
    $header = ob_get_contents();
    ob_end_clean();

    if ($show_new) {
        $header = preg_replace("/<\/head>/is", "<meta name=\"description\" content=\"" . $meta_description . "\" /><meta name=\"theme-color\" content=\"#CCCCCC\" /><link rel=\"stylesheet\" href=\"/wp-content/themes/pmeinter/one-pager/css/styles.css\" /><link rel='stylesheet' href='/wp-content/themes/pmeinter/custom.css?ver=4.5.3' type='text/css' media='all' /><script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCU1f6duHlMUlUxvZMhUgQRpWn-eA27xu8\"></script>" . $canonical . "</head>", $header);
    } else {
        $header = preg_replace("/<\/head>/is", "<link rel='stylesheet' href='/wp-content/themes/pmeinter/custom.css?ver=4.5.3' type='text/css' media='all' /><script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCU1f6duHlMUlUxvZMhUgQRpWn-eA27xu8\"></script></head>", $header);
    }

    $header = preg_replace("/(<body[^>]*>)/is", "$1 <div id=\"fb-root\"></div><script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = \"//connect.facebook.net/fr_CA/sdk.js#xfbml=1&version=v2.7&appId=147422408630352\"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));</script>", $header);


    echo $header;



    if (!$show_new) {
        echo
            "<div id=\"primary\" class=\"content-area\">\n".
            "   <div id=\"content\" class=\"site-content\" role=\"main\">\n".
            "       <div class=\"page\">\n".
            "           <header class=\"entry-header\">\n".
            "               <h1 class=\"entry-title\">" . $title . "</h1>\n".
            "           </header>\n".
            "           <div class=\"entry-content etude\">\n".
            $html .
            "           </div>\n".
            "       </div>\n".
            "   </div> \n".
            "</div>\n";
    } else {

        $videobg = "";
        $banner_css = "";
        $title_color = " ";
        //if ($_SERVER["REMOTE_ADDR"] == "184.163.76.115" || $_SERVER["REMOTE_ADDR"] == "184.161.81.44" || $_SERVER["REMOTE_ADDR"] == "208.71.10.126") {

        // if ($_SERVER["REMOTE_ADDR"] == "70.83.151.25") {
           $videobg = '
                        <!--script src="//malsup.github.com/jquery.cycle2.js"></script-->
                        <div id="banner-etudes" class="cycle-slideshow" data-cycle-fx="fadeout" data-cycle-pause-on-hover="false" data-cycle-speed="4000">
                            <img src="/wp-content/themes/pmeinter/img/banners/banner2018-1.jpg" />
                            <img src="/wp-content/themes/pmeinter/img/banners/banner2018-2.jpg" />
                            <img src="/wp-content/themes/pmeinter/img/banners/banner2018-3.jpg" />
                        </div>';
        // } else {
        //    $videobg = '<video playsinline autoplay muted loop poster="' . $banner . '" id="bgvid"><source src="/bg.mp4?s=1" type="video/mp4"><source src="/bg.WebM?s=1" type="video/webm"></video>';
        //}

        if (trim($banner)) {
            $videobg = '<div id="banner-etudes"></div>';
        }

        //$banner = "";
          $banner_css = "overflow: hidden; background: none; height: 500px; width: 100%; position: relative;";
          $title_color = " style=\"color: #FFF;\" ";
        //}

        echo
            '<a id="back-top" class="fa fa-arrow-up" href="#"><span>Haut</span></a>'.
            '<div class="banner" ' . (trim($banner) != "" ? " style=\"background-image: url('" . $banner . "'); height: 500px;  \" " : " style=\"" . $banner_css . "\" ") . '>
            ' . $videobg . '
            <div class="top-banner-container">
              <div class="header">
                <div class="header-inner container clear">
                  <a class="logo" href="/"><img src="/wp-content/themes/pmeinter/img/logo-pmeinter.jpg" alt="Logo PME Inter Notaires" id="logo-pmeinter"></a>
                  <input type="checkbox" id="navigation-toggle-checkbox" name="navigation-toggle-checkbox" class="navigation-toggle-checkbox none">
                  <label for="navigation-toggle-checkbox" class="navigation-toggle-label" onclick>
                    <span class="navigation-toggle-label-inner">
                      <span class="sr">Navigation</span>
                    </span>
                  </label>
                  <div class="navigation">
                    <ul class="navigation-menu">
                      <!--li class="navigation-item"><a href="#home">PME Inter</a></li-->
                      <li class="navigation-item"><a href="#services">Nos services</a></li>
                      <li class="navigation-item"><a href="#about">À propos</a></li>
                      <li class="navigation-item"><a href="#news">Nouvelles</a></li>
                      <li class="navigation-item"><a href="#lcontact">Contactez-nous</a></li>
                      <li class="navigation-item"><a href="#contact">Prenez rendez-vous</a></li>
                    </ul>
                    <ul class="header-social-list icon-list-inline">
                      <li class="navigation-item-social"><a class="social-icon social-linkedin" href="' . $linkedin . '" target="_blank"><span class="sr">LinkedIn</span></a></li>
                      <li class="navigation-item-social"><a class="social-icon social-facebook" href="' . $facebook . '" target="_blank"><span class="sr">Facebook</span></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="container">
                <div class="banner-inner">
                  <h1 class="banner-lead">
                    <span class="banner-lead-1" ' . $title_color . '>' . $title . '</span>
                    <!--span class="banner-lead-2">' . $cities . '</span-->
                  </h1>
                  <div class="banner-content col-8-m no-float-m">
                    <p>' . $slogan . '</p>
                  </div>
                  <div class="banner-buttons">
                    <a href="#services" class="button button-primary">Nos services</a>
                    <a href="#about" class="button button-secondary">Notre équipe</a>
                  </div>
                  <div class="ss-phone">
                    <div class="mtl"><a href="tel:'. $telephone . '">' . $telephone . '</a></div>
                    <div class="toll-free"><a >' . $fax . '</a> <small>télécopieur</small></div>
                  </div>
                </div>
              </div>
              </div>
            </div>


            <div id="services" class="animate-block content-block services-block">
              <div class="services-block-inner container">
                <div class="content-block-inner text-center">
                  <h2 class="uppercase pad-bottom-20">Nos services</h2>
                  <div class="col-10-l no-float center">
                    <p>' . (trim($intro_services) != '' ? $intro_services : 'PME INTER Notaires jouit d’une solide notoriété dans tous les domaines du paysage juridique au Québec : le droit de la personne, le droit immobilier, le droit des affaires et le droit agricole. Pour éviter les pièges et pour faire des choix éclairés, que ce soit en immobilier pour l’achat ou la vente de votre maison ou lors des moments importants de votre vie tels que le mariage, contactez l’un des conseillers juridiques du plus grand réseau de notaires au Québec.') . '</p>
                  </div>
                </div>
                <div class="col-6 with-facebook">
                <ul class="services-list clear">'.

                (!$hide_droit_affaires ?
                  '<li class="service-item col-6 col-3-m">
                    <h3 class="service-item-heading service-icon service-icon-performance"><a href="/entreprises/droit-des-affaires/">Droit des affaires</a></h3>
                    <p>
                      ' . (trim($droit_affaires) != "" ? $droit_affaires : "Pour l’organisation de votre entreprise, vos relations entre actionnaires et vos transactions juridiques...") . '
                    </p>
                  </li>' : "") .

                (!$hide_droit_personne ?
                  '<li class="service-item col-6 col-3-m">
                    <h3 class="service-item-heading service-icon service-icon-customer"><a href="/particuliers/droit-de-la-personne/">Droit de la personne</a></h3>
                    <p>
                      ' . (trim($droit_personne) != "" ? $droit_personne : "Un service personnalisé, une approche honnête et un savoir-faire exemplaire, le notaire PME INTER Notaires, votre conseiller de toute une vie.") . '
                    </p>
                  </li>' : "").

                (!$hide_droit_immobilier ?
                  '<li class="service-item col-6 col-3-m">
                    <h3 class="service-item-heading service-icon service-icon-it"><a href="/particuliers/droit-immobilier/">Droit immobilier</a></h3>
                    <p>
                      ' . (trim($droit_immobilier) != "" ? $droit_immobilier : "Nous assurons la réussite de vos démarches de financement et de vos transactions immobilières dans les secteurs résidentiel, commercial, industriel et agricole.") . '
                    </p>
                  </li>' : "") .

                (!$hide_droit_agricole ?
                  '<li class="service-item col-6 col-3-m">
                    <h3 class="service-item-heading service-icon service-icon-transformation"><a href="/entreprises/droit-agricole/">Droit agricole</a></h3>
                    <p>
                      ' . (trim($droit_agricole) != "" ? $droit_agricole : "Que ce soit pour l'éclosion de nouveaux projets, leur fructification ou encore pour les transmettre à la future génération, nous sommes là pour vous assister dans ces transactions importantes, résultats d’efforts de toute une vie.") . '
                    </p>
                  </li>' : "").


                (trim($new_service_title) != "" ?
                  '<li class="service-item col-6 col-3-m">
                    <h3 class="service-item-heading" style="margin-top: 110px;"><a href="#">' . $new_service_title . '</a></h3>
                    <p>' . $new_service_description . '</p>
                  </li>' : "").

                '</ul>
                </div><div class="col-6">
                ' . (trim($facebook) != '' ? "<br /><br /><div class=\"fb-page\" data-href=\"" . $facebook . "\" data-tabs=\"timeline\" data-height=\"600\" data-small-header=\"false\" data-adapt-container-width=\"true\" data-hide-cover=\"false\" data-show-facepile=\"true\"><blockquote cite=\"" . $facebook . "\" class=\"fb-xfbml-parse-ignore\"><a href=\"" . $facebook . "\">" . $title . "</a></blockquote></div>" : "<br /><br /><div class=\"fb-page\" data-href=\"https://www.facebook.com/R%C3%A9seau-PME-INTER-Notaires-1444799325801880/\" data-tabs=\"timeline\" data-height=\"600\" data-small-header=\"false\" data-adapt-container-width=\"true\" data-hide-cover=\"false\" data-show-facepile=\"true\"><blockquote cite=\"https://www.facebook.com/R%C3%A9seau-PME-INTER-Notaires-1444799325801880/\" class=\"fb-xfbml-parse-ignore\"><a href=\"https://www.facebook.com/R%C3%A9seau-PME-INTER-Notaires-1444799325801880/\">" . $title . "</a></blockquote></div>") . '


                </div>
                <br clear="all" />
              </div>
            </div>

            <div class="animate-block feature-block contact-feature-block">
              <div class="feature-block-inner container">
                <div class="text-center">
                  <h2 class="feature-block-heading">Prenez un rendez-vous avec un membre de notre équipe!</h2>
                  <a class="button button-primary" href="#contact">Contactez-nous</a>
                </div>
              </div>
            </div>

            <div id="about" class="content-block about-block">
              <div class="about-block-inner container">
                <div class="animate-block content-block-inner text-center">
                  <h2 class="uppercase pad-bottom-20">À propos de nous</h2>
                  <!--div class="col-10-l no-float center"></div-->
                </div>
                ' .
                (
                  !preg_match("/<img/is", $nos_experts) ?
                  '<div class="animate-block about-content clear row-m">
                      <div class="what-we-do-block col-12-m pad-top-20">
                        <h3 class="heading fs-3">Mission</h3>
                        ' . $description . '
                      </div>
                    </div>'
                  :
                    '<div class="animate-block about-content clear row-m">
                      <div class="what-we-do-block col-6-m pad-top-20">
                        <h3 class="heading fs-3">Mission</h3>
                        ' . $description . '
                      </div>
                      <div class="our-clients-block col-6-m clear pad-top-20">
                        <h3 class="heading fs-3">Nos expert(e)s</h3>
                        ' . $nos_experts . '
                        </div>
                    </div>'

                  ).


                '<div class="animate-block our-team-block pad-top-20">
                  <h3 class="heading fs-3">Notre équipe</h3>
                  <ul id="team" class="team-list clear">
                    ' . $equipe_liste . '
                  </ul>
                </div>

                <div class="team-profiles-block">
                  ' . $equipe_bio_liste . '
                </div>
                </div>
              </div>
            </div>

            <!--div id="video-feature-block" class="feature-block video-feature-block">
              <div class="video-feature-inner container">
                <div class="animate-block content-block-inner text-center">
                  <a id="video" class="button button-play" href="#feature-video"><span class="sr">Jouer</span></a>
                  <h2 class="feature-block-heading">We blend the strategic with creative</h2>
                  <div class="col-10-l no-float center">
                    <p>
                     Est-ce que la publicité vidéo est disponible quelque part
                    </p>
                  </div>
                </div>
                <div class="modal" id="feature-video">
                  <a href="#video" class="modal-close"><span class="sr">Fermer</span></a>
                  <div class="modal-inner">
                    <p>
                      Description de la vidéo
                    </p>
                  </div>
                </div>
              </div>
            </div-->

            ' . $map_section . '
            ' . (trim($nouvelles) != '' ?
            '<div id="news" class="animate-block content-block news-block">
              <div class="news-block-inner container">
                <div class="content-block-inner text-center">
                  <h2 class="uppercase pad-bottom-20">Nouvelles</h2>
                  <div class="col-10-l no-float center">
                    <p>
                      Notre équipe de professionnels chevronnés offre un service hautement personnalisé, axé sur la confiance et la compétence.
                    </p>
                  </div>
                </div>
                ' . $nouvelles . '
              </div>
            </div>' : '') .

            '<div id="contact" class="animate-block content-block contact-block">
              <div class="contact-block-inner container">
                <div class="clear">
                  <div class="contact-block-content col-6-m">
                    <h2 class="fs-2 heading">' . $title . '</h2>
                    <p>N\'hésitez pas à nous contacter et à prendre rendez-vous avec notre équipe.</p>
                    <ul class="contact-list">
                      <li><a href="https://www.google.ca/maps/place/' . $address . '" target="_blank"><span class="icon contact-icon fa fa-map-marker">' . $address . '</span></a></li>
                      <li><span class="icon contact-icon fa fa-phone">' . $telephone . '</span></li>
                      <li><span class="icon contact-icon fa fa-fax">' . $fax . '</span></li>
                      ' .($accessible == 1 ? '<li class="accessible"><span><i class="fa fa-wheelchair"></i></span></li>' : '') . '
                    </ul>


                    <div class="heures_ouverture"><br />' . (trim($heures_ouverture) != "" ? "<h2 class=\"fs-2 heading\">Heures d'ouverture</h2>" . $heures_ouverture : "") . '</div>
                    <div class="mode_paiement"><br />' . (trim($mode_paiement) != "" ? "<h2 class=\"fs-2 heading\">Modes de paiement</h2>" . $mode_paiement : "") . '</div>
                    ' . '<div class="mode_paiement"><br /><a href="/carriere/?ville=' . get_etudes_filter() . '" target="_blank"><strong style="color: #FFF;">Visitez notre page carrières!</strong></a></div>' .
                  ' 
                  </div>
                  <div class="contact-block-form col-6-m">
                    <h2 class="fs-2 heading">Prenez rendez-vous</h2>
                    <p>
                      Veuillez spécifier quel service répondrait le mieux à votre besoin.
                    </p>

                    <a class="button button-primary show-form">Afficher le formulaire</a>

                    ' . $formulaire . '
                  </div>
                  <!--div class="contact-block-form-show-hide col-12-m"></div-->
                </div>
              </div>
            </div>
            <div class="footer">
              <div class="footer-inner container">
                <div class="clear">
                  <div class="footer-column col-8-m">
                    <p>
                      &copy; Copyright ' . date('Y') . ' &mdash; Tous droits réservés
                    </p>
                  </div>
                  <div class="footer-column col-4-m">
                    <ul class="footer-social-list icon-list-inline">
                      <li class="navigation-item-social"><a class="social-icon social-linkedin" href="' . $linkedin . '"><span class="sr">LinkedIn</span></a></li>
                      <li class="navigation-item-social"><a class="social-icon social-facebook" href="' . $facebook . '"><span class="sr">Facebook</span></a></li>
                      <!--li class="navigation-item-social"><a class="social-icon social-twitter" href="' . $twitter . '"><span class="sr">Twitter</span></a></li-->
                      <!--li class="navigation-item-social"><a class="social-icon social-youtube" href="#"><span class="sr">YouTube</span></a></li-->
                    </ul>
                  </div>
                </div>
              </div>
            </div>'.
            "<script src=\"/wp-content/themes/pmeinter/one-pager/js/vendor/wow.js\"></script><script src=\"/wp-content/themes/pmeinter/one-pager/js/default.js\"></script>";

    }





    //get_sidebar();
    get_footer();

    function dynamic_body_class() {
        global $show_new;

        $demo2018 = "";
        //if (isset($_COOKIE["DEMO2018"])) {
            $classes[] = "demo2018";
        //}

        if (isset($_GET["e"])) {
            $classes[] = "e" . $_GET["e"];
        }

        $classes[] = "e2016";

        if ($show_new) {
            $classes[] = " op2016";
        }


        if (count($_POST) > 0) {
            $classes[] = "recherche";
            if ($_POST['scity'] != "") {
                $classes[] = "q-city";
            } else if ($_POST['cp'] != "") {
                $classes[] = "q-city";
            } else if ($_POST["city"] != "") {
                $classes[] = "city";
            }
        } else if (count($_GET) > 0) {
            if ($_GET["p"] == "equipe") {
                $classes[] = "equipe";
            } else if ($_GET["p"] == "rendez-vous") {
                $classes[] = "demande-rv";
            } else if ($_GET["p"] == "demande") {
                $classes[] = "demande-info";
            } else if ($_GET["e"] != "") {
                $classes[] = "etude";
            } else if ($_GET["v"] != "") {
                $classes[] = "city";
            } else if ($_GET["l"] == 1) {
                $classes[] = "city-list";
            } else if ($_GET["c"] == 1) {
                $classes[] = "map";
            }
        }
        return $classes;
    }

    function getDemandeInfo($etude, $form = "") {
        global $DB, $_CUSTOM_TITLE, $title, $nouvelles, $submenumap, $show_new, $formulaire;

        $DB->query(
            "SELECT e.*, es.ville, es.courriel_etudes_succursales ".
            "FROM etudes e ".
            "INNER JOIN etudes_succursales es ON e.key = es.etudes_key ".
            "WHERE e.nombre_notaires_associes > 0 ".
            "AND es.siege_social = '1' ".
            "AND es.actif = '1' ".
            "AND e.actif = '1' "
        );


        $rows = $DB->getNumRows();
        $email = "";
        $title_h2 = "";
        $etude_id = "";

        $etude_url = "";
        $equipe_url = "";
        $notaires_demande_url = "";
        $html = "";
        $description = "";

        while ($DB->next_record()) {
            if (addslashes(strtolower($etude)) == asciionly($DB->getField("nom"))) {

                if (trim($etude_url . $equipe_url . $notaires_demande_url) == "") {
                    $etude_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom"));
                    $equipe_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "/equipe";
                    $notaires_demande_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "/demande-information";
                    //$html .= "<h5>" . __("Notre <span>Étude</span>") . "</h5>";
                }

                $_CUSTOM_TITLE = $DB->getField('nom') . " ";

                if ($form == "rendez-vous") {
                    //$title = "Prendre rendez-vous";
                } else {
                    //$title = "Demande d'information";
                }

                $title_h2 = $DB->getField("nom");

                if (strpos($DB->getField("courriel_etudes_succursales"), "@")) {
                    $email = $DB->getField("courriel_etudes_succursales");
                } else {
                    $email = $DB->getField("courriel");
                }
                $etude_id = $DB->getField("key");
            }
        }

        if (trim($email) == "") { $email = "info@pmeinter.com"; }

        ob_start();
        if ($form == "rendez-vous") {
            gravity_form(4, false, true, false, array('etude' => $etude, 'etude_courriel' => $email), true);
            $webform = ob_get_contents();
        } else {
            gravity_form(3, false, true, false, array('etude' => $etude, 'etude_courriel' => $email), true);
            $webform = ob_get_contents();
        }
        ob_end_clean();

        if ($form == "rendez-vous") {
            $html = preg_replace("/action=\'([^\']+?)\'/s", "action=\"/rendez-vous/\"", $webform);
        } else {
            $html = preg_replace("/action=\'([^\']+?)\'/s", "action=\"/demande-dinformation/\"", $webform);
        }

        $formulaire = $html;

        $html = "<h2>" . $title_h2 . "</h2>" . $html;

        $submenumap =
            "<ul class=\"navigationEtude\">".
            " <li id=\"smetude\"><a href=\"" . $etude_url . "\">" . __("Étude") . "</a></li>".
            " <li id=\"smequipe\"><a href=\"" . $equipe_url . "\">" . __("Équipe") . "</a></li>".
            " <li id=\"smdemande\"><a href=\"" . $notaires_demande_url . "\">" . __("Demande d'information") . "</a></li>".
            "</ul>";




        $nouvelles = getEtudeNouvelles($etude_id);


        return $html;
    }

    function getEtudeEquipe($etude_id) {
        global $DB, $submenumap, $title, $nouvelles, $show_new, $equipe_liste, $equipe_bio_liste;

        $nouvelles = getEtudeNouvelles($etude_id);


        $DB->query(
            "SELECT DISTINCT ".
            "   et.nom AS etude, ".
            "   em.key AS employe_key, ".
            "   em.prenom AS prenom, ".
            "   em.nom AS nom, ".
            "   em.courriel AS courriel, ".
            "   em.facebook AS facebook, ".
            "   em.twitter AS twitter, ".
            "   em.linkedin AS linkedin, ".
            "   em.description AS description, ".
            "   em.image AS image, ".
            "   em.annee_debut_pratique AS annee_debut_pratique, ".
            "   em.expertises_droit_affaires AS expertises_droit_affaires, ".
            "   em.expertises_droit_personne AS expertises_droit_personne, ".
            "   em.expertises_droit_immobilier AS expertises_droit_immobilier, ".
            "   em.expertises_sectorielles AS expertises_sectorielles, ".
            "   em.succursale AS succursale, ".
            "   fo.abbreviation_m AS fonction, ".
            //" fo.key AS fonction_key, ".
            "   REPLACE(REPLACE(CONCAT_WS(em.prenom, em.nom, ' '), 'É', 'E'), 'é', 'e') AS fullname ".
            "FROM ".
            "   `employes` AS em, ".
            "   `etudes` AS et, ".
            "   `etudes_succursales` AS et_su, ".
            "   `employes_etudes_succursales` AS em_et_su, ".
            "   `fonctions` AS fo, ".
            "   `employes_fonctions` AS em_fo ".
            "WHERE ".
            "   et.key = '" . $etude_id . "' ".
            "   AND et_su.etudes_key = et.key ".
            "   AND em.key NOT IN (1160) ".
            "   AND em_et_su.etudes_succursales_key = et_su.key ".
            "   AND em_et_su.employes_key = em.key ".
            "   AND em_fo.employes_key = em.key ".
            "   AND em_fo.fonctions_key = fo.key ".
            "   AND fo.key IN ('8','15') ".
            "   AND et.actif = '1' ".
            "   AND et_su.actif = '1' ".
            "   AND em.actif = '1' ".
            "ORDER BY ".
            "   fullname ASC;"
        );

        $employes = array();

        //$html .= "<h5>" . __("Notre <span>Étude</span>") . "</h5>";

        while ($DB->next_record()) {

            $etude = toHtml($DB->getField('etude'));
            $employe_key = toHtml($DB->getField('employe_key'));
            $prenom = toHtml($DB->getField('prenom'));
            $nom = toHtml($DB->getField('nom'));
            $courriel = toHtml($DB->getField('courriel'));
            $facebook = toHtml($DB->getField('facebook'));
            $twitter = toHtml($DB->getField('twitter'));
            $linkedin = toHtml($DB->getField('linkedin'));
            $notes_biographiques = stripslashes($DB->getField('description'));
            $annee_debut_pratique = toHtml($DB->getField('annee_debut_pratique'));
            $annee_debut_pratique_avocat = toHtml($DB->getField('annee_debut_pratique_avocat'));
            $expertises_droit_affaires = stripslashes($DB->getField('expertises_droit_affaires'));
            $expertises_droit_personne = stripslashes($DB->getField('expertises_droit_personne'));
            $expertises_droit_immobilier = stripslashes($DB->getField('expertises_droit_immobilier'));
            $expertises_sectorielles = stripslashes($DB->getField('expertises_sectorielles'));
            $succursale = toHtml($DB->getField('succursale'));
            $image_file = toHtml($DB->getField('image'));
            $fonction = toHtml($DB->getField('fonction'));




            if ($employe_key != $employe_key_prev) {
                $employes[$employe_key]['id'] = "employe_" . $employe_key;
                $employes[$employe_key]['nom'] = $fonction . " " . $prenom . " " . $nom;
                $employes[$employe_key]['courriel'] = $courriel;

                $employes[$employe_key]['facebook'] = $facebook;
                $employes[$employe_key]['twitter'] = $twitter;
                $employes[$employe_key]['linkedin'] = $linkedin;

                $employes[$employe_key]['notes_biographiques'] = $notes_biographiques;
                $employes[$employe_key]['image_url'] = EXTRANET_DOCS_EMPLOYES_IMG_URL . $image_file;

                if (trim($image_file) == "") {
                  $employes[$employe_key]['image_url'] = "/wp-content/themes/pmeinter/one-pager/img/anonymous.png";
                }


                if (!url_exists($employes[$employe_key]['image_url'])) {
                    $employes[$employe_key]['image_url'] = preg_replace("/\.jpg/", ".JPG", $employes[$employe_key]['image_url']);
                } else {
                  //echo "<!-- " . $employes[$employe_key]['image_url'] . "-->";
                }

                $employes[$employe_key]['description'] = "";

                if ($courriel != "") {
                    $employes[$employe_key]['email'] .= "<div class=\"email\"><a href=\"mailto:" . $courriel . "\" alt=\"Courriel de l'employ&eacute;\">" . $courriel . "</a></div>";
                }

                if ($succursale != "") {
                    $employes[$employe_key]['description'] .= "<p><strong>Succursale attitr&eacute;e&nbsp;:</strong> " . $succursale . "</p>";
                }

                $DB->query(
                    "SELECT fonctions_key ".
                    "FROM `employes_fonctions` ".
                    "WHERE employes_key = '" . $employe_key . "' "
                );
                $fonctions = array();
                while ($DB->next_record()) {
                    $fonctions[$DB->getField("fonctions_key")] = 1;
                }

                if ($annee_debut_pratique != "" && $fonctions[8] == 1) {
                    $employes[$employe_key]['debut_pratique'] .= "<p class=\"debut-pratique\"><strong>Notaire depuis&nbsp;:</strong> " . $annee_debut_pratique . "</p>";
                }



                if ($annee_debut_pratique_avocat != "" && $fonctions[15] == 1) {
                    $employes[$employe_key]['debut_pratique'] .= "<p class=\"debut-pratique\"><strong>Avocat(e), conseiller(e) juridique depuis&nbsp;:</strong> " . $annee_debut_pratique_avocat . "</p>";
                } else if ($fonctions[15] == 1) {
                    $employes[$employe_key]['debut_pratique'] .= "<p class=\"debut-pratique\"><strong>Avocat(e), conseiller(e) juridique</strong> </p>";
                }

                if ($expertises_droit_affaires != "") {
                    $employes[$employe_key]['affaires'] .= "<p class=\"affaires\"><strong>Droit des affaires&nbsp;:</strong><br />" . $expertises_droit_affaires . "</p>";
                }
                if ($expertises_droit_personne != "") {
                    $employes[$employe_key]['personnes'] .= "<p class=\"personnes\"><strong>Droit de la personne&nbsp;:</strong><br />" . $expertises_droit_personne . "</p>";
                }
                if ($expertises_droit_immobilier != "") {
                    $employes[$employe_key]['immobilier'] .= "<p class=\"immobilier\"><strong>Droit immobilier&nbsp;:</strong><br />" . $expertises_droit_immobilier . "</p>";
                }
                if ($expertises_sectorielles != "") {
                    $employes[$employe_key]['sectoriel'] .= "<p class=\"sectoriel\"><strong>Expertises sectorielles&nbsp;:</strong><br />" . $expertises_sectorielles . "</p>";
                }



                $equipe_liste .=
                    '<li class="team-item col-6 col-4-m no-padding-m col-3-l no-padding-l col-2-xl no-padding-xl">
                      <a id="team-' . $employes[$employe_key]['id'] . '" href="#team-' . $employes[$employe_key]['id'] . '-profile"><img class="block img-full" src="' . $employes[$employe_key]['image_url'] . '" alt="' . $employes[$employe_key]['nom'] . '">
                        <span class="team-item-content">
                          <strong class="team-item-name">' . $employes[$employe_key]['nom'] . '</strong>
                          <strong class="team-item-title"></strong>
                          <span class="team-item-position">' . $succursale . '</span>
                        </span>
                      </a>
                    </li>';

                $equipe_bio_liste .=
                    '<div class="modal" id="team-' . $employes[$employe_key]['id'] . '-profile">
                        <a href="#team-' . $employes[$employe_key]['id'] . '" class="modal-close"><span class="sr">Fermer</span></a>
                        <div class="modal-inner">
                          <h2 class="heading fs-3 team-profile-heading">' . $employes[$employe_key]['nom'] . '</h2>
                          <strong class="block">' . $succursale . '</strong>
                          <div class="clear team-profile-content">
                            <div class="team-profile-image block img-fluid">
                              <img class="img-fluid" src="' . $employes[$employe_key]['image_url'] . '" alt="' . $employes[$employe_key]['nom'] . '"><br />
                              ' . (trim($employes[$employe_key]['linkedin']) != "" ? "<a href=\"" . $employes[$employe_key]['linkedin'] . "\" target=\"_blank\"><span class=\"fa fa-linkedin\"></span></a>&nbsp;&nbsp;" : "") .
                               (trim($employes[$employe_key]['twitter']) != "" ? "<a href=\"" . $employes[$employe_key]['twitter'] . "\" target=\"_blank\"><span class=\"fa fa-twitter\"></span></a>&nbsp;&nbsp;" : "") .
                                (trim($employes[$employe_key]['facebook']) != "" ? "<a href=\"" . $employes[$employe_key]['facebook'] . "\" target=\"_blank\"><span class=\"fa fa-facebook-official\"></span></a> " : "") .
                              '
                            </div>
                            <div class="team-profile-content-block">
                              ' . $employes[$employe_key]['email']. '
                              ' . $employes[$employe_key]['debut_pratique'] . '
                              ' . $employes[$employe_key]['affaires'] . '
                              ' . $employes[$employe_key]['personnes'] . '
                              ' . $employes[$employe_key]['immobilier'] . '
                              ' . $employes[$employe_key]['sectoriel'];

                        if (strlen($employes[$employe_key]['notes_biographiques']) > 16) {

                            if (strpos($employes[$employe_key]['notes_biographiques'], '<p>') === false) {
                                $employes[$employe_key]['notes_biographiques'] = '<p>' . $employes[$employe_key]['notes_biographiques'] . '</p>';
                            }

                            $equipe_bio_liste .= $employes[$employe_key]['notes_biographiques'];
                        }

                $equipe_bio_liste .=
                    '       </div>
                          </div>
                        </div>
                      </div>';



                //$html .=
                  //  "<div class=\"boxEquipeList\"><a href=\"#" . $employes[$employe_key]['id'] . "\">" . $employes[$employe_key]['nom'] . "</a></div>";
            }
            $employe_key_prev = $employe_key;
        }

        if (is_array($employes) && count($employes) > 0 && !$show_new) {

            $html .=
                "<div class=\"separator\">&nbsp;</div>";

            foreach ($employes as $employe_key => $info) {


                $html .=
                    "<!-- ===================== Description membre ============= -->\n".
                    "<div id=\"" . $info['id'] . "\" class=\"equipe01\">\n".
                    (preg_match("/\.[a-z]+$/i", $info['image_url']) ? " <div class=\"imgEquipe\"><img width=\"126\" height=\"180\" src=\"" . $info['image_url'] . "\" alt=\"" . $info['nom'] . "\"></div>\n" : "").
                    "   <h3><a name=\"equipe01\"></a>" . $info['nom'] . "</h3>\n".
                    "   <h4>Notes biographiques</h4>".
                    "   ". $info['email'] . "\n".
                    "   ". $info['debut_pratique'] . "\n".
                    "   ". $info['affaires'] . "\n".
                    "   ". $info['personnes'] . "\n".
                    "   ". $info['immobilier'] . "\n".
                    "   ". $info['sectoriel'] . "\n";

                if (strlen($info['notes_biographiques']) > 16) {

                    if (strpos($info['notes_biographiques'], '<p>') === false) {
                        $info['notes_biographiques'] = '<p>' . $info['notes_biographiques'] . '</p>';
                    }

                    $html .=
                        "   <div class=\"box\">\n".
                        "       <span class=\"close\">X</span>\n".
                        "       <h3><a name=\"equipe01\"></a>" . $info['nom'] . "</h3>\n".
                        "       <h4>Notes biographiques</h4>".
                        "       <div class=\"bio\">" . $info['notes_biographiques'] . "</div>\n".
                        "   </div>\n";
                }

                $html .=
                    "<br clear=\"all\" /></div>".
                    "<!-- ===================== fin Description membre ============= -->";


            }
        }

        return $html;
    }


    function getEtude($etude_id) {
        global $DB, $submenumap, $title, $nouvelles, $show_new, $address, $cities, $slogan, $description, $telephone, $fax, $linkedin, $facebook, $twitter, $nos_experts, $map_section, $meta_description, $mode_paiement, $heures_ouverture, $banner, $hide_droit_affaires, $hide_droit_personne, $hide_droit_agricole, $hide_droit_immobilier, $droit_affaires, $droit_personne, $droit_agricole, $droit_immobilier, $new_service_title, $new_service_description, $intro_services, $canonical, $accessible;

        if (trim($etude_id) == "") {
            header("Location: http://pmeinter.com/", true, 301);
            exit;
        }

        $DB->query(
            "SELECT e.*, es.* ".
            "FROM etudes e ".
            "INNER JOIN etudes_succursales es ON e.key = es.etudes_key ".
            "WHERE e.nombre_notaires_associes > 0 ".
            "AND e.key = '" . $etude_id . "' ".
            "AND es.siege_social = '1' ".
            "AND es.actif = '1' ".
            "AND e.actif = '1' ".
            "ORDER BY nom ASC"
        );

        $etude_url = "";
        $equipe_url = "";
        $notaires_demande_url = "";
        $html = "";
        $description = "";
        $canonical = "";

        while ($DB->next_record()) {
            if (trim($etude_url . $equipe_url . $notaires_demande_url) == "") {
                $etude_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom"));
                $equipe_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "/equipe";
                $notaires_demande_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "/demande-information";
                //$html .= "<h5>" . __("Notre <span>Étude</span>") . "</h5>";
            }

            $slogan = toHtml($DB->getField('slogan'));
            $linkedin = toHtml($DB->getField('linkedin'));
            $facebook = toHtml($DB->getField('facebook'));
            $twitter = toHtml($DB->getField('twitter'));
            $accessible = $DB->getField("accessible");

            if (trim($linkedin) == "") { $linkedin = "https://www.linkedin.com/company/pme-inter-notaires"; }
            if (trim($facebook) == "") { $facebook = "https://www.facebook.com/Réseau-PME-INTER-Notaires-1444799325801880/"; }

            $hide_droit_affaires = 0;
            $hide_droit_personne = 0;
            $hide_droit_agricole = 0;
            $hide_droit_immobilier = 0;

            if ($DB->getField("droit_affaires_oui") == 0) { $hide_droit_affaires = 1; }
            if ($DB->getField("droit_personne_oui") == 0) { $hide_droit_personne = 1; }
            if ($DB->getField("droit_agricole_oui") == 0) { $hide_droit_agricole = 1; }
            if ($DB->getField("droit_immobilier_oui") == 0) { $hide_droit_immobilier = 1; }

            $droit_affaires = trim(toHtml($DB->getField("droit_affaires_desc")));
            $droit_personne = trim(toHtml($DB->getField("droit_personne_desc")));
            $droit_immobilier = trim(toHtml($DB->getField("droit_immobilier_desc")));
            $droit_agricole = trim(toHtml($DB->getField("droit_agricole_desc")));

            $intro_services = trim(toHtml($DB->getField("intro_services")));

            $new_service_title = trim(toHtml($DB->getField("new_service_title")));
            $new_service_description = trim(toHtml($DB->getField("new_service_description")));


            $heures_ouverture = toHtml($DB->getField('heures_ouverture'));
            $mode_paiement = toHtml($DB->getField('mode_paiement'));
            $meta_description = toHtml($DB->getField('meta_description'));

            $etude = toHtml($DB->getField('nom'));
            $title = $etude;
            $lat = $DB->getField('lat');
            $lng = $DB->getField('long');


            $adresse = toHtml($DB->getField('adresse'));
            $address = $adresse;
            $ville = toHtml($DB->getField('ville'));
            $cities = $ville;
            $province = toHtml($DB->getField('province'));
            $code_postal = toHtml($DB->getField('code_postal'));
            $telephone1 = toHtml(str_replace(array("(", ")"), "", $DB->getField('telephone1')));
            $telephone2 = toHtml(str_replace(array("(", ")"), "", $DB->getField('telephone2')));
            $telecopieur = toHtml(str_replace(array("(", ")"), "", $DB->getField('telecopieur')));
            $telephone = $telephone1;
            $fax = $telecopieur;

            if (strpos($DB->getField("courriel_etudes_succursales"), "@")) {
                $courriel = toHtml($DB->getField("courriel_etudes_succursales"));
            } else {
                $courriel = toHtml($DB->getField('courriel'));
            }

            $courriel_str = $courriel;
            if (strlen($courriel_str) > 32) { $courriel_str = substr($courriel_str, 0, 30) . "..."; }





            $html .=
                "<div class=\"address\" data-map='{\"lat\":\"" . $lat . "\", \"lng\":\"" . $lng . "\", \"title\":\"" . $title . " " . $ville . "\"}'>\n".
                "   <p class=\"address\">".
                        $adresse . "<br />".
                        $ville . " (" . $province . ")&nbsp;&nbsp;" . $code_postal . "<br />".
                        "<a href=\"mailto:" . $courriel . "\">" . $courriel_str . "</a>".
                "   </p>";

            if ($telephone1 != "" || $telephone2 != "") {
                $html .=
                    "   <div class=\"tel\">\n".
                    "       <h4>" . __("Téléphone") . "</h4>".
                    "       <p>";
                if ($telephone1 != "") $html .= $telephone1 . "<br/>";
                if ($telephone2 != "") $html .= $telephone2 . "<br/>";
                $html .=
                    "       </p>\n".
                    "   </div>\n";
            }

            if ($telecopieur != "") {
                $html .=
                    "   <div class=\"tel\">\n".
                    "       <h4>" . __("Télécopieur") . "</h4>\n".
                    "       <p>" . $telecopieur . "</p>\n".
                    "   </div>\n";
            }

            $html .= "<br clear=\"all\" /></div>";

            $etude_description = stripslashes($DB->getField('description'));
            $image_file = toHtml($DB->getField('image'));
            $image_description_1 = toHtml($DB->getField('image_description_1'));
            $image_description_2 = toHtml($DB->getField('image_description_2'));
            $image_description_3 = toHtml($DB->getField('image_description_3'));
            $image_description = "";

            $banner_file = toHtml($DB->getField('banner'));
            if ($banner_file != "") {
                $banner = EXTRANET_DOCS_ETUDES_IMG_URL . $banner_file;

                if (!url_exists($banner)) {
                    $banner = preg_replace("/\.jpg/", ".JPG", $banner);
                }
           }


           if (trim($etude_description) == "" || preg_match("/^\s*<p>&nbsp;<\/p>\s*$/is", $etude_description) || preg_match("/^\s*<br\/?>\s*$/is", $etude_description)) {
              $etude_description = "<p>Notre équipe de professionnels chevronnés offre un service hautement personnalisé, axé sur la confiance et la compétence. Pour avoir l’esprit tranquille, en droit des affaires, en droit de la personne ou en droit immobilier, ne laissez rien au hasard : consultez les experts de PME INTER Notaires. C’est une question de confiance...</p>";
           }


            if ($etude_description != "") {
                $description .= "   <div class=\"description\">" . $etude_description . "</div>";
            }

            if ($image_description_1 != "") {
                $image_description .= "<p><strong>Premi&egrave;re rang&eacute;e&nbsp;:</strong><br />" . $image_description_1 . "</p>";
            }
            if ($image_description_2 != "") {
                $image_description .= "<p><strong>Deuxi&egrave;me rang&eacute;e&nbsp;:</strong><br />" . $image_description_2 . "</p>";
            }
            if ($image_description_3 != "") {
                $image_description .= "<p><strong>Troisi&egrave;me rang&eacute;e&nbsp;:</strong><br />" . $image_description_3 . "</p>";
            }

            if ($show_new) {
                if ($etude_description != "" || $image_file != "") {
                    if ($image_file != "") {
                        $image_url = EXTRANET_DOCS_ETUDES_IMG_URL . $image_file;

                        if (!url_exists($image_url)) {
                            $image_url = preg_replace("/\.jpg/", ".JPG", $image_url);
                        }


                        $nos_experts .= "<div class=\"photo\"><img src=\"" . $image_url . "\" width=\"296\" alt=\"Photo de l'&eacute;quipe\" /></div>";
                    }
                    $nos_experts .= "<div class=\"photo_equipe\">" . $image_description . "</div>";
                    $nos_experts .= "<br clear=\"all\" />";

                }
            } else {


                if ($etude_description != "" || $image_file != "") {
                    if ($image_file != "") {
                        $image_url = EXTRANET_DOCS_ETUDES_IMG_URL . $image_file;

                        if (!url_exists($image_url)) {
                            $image_url = preg_replace("/\.jpg/", ".JPG", $image_url);
                        }


                        $description .= "<div class=\"photo\"><img src=\"" . $image_url . "\" width=\"296\" alt=\"Photo de l'&eacute;quipe\" /></div>";
                    }
                    $description .= "<div class=\"photo_equipe\">" . $image_description . "</div>";
                    $description .= "<br clear=\"all\" />";

                }
            }
        }


        $html .= "<div class=\"equipe-layer\">" . $description . "</div>";

        $html .= "<div class=\"members\"><div class=\"list\">" . getEtudeEquipe($etude_id) . "<br clear=\"all\" /></div></div>";


        $DB->query(
            "SELECT e.*, es.* ".
            "FROM etudes e ".
            "INNER JOIN etudes_succursales es ON e.key = es.etudes_key ".
            "WHERE e.nombre_notaires_associes > 0 ".
            "AND e.key = '" . $etude_id . "' ".
            "AND es.siege_social = '1' ".
            "AND es.actif = '1' ".
            "ORDER BY nom ASC"
        );

        while ($DB->next_record()) {
            $adresse = toHtml($DB->getField('adresse'));
            $lat = $DB->getField('lat');
            $lng = $DB->getField('long');

            $ville = toHtml($DB->getField('ville'));
            $province = toHtml($DB->getField('province'));
            $code_postal = toHtml($DB->getField('code_postal'));
            $telephone1 = toHtml(str_replace(array("(", ")"), "", $DB->getField('telephone1')));
            $telephone2 = toHtml(str_replace(array("(", ")"), "", $DB->getField('telephone2')));
            $telecopieur = toHtml(str_replace(array("(", ")"), "", $DB->getField('telecopieur')));

            if (strpos($DB->getField("courriel_etudes_succursales"), "@")) {
                $courriel = toHtml($DB->getField("courriel_etudes_succursales"));
            } else {
                $courriel = toHtml($DB->getField('courriel'));
            }

            $courriel_str = $courriel;
            if (strlen($courriel_str) > 32) { $courriel_str = substr($courriel_str, 0, 30) . "..."; }


            $etude_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom"));


            $first_map_section =
                "<div class=\"address col-sm-3 col-xs-6\" data-map='{\"lat\":\"" . $lat . "\", \"lng\":\"" . $lng . "\", \"title\":\"" . $title . " " . $ville . "\"}'>\n".
                "   <p class=\"address\">" . $adresse . "<br />" . $ville . "<br />" . $province . ", " . $code_postal . "<br/>";

            if ($courriel != "") $first_map_section .= "<a href=\"mailto:" . $courriel . "\">" . $courriel_str . "</a>";
            $first_map_section .= "  </p>";

            $first_map_section .= "  <div class=\"phone\">\n";
            if ($telephone1 != "" || $telephone2 != "") {
                $first_map_section .=
                    "       <h4 class=\"tel\">" . __("Téléphone") . "</h4>".
                    "       <p class=\"tel\">";
                if ($telephone1 != "") $first_map_section .= $telephone1 . "<br/>";
                if ($telephone2 != "") $first_map_section .= $telephone2 . "<br/>";
                if ($telephone1 == "" && $telephone2 == "") $first_map_section .= __("non disponible") . "<br/>";
                $first_map_section .= "      </p>";
            }

            $first_map_section .=
                "   </div>\n".
                "   <div class=\"fax\">\n";

            if ($telecopieur != "") {
                $first_map_section .=
                    "           <h4 class=\"tel\">" . __("Télécopieur") . "</h4>\n".
                    "           <p class=\"fax\">" . $telecopieur . "</p>";
            }

            $first_map_section .=
                "   </div>".
                "</div>\n";

            if ($_SERVER["REQUEST_URI"] != $etude_url) {
              $canonical = '<link rel="canonical" href="http://pmeinter.com' . $etude_url . '" />';
            }
        }





        $nouvelles_array = array();
        while ($DB->next_record()) {
            if (trim($etude_url . $equipe_url . $notaires_demande_url) == "") {
                $etude_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom"));
                $equipe_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "/equipe";
                $notaires_demande_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "/demande-information";
            }

        }










        $DB->query(
            "SELECT e.*, es.* ".
            "FROM etudes e ".
            "INNER JOIN etudes_succursales es ON e.key = es.etudes_key ".
            "WHERE e.nombre_notaires_associes > 0 ".
            "AND e.key = '" . $etude_id . "' ".
            "AND es.siege_social = '0' ".
            "AND es.actif = '1' ".
            "ORDER BY nom ASC"
        );

        $map_section = "<div id=\"lcontact\" class=\"autres-adresses container row\">";
        if ($DB->getNumRows() > 0) {
            if ($show_new) {
                $map_section .= "<h3 class=\"heading fs-3\">" . __("Nos adresses") . "</h3>";
            } else {
                $map_section .= "<h2>" . __("Autres <span>Adresses</span>") . "</h2>";
            }
        } else {
          $map_section .= "<h3 class=\"heading fs-3\">" . __("Notre adresse") . "</h3>";
          if (trim($first_map_section) != "") {
            $map_section .= $first_map_section;
            $first_map_section = "";
          }
        }
        while ($DB->next_record()) {
            $adresse = toHtml($DB->getField('adresse'));
            $lat = $DB->getField('lat');
            $lng = $DB->getField('long');

            $ville = toHtml($DB->getField('ville'));
            $province = toHtml($DB->getField('province'));
            $code_postal = toHtml($DB->getField('code_postal'));
            $telephone1 = toHtml(str_replace(array("(", ")"), "", $DB->getField('telephone1')));
            $telephone2 = toHtml(str_replace(array("(", ")"), "", $DB->getField('telephone2')));
            $telecopieur = toHtml(str_replace(array("(", ")"), "", $DB->getField('telecopieur')));

            if (strpos($DB->getField("courriel_etudes_succursales"), "@")) {
                $courriel = toHtml($DB->getField("courriel_etudes_succursales"));
            } else {
                $courriel = toHtml($DB->getField('courriel'));
            }

            $courriel_str = $courriel;
            if (strlen($courriel_str) > 32) { $courriel_str = substr($courriel_str, 0, 30) . "..."; }


            if (trim($first_map_section) != "") {
              $map_section .= $first_map_section;
              $first_map_section = "";
            }


            $map_section .=
                "<div class=\"address col-sm-3 col-xs-6\" data-map='{\"lat\":\"" . $lat . "\", \"lng\":\"" . $lng . "\", \"title\":\"" . $title . " " . $ville . "\"}'>\n".
                "   <p class=\"address\">" . $adresse . "<br />" . $ville . "<br />" . $province . ", " . $code_postal . "<br/>";

            if ($courriel != "") $map_section .= "<a href=\"mailto:" . $courriel . "\">" . $courriel_str . "</a>";
            $map_section .= "  </p>";

            $map_section .= "  <div class=\"phone\">\n";
            if ($telephone1 != "" || $telephone2 != "") {
                $map_section .=
                    "       <h4 class=\"tel\">" . __("Téléphone") . "</h4>".
                    "       <p class=\"tel\">";
                if ($telephone1 != "") $map_section .= $telephone1 . "<br/>";
                if ($telephone2 != "") $map_section .= $telephone2 . "<br/>";
                if ($telephone1 == "" && $telephone2 == "") $map_section .= __("non disponible") . "<br/>";
                $map_section .= "      </p>";
            }

            $map_section .=
                "   </div>\n".
                "   <div class=\"fax\">\n";

            if ($telecopieur != "") {
                $map_section .=
                    "           <h4 class=\"tel\">" . __("Télécopieur") . "</h4>\n".
                    "           <p class=\"fax\">" . $telecopieur . "</p>";
            }
            $map_section .=
                "   </div>".
                "</div>\n";
        }

        $map_section .=
            "</div>".
            "<div id=\"gmap\"></div>\n";

        $html .= $map_section;

            //"<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2792.9443704399428!2d-73.41654614827428!3d45.57155727899974!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cc902fd0b66e881%3A0xd9fb61f4a6abe03a!2s1550+Rue+Amp%C3%A8re%2C+Boucherville%2C+QC+J4B+7L4!5e0!3m2!1sen!2sca!4v1469102202380\" frameborder=\"0\" style=\"border:0; width: 100%; height: 400px;pointer-events:none;\" allowfullscreen></iframe>";

        //$html .= $description;

        $submenumap =
            "<ul class=\"navigationEtude\">".
            " <li id=\"smetude\"><a href=\"" . $etude_url . "\">" . __("Étude") . "</a></li>".
            " <li id=\"smequipe\"><a href=\"" . $equipe_url . "\">" . __("Équipe") . "</a></li>".
            " <li id=\"smdemande\"><a href=\"" . $notaires_demande_url . "\">" . __("Demande d'information") . "</a></li>".
            "</ul>";


        $nouvelles = getEtudeNouvelles($etude_id);


        return $html;
    }

    function get_etudes_filter(){
      global $DB;
      $etude_id = getEtudeIDByURL($_GET["e"]);

      $query_string = "";
      
      $DB->query(
        "SELECT e.*, es.* ".
        "FROM etudes e ".
        "INNER JOIN etudes_succursales es ON e.key = es.etudes_key ".
        "WHERE e.nombre_notaires_associes > 0 ".
        "AND e.key = '" . $etude_id . "' ".
        "AND es.siege_social = '0' ".
        "AND es.actif = '1' ".
        "ORDER BY nom ASC"
    );

    while($record = $DB->next_record()){
      $ville = toHtml($DB->getField('ville'));
      $query_string .= $ville . ',';
    }

    
      return $query_string;
    }


    function getEtudeNouvelles($etude_id) {
        global $DB, $submenumap, $title, $nouvelles, $videos, $videos_ss, $show_new;

        $DB->query(
            "SELECT e.*, es.* ".
            "FROM etudes e ".
            "INNER JOIN etudes_succursales es ON e.key = es.etudes_key ".
            "WHERE e.nombre_notaires_associes > 0 ".
            "AND e.key = '" . $etude_id . "' ".
            "AND es.siege_social = '1' ".
            "AND es.actif = '1' ".
            "AND e.actif = '1' ".
            "ORDER BY nom ASC"
        );

        $nouvelles_array = array();
        while ($DB->next_record()) {
            if (trim($etude_url . $equipe_url . $notaires_demande_url) == "") {
                $etude_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom"));
                $equipe_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "/equipe";
                $notaires_demande_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "/demande-information";
            }

            for ($n = 1; $n <= NB_NOUVELLES; $n++) {
                if ($DB->getField('nouvelle_' . $n . '_visible')) {
                    $nouvelles_array[$n]['titre'] = $DB->getField('nouvelle_' . $n . '_titre');
                    $nouvelles_array[$n]['doc'] = $DB->getField('nouvelle_' . $n . '_doc');
                    $nouvelles_array[$n]['video'] = $DB->getField('nouvelle_' . $n . '_video');
                    $nouvelles_array[$n]['description'] = $DB->getField('nouvelle_' . $n . '_description');
                    $nouvelles_array[$n]['img'] = $DB->getField('nouvelle_' . $n . '_img');

                }
            }
        }

        if ($_GET["p"] == "equipe") {
            if ($_SERVER["REQUEST_URI"] != $equipe_url) {
                //header("Location:". $equipe_url, TRUE, 301);
            }
        } else if ($_GET["p"] != "") {

        } else if ($_GET["e"] != "") {
            if ($_SERVER["REQUEST_URI"] != $etude_url) {
                //header("Location:". $etude_url, TRUE, 301);
            }
        }

        $submenumap =
            "<ul class=\"navigationEtude\">".
            " <li id=\"smetude\"><a href=\"" . $etude_url . "\">" . __("Étude") . "</a></li>".
            " <li id=\"smequipe\"><a href=\"" . $equipe_url . "\">" . __("Équipe") . "</a></li>".
            " <li id=\"smdemande\"><a href=\"" . $notaires_demande_url . "\">" . __("Demande d'information") . "</a></li>".
            "</ul>";

        $videos = array();

        if (count($nouvelles_array) > 0) {

            if ($show_new) {


                $nouvelles =
                    '<div class="animate-block news-list clear">
                        <div class="news-list-left col-8-l no-padding-l">';


                if (isset($nouvelles_array[1]['doc'])) {

                    $nouvelles .=
                      '<div class="news-item clear">
                        <a href="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/' . $nouvelles_array[1]['doc'] . '" target="_blank" class="news-item-image col-7-l no-padding-l">
                          ' . (isset($nouvelles_array[1]['img']) && $nouvelles_array[1]['img'] != "" ? '<img class="block img-fluid" src="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/t' . $nouvelles_array[1]['img'] . '" alt="" />' : '<img class="block img-fluid" src="/wp-content/themes/pmeinter/one-pager/img/news-item-01.jpg" alt="" />') .
                          '
                        </a>
                        <div class="news-item-content col-5-l no-padding-l">
                          <div class="news-item-content-inner">
                            <h3 class="news-item-title uppercase"><a href="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/' . $nouvelles_array[1]['doc'] . '" target="_blank">' . $nouvelles_array[1]['titre'] . '</a></h3>
                            <p>' . $nouvelles_array[1]['description'] . '</p>
                            <!--em class="news-item-publish-date">08.07.2015</em-->
                          </div>
                        </div>
                      </div>';

                } else if (isset($nouvelles_array[1]['video'])) {

                  $nouvelles .=
                      '<div class="news-item clear">
                        <a href="' . $nouvelles_array[1]['video'] . '" target="_blank" class="news-item-image col-7-l no-padding-l">
                          ' . (isset($nouvelles_array[1]['img']) && $nouvelles_array[1]['img'] != "" ? '<img class="block img-fluid" src="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/t' . $nouvelles_array[1]['img'] . '" alt="" />' : '<img class="block img-fluid" src="/wp-content/themes/pmeinter/one-pager/img/news-item-01.jpg" alt="" />') .
                          '
                        </a>
                        <div class="news-item-content col-5-l no-padding-l">
                          <div class="news-item-content-inner">
                            <h3 class="news-item-title uppercase"><a href="' . $nouvelles_array[1]['video'] . '" target="_blank">' . $nouvelles_array[1]['titre'] . '</a></h3>
                            <p>' . $nouvelles_array[1]['description'] . '</p>
                            <!--em class="news-item-publish-date">08.07.2015</em-->
                          </div>
                        </div>
                      </div>';

                }



                if (isset($nouvelles_array[2]['doc'])) {

                    $nouvelles .=
                      '<div class="news-item news-item-secondary clear">
                        <a href="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/' . $nouvelles_array[2]['doc'] . '" target="_blank" class="news-item-image col-7-l no-padding-l">
                          ' . (isset($nouvelles_array[2]['img']) && $nouvelles_array[2]['img'] != "" ? '<img class="block img-fluid" src="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/t' . $nouvelles_array[2]['img'] . '" alt="" />' : '<img class="block img-fluid" src="/wp-content/themes/pmeinter/one-pager/img/news-item-02.jpg" alt="" />') .
                          '
                        </a>
                        <div class="news-item-content col-5-l no-padding-l">
                          <div class="news-item-content-inner">
                            <h3 class="news-item-title uppercase"><a href="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/' . $nouvelles_array[2]['doc'] . '" target="_blank">' . $nouvelles_array[2]['titre'] . '</a></h3>
                            <p>' . $nouvelles_array[2]['description'] . '</p>
                            <!--em class="news-item-publish-date">08.07.2015</em-->
                          </div>
                        </div>
                      </div>';

                } else if (isset($nouvelles_array[2]['video'])) {

                  $nouvelles .=
                      '<div class="news-item news-item-secondary clear">
                        <a href="' . $nouvelles_array[2]['video'] . '" target="_blank" class="news-item-image col-7-l no-padding-l">
                          ' . (isset($nouvelles_array[2]['img']) && $nouvelles_array[2]['img'] != "" ? '<img class="block img-fluid" src="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/t' . $nouvelles_array[2]['img'] . '" alt="" />' : '<img class="block img-fluid" src="/wp-content/themes/pmeinter/one-pager/img/news-item-02.jpg" alt="" />') .
                          '
                        </a>
                        <div class="news-item-content col-5-l no-padding-l">
                          <div class="news-item-content-inner">
                            <h3 class="news-item-title uppercase"><a href="' . $nouvelles_array[2]['video'] . '" target="_blank">' . $nouvelles_array[2]['titre'] . '</a></h3>
                            <p>' . $nouvelles_array[2]['description'] . '</p>
                            <!--em class="news-item-publish-date">08.07.2015</em-->
                          </div>
                        </div>
                      </div>';

                }

                $nouvelles .=
                  '</div>
                  <div class="news-list-right col-4-l no-padding-l">';


                if (isset($nouvelles_array[3]['doc'])) {

                    $nouvelles .=
                      '<div class="news-item news-item-third clear">
                        <a href="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/' . $nouvelles_array[3]['doc'] . '" target="_blank" class="news-item-image col-7-l no-padding-l">
                          ' . (isset($nouvelles_array[3]['img']) && $nouvelles_array[3]['img'] != "" ? '<img class="block img-fluid" src="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/t' . $nouvelles_array[3]['img'] . '" alt="" />' : '<img class="block img-fluid" src="/wp-content/themes/pmeinter/one-pager/img/news-item-03.jpg" alt="" />') .
                          '
                        </a>
                        <div class="news-item-content col-5-l no-padding-l">
                          <div class="news-item-content-inner">
                            <h3 class="news-item-title uppercase"><a href="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/' . $nouvelles_array[3]['doc'] . '" target="_blank">' . $nouvelles_array[3]['titre'] . '</a></h3>
                            <p>' . $nouvelles_array[3]['description'] . '</p>
                            <!--em class="news-item-publish-date">08.07.2015</em-->
                          </div>
                        </div>
                      </div>';

                } else if (isset($nouvelles_array[3]['video'])) {

                  $nouvelles .=
                      '<div class="news-item news-item-third clear">
                        <a href="' . $nouvelles_array[3]['video'] . '" target="_blank" class="news-item-image col-7-l no-padding-l">
                          ' . (isset($nouvelles_array[3]['img']) && $nouvelles_array[3]['img'] != "" ? '<img class="block img-fluid" src="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/t' . $nouvelles_array[3]['img'] . '" alt="" />' : '<img class="block img-fluid" src="/wp-content/themes/pmeinter/one-pager/img/news-item-03.jpg" alt="" />') .
                          '
                        </a>
                        <div class="news-item-content col-5-l no-padding-l">
                          <div class="news-item-content-inner">
                            <h3 class="news-item-title uppercase"><a href="' . $nouvelles_array[3]['video'] . '" target="_blank">' . $nouvelles_array[3]['titre'] . '</a></h3>
                            <p>' . $nouvelles_array[3]['description'] . '</p>
                            <!--em class="news-item-publish-date">08.07.2015</em-->
                          </div>
                        </div>
                      </div>';

                }


                $nouvelles .=
                  '</div>
                  <div class="news-list-right col-4-l no-padding-l" style="width: 100%;">';


                if (isset($nouvelles_array[4]['doc'])) {

                    $nouvelles .=
                      '<div class="news-item news-item-fourth clear" style="width: 100%;">
                        <a href="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/' . $nouvelles_array[4]['doc'] . '" target="_blank" class="news-item-image col-5-l no-padding-l">
                          ' . (isset($nouvelles_array[4]['img']) && $nouvelles_array[4]['img'] != "" ? '<img class="block img-fluid" src="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/t' . $nouvelles_array[4]['img'] . '" alt="" />' : '<img class="block img-fluid" src="/wp-content/themes/pmeinter/one-pager/img/news-item-04.jpg" alt="" />') .
                          '
                        </a>
                        <div class="news-item-content col-7-l no-padding-l">
                          <div class="news-item-content-inner">
                            <h3 class="news-item-title uppercase"><a href="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/' . $nouvelles_array[4]['doc'] . '" target="_blank">' . $nouvelles_array[4]['titre'] . '</a></h3>
                            <p>' . $nouvelles_array[4]['description'] . '</p>
                            <!--em class="news-item-publish-date">08.07.2015</em-->
                          </div>
                        </div>
                      </div>';

                } else if (isset($nouvelles_array[4]['video'])) {

                  $nouvelles .=
                      '<div class="news-item news-item-fourth clear" style="width: 100%;">
                        <a href="' . $nouvelles_array[4]['video'] . '" target="_blank" class="news-item-image col-5-l no-padding-l">
                          ' . (isset($nouvelles_array[4]['img']) && $nouvelles_array[4]['img'] != "" ? '<img class="block img-fluid" src="https://bd.pmeinter.ca/docs/etudes_nouvelles/' . $etude_id . '/t' . $nouvelles_array[4]['img'] . '" alt="" />' : '<img class="block img-fluid" src="/wp-content/themes/pmeinter/one-pager/img/news-item-04.jpg" alt="" />') .
                          '
                        </a>
                        <div class="news-item-content col-7-l no-padding-l">
                          <div class="news-item-content-inner">
                            <h3 class="news-item-title uppercase"><a href="' . $nouvelles_array[4]['video'] . '" target="_blank">' . $nouvelles_array[4]['titre'] . '</a></h3>
                            <p>' . $nouvelles_array[4]['description'] . '</p>
                            <!--em class="news-item-publish-date">08.07.2015</em-->
                          </div>
                        </div>
                      </div>';

                }

                $nouvelles .=
                    '</div>
                    </div>';



            } else {

                $nouvelles .= "<ul>";
                foreach ($nouvelles_array as $nb => $data) {

                    $nouvelle_url = EXTRANET_DOCS_ETUDES_NOUVELLES_URL . $etude_id . "/" . $data['doc'];

                    if (preg_match("/(http:\/\/)?(www\.[a-z0-9\-]+\.[a-z]+)/",$data['titre'])) {
                        $nouvelle_url = preg_replace("/.*?(http:\/\/)?(www\.[a-z0-9\-]+\.[a-z]+).*?/", "http://$2", $data['titre']);
                        $data['doc'] = ".";
                    }

                    if (trim($data['video']) != "") {
                        $nouvelles .= "";


                        if (trim($data['doc']) != "") {
                            $videos[] = toHtml($data['titre']) . "<br /><iframe width=\"298\" height=\"168\" src=\"https://www.youtube.com/embed/" . preg_replace("/^.*?(v=|\.be\/)([^\&]+).*?$/", "$2", $data["video"]) . "\" frameborder=\"0\" allowfullscreen></iframe><br />\n".
                                "<a href=\"" . $nouvelle_url . "\" target=\"_blank\" style=\"color: #5c777f;\">Notes PDF</a>\n";
                        } else {
                            $videos[] = toHtml($data['titre']) . "<br /><iframe width=\"298\" height=\"168\" src=\"https://www.youtube.com/embed/" . preg_replace("/^.*?(v=|\.be\/)([^\&]+).*?$/", "$2", $data["video"]) . "\" frameborder=\"0\" allowfullscreen></iframe>\n";
                        }
                    } else if (trim($data['doc']) != "") {
                        $nouvelles .= "<li><a href=\"" . $nouvelle_url . "\" target=\"_blank\">" . toHtml($data['titre']) . "</a></li>";

                    } else {
                        $nouvelles .= "<li>" . toHtml($data['titre']) . "</li>";
                    }
                }
                $nouvelles .= "</ul>";

            }


        }


        $DB->query(
            "SELECT e.* ".
            "FROM etudes e ".
            "WHERE e.key = '46' "
        );

        $nouvelles_array = array();
        while ($DB->next_record()) {
            for ($n = 1; $n <= NB_NOUVELLES; $n++) {
                if ($DB->getField('nouvelle_' . $n . '_visible')) {
                    $nouvelles_array[$n]['titre'] = $DB->getField('nouvelle_' . $n . '_titre');
                    $nouvelles_array[$n]['doc'] = $DB->getField('nouvelle_' . $n . '_doc');
                    $nouvelles_array[$n]['video'] = $DB->getField('nouvelle_' . $n . '_video');
                }
            }
        }

        $videos_ss = array();

        if (count($nouvelles_array) > 0) {
            foreach ($nouvelles_array as $nb => $data) {

                $nouvelle_url = EXTRANET_DOCS_ETUDES_NOUVELLES_URL . "46" . "/" . $data['doc'];

                if (preg_match("/(http:\/\/)?(www\.[a-z0-9\-]+\.[a-z]+)/",$data['titre'])) {
                    $nouvelle_url = preg_replace("/.*?(http:\/\/)?(www\.[a-z0-9\-]+\.[a-z]+).*?/", "http://$2", $data['titre']);
                    $data['doc'] = ".";
                }

                if (trim($data['video']) != "") {
                    //$videos_ss[] = toHtml($data['titre']) . "<br /><iframe width=\"298\" height=\"168\" src=\"https://www.youtube.com/embed/" . preg_replace("/^.*?(v=|\.be\/)([^\&]+).*?$/", "$2", $data["video"]) . "\" frameborder=\"0\" allowfullscreen></iframe>\n";

                    if (trim($data['doc']) != "") {
                        $videos_ss[] = toHtml($data['titre']) . "<br /><iframe width=\"298\" height=\"168\" src=\"https://www.youtube.com/embed/" . preg_replace("/^.*?(v=|\.be\/)([^\&]+).*?$/", "$2", $data["video"]) . "\" frameborder=\"0\" allowfullscreen></iframe><br />\n".
                            "<a href=\"" . $nouvelle_url . "\" target=\"_blank\" style=\"color: #5c777f;\">Notes PDF</a>\n";
                    } else {
                        $videos_ss[] = toHtml($data['titre']) . "<br /><iframe width=\"298\" height=\"168\" src=\"https://www.youtube.com/embed/" . preg_replace("/^.*?(v=|\.be\/)([^\&]+).*?$/", "$2", $data["video"]) . "\" frameborder=\"0\" allowfullscreen></iframe>\n";
                    }

                }
            }
        }


        return $nouvelles;

    }

    function getEtudeIDByURL($url) {
        global $DB;

        $DB->query(
            "SELECT e.* ".
            "FROM etudes e ".
            "WHERE e.actif > 0 ".
            "ORDER BY nom ASC"
        );

        $key = "";
        while ($DB->next_record()) {
            if ($url == asciionly($DB->getField("nom"))) {
                $key = $DB->getField("key");
            }
            //echo "<!--" . $url . " != " . asciionly($DB->getField("nom")) . " = " . $key . "-->\n";
        }


        return $key;
    }

    function getEtudesList() {
        global $DB, $_CUSTOM_TITLE;

        $DB->query(
            "SELECT e.*, es.ville ".
            "FROM etudes e ".
            "INNER JOIN etudes_succursales es ON es.etudes_key = e.key ".
            "WHERE e.nombre_notaires_associes > 0 ".
            "ORDER BY nom ASC"
        );

        $etudes = array();
        $html = "<ul class=\"etude-list\">";
        while ($DB->next_record()) {
            if (!isset($etudes["e" . $DB->getField("key")])) {
                $html .= "<li><a href=\"" . __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "\">" . $DB->getField("nom") . "</a></li>";
                $etudes["e" . $DB->getField("key")] = 1;
            }
        }
        $html .= "</ul>";

        return $html;
    }

    function getEtudesMap($page = "") {
        global $DB, $_CUSTOM_TITLE;

        $DB->query(
            "SELECT e.*, es.ville ".
            "FROM etudes e ".
            "INNER JOIN etudes_succursales es ON es.etudes_key = e.key ".
            "WHERE e.nombre_notaires_associes > 0 ".
            "AND es.actif = '1' ".
            "AND e.actif = '1' ".
            "ORDER BY nom ASC"
        );

        $etudes = array();
        $html = "<ul class=\"etude-list\">";
        while ($DB->next_record()) {
            if (!isset($etudes["e" . $DB->getField("key")])) {
                $html .= "<li><a href=\"" . __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "" . ($page == "rv" ? "/rendez-vous" : ($page == "di" ? "/demande-information" : "")) . "\">" . $DB->getField("nom") . "</a></li>";
                $etudes["e" . $DB->getField("key")] = 1;
            }
        }
        $html .= "</ul>";

        $cities = explode(" - ", getCityList("", $page));


        $coords = getCoordsArray();

        $map = "";
        foreach ($coords AS $etude) {
            //foreach ($yarray AS $y => $etude) {
            if ($etude['visible'] == 1) {

                $lat = $etude['lat'];
                $lng = $etude['long'];
                $ville = toHtml($etude['ville']);
                $title = toHtml($etude['nom']);

                if ($lat != "") {

                  $url = __("/notaires-") . asciionly($etude['ville']) . "/" . asciionly($etude['nom']) . "" . ($page == "rv" ? "#contact" : ($page == "di" ? "#contact" : ""));

                  $map .=
                      "<div class=\"etude address\" data-map='{\"lat\":\"" . $lat . "\", \"lng\":\"" . $lng . "\", \"title\":\"" . $title . " " . $ville . "\", \"name\":\"" . stripslashes($etude['nom']) . "\", \"url\":\"" . $url . "\", \"info\":\"" . $ville . "\"}' style=\"\">\n".
                      "</div>";
                    }
            }
            //}
        }

        $html =
            "<div class=\"map\">\n".
            " <form action=\"" . __("/notaires") . "\" method=\"post\">\n".
            "   <h3>Recherche par nom (&Eacute;tude ou notaire)</h3>".
            "   <input type=\"text\" id=\"sidescity\" name=\"scity\" value=\"\" />\n".
            "   <input type=\"submit\" id=\"mapsubmit\" value=\"OK\" />\n".
            " </form>\n".
            " <div id=\"gmap\"></div>" . $map .
            "</div>".
            "<div class=\"col_city\">\n".
            "   <ul>";
        foreach ($cities AS $city) {
            $html .= "<li>" . $city . "</li>";
        }
        $html .= "</ul></div>";

        return $html;
    }


    function getEtudesByCity($city) {
        global $DB, $_CUSTOM_TITLE, $title, $nouvelles;
        echo ".-.";
        $DB->query(
            "SELECT e.*, es.* ".
            "FROM etudes_succursales es INNER JOIN etudes e ON es.etudes_key = e.key ".
            "WHERE e.nombre_notaires_associes > 0 ".
            "AND e.actif = '1' ".
            //"AND es.siege_social = '1' ".
            "AND es.actif = '1' "
        );

        $etudes = array();
        $rows = $DB->getNumRows();

        $nb_etudes = 0;
        $lien_etude = "";
        $html = "<ul class=\"etude-list\">";
        while ($DB->next_record()) {

            if (addslashes(strtolower(asciionly($city))) == asciionly($DB->getField("ville"))) {
                if ($rows == 1) {
                    $_CUSTOM_TITLE = $DB->getField('ville') . " " . $DB->getField('nom') . " ";
                    $title = $DB->getField("ville");
                } else {
                    $_CUSTOM_TITLE = $DB->getField('ville') . " ";
                    $title = $DB->getField("ville");
                }

                if (!isset($etudes["e" . $DB->getField("key")])) {
                    $lien_etude = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom"));

                    if ($_GET["p"] == "di") {
                        $lien_etude .= "/demande-information";
                    } else if ($_GET["p"] == "rv") {
                        $lien_etude .= "/rendez-vous";
                    }

                    $html .= "<li><a href=\"" . $lien_etude . "\">" . $DB->getField("nom") . "</a> <p class=\"adresse\">" . $DB->getField("adresse") . ", " . $DB->getField("ville") . "</p></li>";
                    $etudes["e" . $DB->getField("key")] = 1;
                    $nb_etudes++;
                }
            }
        }
        $html .= "</ul>";


        if ($nb_etudes == 1 || count($liens) == 1) {
            header("Location: " . $lien_etude);
            exit;
        }

        return $html;
    }


    function getEtudesByKeyword($keyword) {
      global $NORMALIZED_SORTING_TABLE_UTF8, $DB, $_CUSTOM_TITLE, $title, $nouvelles;

      $_CUSTOM_TITLE = "Recherche : " . stripslashes(stripslashes($_POST["cp"]) . " " . $_POST["scity"]);
      $title = "Recherche : " . stripslashes(stripslashes($_POST["cp"]) . " " . $_POST["scity"]);

      if ($_POST["cp"] != "") {
        $keyword .= " " . $_POST["cp"];
      }


      $normalized_search_string = strtolower(strtr(trim(stripslashes($keyword)), $NORMALIZED_SORTING_TABLE_UTF8));
      $normalized_search_terms = explode(' ', $normalized_search_string);


      if (strlen($normalized_search_string) > 0) {

        $DB->query(
          "SELECT DISTINCT ".
          " em.key AS employe_key, ".
          " em.nom AS employe_nom, ".
          " em.prenom AS employe_prenom, ".
          " em.description AS employe_description, ".
          " em.courriel AS employe_courriel, ".
          " em.expertises_droit_affaires AS employe_droit_affaires, ".
          " em.expertises_droit_personne AS employe_droit_personne, ".
          " em.expertises_droit_immobilier AS employe_droit_immobilier, ".
          " em.expertises_sectorielles AS employe_expertises_sectorielles, ".
          " et.key AS etude_key, ".
          " et.nom AS etude_nom, ".
          " et.description AS etude_description ".
          "FROM ".
          " `employes` AS em, ".
          " `etudes` AS et, ".
          " `etudes_succursales` AS et_su, ".
          " `employes_etudes_succursales` AS em_et_su, ".
          " `fonctions` AS fo, ".
          " `employes_fonctions` AS em_fo ".
          "WHERE ".
          " em.actif = '1' ".
          " AND et_su.etudes_key = et.key ".
          " AND em_et_su.etudes_succursales_key = et_su.key ".
          " AND em_et_su.employes_key = em.key ".
          " AND em_fo.employes_key = em.key ".
          " AND em_fo.fonctions_key = fo.key ".
          " AND fo.key IN ('8','15') ".
          " AND et.actif = '1' ".
          " AND et_su.actif = '1' ".
          "ORDER BY ".
          " em.nom ASC;"
        );

        $result = array();
        $i = 0;
        while ($record = $DB->next_record()) {
          $result[$i]['employe_key'] = $DB->getField('employe_key');
          $result[$i]['employe_nom'] = $DB->getField('employe_nom');
          $result[$i]['employe_description'] = $DB->getField('employe_description');
          $result[$i]['employe_prenom'] = $DB->getField('employe_prenom');
          $result[$i]['employe_courriel'] = $DB->getField('employe_courriel');
          $result[$i]['employe_droit_affaires'] = $DB->getField('employe_droit_affaires');
          $result[$i]['employe_droit_personne'] = $DB->getField('employe_droit_personne');
          $result[$i]['employe_droit_immobilier'] = $DB->getField('employe_droit_immobilier');
          $result[$i]['employe_expertises_sectorielles'] = $DB->getField('employe_expertises_sectorielles');
          $result[$i]['etude_key'] = $DB->getField('etude_key');
          $result[$i]['etude_nom'] = $DB->getField('etude_nom');
          $result[$i]['etude_description'] = $DB->getField('etude_description');
          $result[$i]['etude_ville'] = '';
          $result[$i]['etude_villes'] = '';

          $DB->query(
            "SELECT ".
            " et_su.ville AS etude_ville, ".
            " et_su.siege_social AS siege_social ".
            "FROM ".
            " `etudes_succursales` AS et_su ".
            "WHERE ".
            " et_su.etudes_key = '" . $result[$i]['etude_key'] . "' AND et_su.actif = '1' ".
            "ORDER BY et_su.ville;"
          );

          while ($record2 = $DB->next_record()) {
            if ($DB->getField('siege_social') == 1) {
              $result[$i]['etude_ville'] = $DB->getField('etude_ville');
            }
            if (!preg_match("/" . $DB->getField('etude_ville') . "/", $result[$i]['etude_villes'])) {
              $result[$i]['etude_villes'] .= $DB->getField('etude_ville') . ', ';
            }

          }

          $i++;
        }

        if (is_array($result) && count($result) > 0) {

          $HTML .= "<table id=\"searchresult\" cellspacing=\"0\" cellpadding=\"0\">\n";

          $done = array();
          foreach ($result as $i => &$info) {

            for($i = 0; $i < count($normalized_search_terms); $i++) {
              $term = $normalized_search_terms[$i];
              $normalized_result_notaire = strtolower(strtr(stripslashes(($info['employe_prenom'] . " " . $info['employe_nom'])), $NORMALIZED_SORTING_TABLE_UTF8));
              $normalized_result_etude = strtolower(strtr(stripslashes(($info['etude_nom'])), $NORMALIZED_SORTING_TABLE_UTF8));
              $normalized_result_ville = strtolower(strtr(stripslashes(($info['etude_ville'])), $NORMALIZED_SORTING_TABLE_UTF8));
              $normalized_result_villes = strtolower(strtr(stripslashes(($info['etude_villes'])), $NORMALIZED_SORTING_TABLE_UTF8));

              $normalized_result_eda = strtolower(strtr(stripslashes(($info['employe_droit_affaires'])), $NORMALIZED_SORTING_TABLE_UTF8));
              $normalized_result_edp = strtolower(strtr(stripslashes(($info['employe_droit_personne'])), $NORMALIZED_SORTING_TABLE_UTF8));
              $normalized_result_edi = strtolower(strtr(stripslashes(($info['employe_droit_immobilier'])), $NORMALIZED_SORTING_TABLE_UTF8));
              $normalized_result_ees = strtolower(strtr(stripslashes(($info['employe_expertises_sectorielles'])), $NORMALIZED_SORTING_TABLE_UTF8));
              //$normalized_result_eda = strtolower(strtr(stripslashes(($info['employe_droit_affaires'])), $NORMALIZED_SORTING_TABLE_UTF8));

              $normalized_result_etude_desc = strtolower(strtr(stripslashes(($info['etude_description'])), $NORMALIZED_SORTING_TABLE_UTF8));
              $normalized_result_employe_desc = strtolower(strtr(stripslashes(($info['employe_description'])), $NORMALIZED_SORTING_TABLE_UTF8));

              if (!in_array($info['employe_key'], $done)) {

                if (
                  strpos($normalized_result_notaire, $term) !== false
                  || strpos($normalized_result_etude, $term) !== false
                  || strpos($normalized_result_ville, $term) !== false
                  || strpos($normalized_result_villes, $term) !== false


                  || strpos($normalized_result_eda, $term) !== false
                  || strpos($normalized_result_edp, $term) !== false
                  || strpos($normalized_result_edi, $term) !== false
                  || strpos($normalized_result_ees, $term) !== false

                  || strpos($normalized_result_etude_desc, $term) !== false
                  || strpos($normalized_result_employe_desc, $term) !== false


                  || (strpos($normalized_search_string, 'affaire') !== false && trim($info['employe_droit_affaires']) !== '')
                  || (strpos($normalized_search_string, 'personne') !== false && trim($info['employe_droit_personne']) !== '')
                  || (strpos($normalized_search_string, 'immobilie') !== false && trim($info['employe_droit_immobilier']) !== '')
                  || (strpos($normalized_search_string, 'sectoriel') !== false && trim($info['employe_expertises_sectorielles']) !== '')
                ) {

                  if ($info['etude_key'] == 24) {
                    //$ville = "Boucherville, Longueuil, Montr&eacute;al";
                    $ville = toHtml(preg_replace("/\,\s+$/", "", $info['etude_villes']));
                  } else {
                    //$ville = toHtml($info['etude_ville']);
                    $ville = toHtml(preg_replace("/\,\s+$/", "", $info['etude_villes']));
                  }

                  $lien_etude = __("/notaires-") . asciionly($info['etude_ville']) . "/" . asciionly($info['etude_nom']);

                  if (($i+1) == count($normalized_search_terms)) {

                    $HTML .= "<tr>\n";
                    $HTML .= "<td width=\"200px\"><a href=\"" . $lien_etude . "/equipe#employe_" . $info['employe_key'] . "\">Me " . toHtml($info['employe_nom']) . ", " . toHtml($info['employe_prenom']) . "</a><br />&nbsp;</td>\n";
                    $HTML .= "<td><a href=\"" . $lien_etude . "\">" . toHtml($info['etude_nom']) . "</a><br />" . $ville . "</td>";
                    $HTML .= "</tr>";

                    $nb_etudes++;

                    $done[] = $info['employe_key'];
                  }

                } else {
                  $done[] = $info['employe_key'];
                }
              }
            }
          }

          $HTML .= "</table>\n";

        }

        if (count($done) == 0) {
          $HTML .= "<span style=\"margin-left:12px;\">Aucun r&eacute;sultat.</span>\n";
        }

      } else {
        $HTML .= "<span style=\"margin-left:12px;\">Aucun r&eacute;sultat.</span>\n";
      }


        if ($nb_etudes == 1) {
            //header("Location: ". $lien_etude);
            exit;
        }

        return $HTML;
    }



    function getCoordsArray() {
        global $DB;

        $coordsArray = array();

        $DB->query(
            "SELECT e.nom AS nom, es.ville, es.lat, es.long ".
            "FROM `etudes` AS e ".
            "INNER JOIN `etudes_succursales` AS es ON es.etudes_key = e.key ".
            "WHERE e.actif = '1' ".
            "AND es.actif = '1' ".
            "ORDER BY e.key ASC;"
        );

        while ($record = $DB->next_record()) {
            $nom = $DB->getField('nom');
            $info = $DB->getField('info');
            $ville = $DB->getField('ville');

            // $coordsArray[$x][$y]['visible'] = 1;
            // $coordsArray[$x][$y]['lat'] = $DB->getField('lat');
            // $coordsArray[$x][$y]['long'] = $DB->getField('long');

            // $coordsArray[$x][$y]['nom'] = $nom;
            // $coordsArray[$x][$y]['info'] = $info;
            // $coordsArray[$x][$y]['ville'] = $ville;

            $coordsArray[] = array('visible' => 1, 'lat' => $DB->getField('lat'), 'long' => $DB->getField('long'), 'nom' => $nom, 'ville' => $ville, 'info' => $ville);
        }

        // echo "<!--";
        // print_r($coordsArray);
        // echo "-->";

        return $coordsArray;
    }

    function url_exists($url) {
        $file_headers = @get_headers($url);

        if(preg_match("/404 /", $file_headers[0])){
              return false;
        } else if (preg_match("/302 /", $file_headers[0]) && preg_match("/404 /", $file_headers[7])) {
            return false;
        } else {
            return true;
        }
    }