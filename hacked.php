<?php

set_time_limit(300);

hackedWalk(__DIR__);

function hackedWalk($dir) {

    $current = scandir($dir);
    for ($i = 0; $i < count($current); $i++) {
        if ($current[$i] != "." && $current[$i] != "..") {
            if (is_dir($dir . "/" . $current[$i] . "/")) {
                //echo "DIR: " . $dir . "/" . $current[$i] . "/<br />";
                hackedWalk($dir . "/" . $current[$i]);
            } else {
                hackedFile($dir . "/" . $current[$i]);
            }
        }
    }

}

function hackedFile($file) {
    $score = getHackedScore($file);
    if ($score >= 20) {
        echo "<strong>" . $score . " (" . date("Y-m-d", filemtime($file)) .") - " . $file . "</strong><br />";
    } else {

        // if (filemtime($file) > strtotime("2018-05-26")) {
        //     echo "<strong>" . $score . " (" . date("Y-m-d", filemtime($file)) .") - " . $file . "</strong><br />";
        // }

        //echo $score . " - " . $file . "<br />";
    }



}

function getHackedScore($file) {
    $score = 0;

    if (file_exists($file)) {
        $ico = isICO($file);
        $php = isPHP($file);

        if (($ico + $php)) {
            $content = file_get_contents($file);

            $score += $ico;
            $score += $php;
            $score += hasFakeComment($content);
            $score += hasHexCode($content);
            $score += hasCookies($content);
            $score += hasLongLinesWithoutSpaces($content);
            $score += hasEval($content);
            $score += hasExit($content);
            $score += hasBase64($content);
            $score += hasMD5($content);
            $score += hasWeirdContactenatedArrays($content);
            $score += hasAlreadyRun($content);
            $score += hasFuckingLongLines($content);
            $score += hasInclude($content);
            $score += hasCreateFunction($content);
            $score += hasLinesWithLotsOfPercent($content);
        }
    }

    return $score;
}

function isICO($file) {
    if (strpos(strtolower($file), ".ico") !== false) { return 25; }
    return 0;
}

function isPHP($file) {
    if (strpos(strtolower($file), ".php") !== false) {
        if (preg_match("/[bcdfghjklmnpqrstvwxz]{4}/is", $file)) {
            return 15;
        }
        return 4;
    }
    return 0;
}


function hasFakeComment($content) {
    if (preg_match("/\/\*[a-z0-9]+\*\//is", $content)) { return 5; }
    return 0;
}

function hasHexCode($content) {
    if (strpos($content, "\x") !== false) { return 5; }
    return 0;
}

function hasCookies($content) {
    //_COOKIE
    if (strpos($content, "_COOKIE") !== false) { return 3; }
    return 0;
}

function hasExit($content) {
    if (strpos($content, "exit;") !== false) { return 5; }
    return 0;
}
function hasLongLinesWithoutSpaces($content) {
    if (preg_match("/[^\s]{150}/is", $content)) { return 5; }
    return 0;
}

function hasEval($content) {
    if (strpos($content, "eval(") !== false) { return 10; }
    return 0;
}

function hasBase64($content) {
    if (strpos($content, "base64_") !== false) { return 10; }
    return 0;
}

function hasMD5($content) {
    if (strpos($content, "md5(") !== false) { return 5; }
    return 0;
}

function hasWeirdContactenatedArrays($content) {
    //$blwditt[29].$blwditt[29].$blwditt[31]
    if (preg_match("/\$[a-z0-9]+\[[0-9]+\]\./is", $content)) { return 5; }

    if (preg_match("/\[[a-z]+\]/is", $content)) { return 10; }

    return 0;
}

function hasAlreadyRun($content) {
    //ALREADY_RUN
    if (strpos($content, "ALREADY_RUN") !== false) { return 5; }
    return 0;
}

function hasFuckingLongLines($content) {
    if (preg_match("/[^\n]{250}/is", $content)) { return 5; }
    return 0;
}

function hasInclude($content) {
    //@include
    if (strpos($content, "@include") !== false) { return 6; }
    return 0;
}

function hasCreateFunction($content) {
    if (strpos($content, "create_function") !== false) { return 20; }
    return 0;
}



function hasLinesWithLotsOfPercent($content) {
    //Cn%ja%j2%Cj%Cj%Cj%Cj%Cj%Cj%Cj%Cj%YG%ja%j2%Cj%Cj%Cj%Cj%Cj%Cj%Cj%Cj%Cj%Cj%Cj%Cj%CxShyDprb%
    if (preg_match("/([a-z0-9]+\%){10}/is", $content)) { return 15; }
    return 0;
}




