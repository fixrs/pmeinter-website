<?php
   $messagefete = array(
      "2018" => array (
         "anjou" => "https://www.youtube.com/embed/3ZggczRMDJk?rel=0",
         "stemarie" => "https://www.youtube.com/embed/6d-O73130N4?rel=0",
         "boucherville" => "https://www.youtube.com/embed/Sy9a9-qNkfk?rel=0",
         "drummondville" => "https://www.youtube.com/embed/dcgTXLTmLsw?rel=0",
         "amos" => "https://www.youtube.com/embed/ymZ2wWIlDuA?rel=0",
         "gatineau" => "https://www.youtube.com/embed/ap8IwieuDN4?rel=0"
      ),
      "2019" => array (
        "abitibi" => "/fetes/2019/2019-abitibi.mp4",
        "pfdnotaires" => "/fetes/2019/2019-pfdnotaires.mp4",
        "gatineau" => array(
          "fr" => "/fetes/2019/2019-fr-gatineau.mp4",
          "en" => "/fetes/2019/2019-en-gatineau.mp4"
        ),
        "gatineau-fr" => "/fetes/2019/2019-fr-gatineau.mp4",
        "gatineau-en" => "/fetes/2019/2019-en-gatineau.mp4",

        "drummondville" => "/fetes/2019/2019-drummondville.mp4",
        "magog" => "/fetes/2019/2019-magog.mp4",
        "mtl" => "/fetes/2019/2019-mtl.mp4",
        "ste-marie" => "/fetes/2019/2019-ste-marie.mp4"
      ),
      "2020" => array (
        "abitibi" => "/fetes/2020/2020-abitibi.mp4",
        "lrv" => "/fetes/2020/2020-lrv.mp4",
        "gatineau" => "/fetes/2020/2020-gatineau.mp4",
        "gatineau-fr" => "/fetes/2020/2020-gatineau.mp4",
        "gatineau-en" => "/fetes/2020/2020-gatineau.mp4",
        "drummondville" => "/fetes/2020/2020-dauphinais_julien.mp4",
        "vachon-breton" => "/fetes/2020/2020-vachon_breton.mp4",
        "mtl" => "/fetes/2020/2020-montreal.mp4",
        "ste-marie" => "/fetes/2020/2020-vachon_breton.mp4"
      ),
      "2021" => array (
        "abitibi" => "/fetes/2021/2021-abitibi.mp4",
        "gatineau" => "/fetes/2021/2021-gatineau-fr.mp4",
        "gatineau-fr" => "/fetes/2021/2021-gatineau-fr.mp4",
        "gatineau-en" => "/fetes/2021/2021-gatineau-en.mp4",
        "lrv" => "/fetes/2021/2021-lrv.mp4",
        "mtl-anjou" => "/fetes/2021/2021-mtl-anjou.mp4",
        "ste-marie" => "/fetes/2021/2021-ste-marie.mp4"
      )
   );

   if (isset($_GET["a"]) && isset($_GET["v"])) {
      $lien=$messagefete[$_GET["a"]][str_replace("/", "", $_GET["v"])];
    } else {
      header("Location: http://pmeinter.com");
      exit;
    }
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PME Inter vous souhaite de joyeuses fêtes!</title>
    <style>
            iframe, video { position:absolute; left:0%; top:0%; }
            video { display: block; width: 100%; height: 100%; }
            video#fr, video#en { display: none; width: 100%; height: 100%; }
            .language ul { list-style-type: none; width: 320px; position: absolute; left: 50%; margin-left: -160px; top: 40%; }
            .language ul li { background-color: #000; padding: 20px 10px; position: relative; float: left; text-align: center; display: block; margin-left: 5px; margin-right: 5px; color: #FFF; font-weight: bold; cursor: pointer; }
        </style>
    </head>
    <body>
        <?php

          if (isset($_GET["a"]) && $_GET["a"] > 2018) {

            if (is_array($messagefete[$_GET["a"]][str_replace("/", "", $_GET["v"])])) {

              echo
                '<div class="language">
                  <ul id="choices">
                    <li class="fr" onclick="document.getElementById(\'fr\').style.display=\'block\'; document.getElementById(\'choices\').style.display=\'none\';document.getElementById(\'fr\').play();">Français</li>
                    <li class="en" onclick="document.getElementById(\'en\').style.display=\'block\'; document.getElementById(\'choices\').style.display=\'none\'; document.getElementById(\'en\').play();">English</li>
                  </ul>';

              foreach ($messagefete[$_GET["a"]][str_replace("/", "", $_GET["v"])] as $lang => $video) {
                $lien = $messagefete[$_GET["a"]][str_replace("/", "", $_GET["v"])][$lang];
                echo '<video id="' . $lang . '" controls muted><source src="' . $lien . '" type="video/mp4"></video>';

              }

            } else {
              if (str_replace("/", "", $_GET["v"]) == "ste-marie") {
                echo '<iframe src="https://www.youtube.com/embed/y_glMrEwrsU?rel=0&autoplay=1&mute=1" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
              } else {
                echo '<video controls autoplay muted><source src="' . $lien . '" type="video/mp4"></video>';
              }
            }

          } else {
            echo '<iframe src="' . $lien . '" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
          }
        ?>
    </body>
</html>

