<?php
	require('wp-load.php');

	$NORMALIZED_SORTING_TABLE = array(
		'á' => 'a', 'à' => 'a', 'â' => 'a', 'ä' => 'a', 'ã' => 'a', 'å' => 'a',
		'Á' => 'A', 'À' => 'A', 'Â' => 'A', 'Ä' => 'A', 'Ã' => 'A', 'Å' => 'A',
		'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ȩ' => 'e',
		'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ȩ' => 'E',
		'í' => 'i', 'ì' => 'i', 'î' => 'i', 'ï' => 'i',
		'Í' => 'I', 'Ì' => 'I', 'Î' => 'I', 'Ï' => 'I',
		'ó' => 'o', 'ò' => 'o', 'ô' => 'o', 'ö' => 'o', 'õ' => 'o',
		'Ó' => 'O', 'Ò' => 'O', 'Ô' => 'O', 'Ö' => 'O', 'Õ' => 'O',
		'ú' => 'u', 'ù' => 'u', 'û' => 'u', 'ü' => 'u',
		'Ú' => 'U', 'Ù' => 'U', 'Û' => 'U', 'Ü' => 'U',
		'ñ' => 'n', 'Ñ' => 'N', 'ç' => 'c', 'Ç' => 'C', 'ý' => 'y', 'Ý' => 'Y',
		'æ' => 'ae', 'Æ' => 'AE', 'œ' => 'oe'
	);

	foreach ($NORMALIZED_SORTING_TABLE as $key => $value) {
		$NORMALIZED_SORTING_TABLE_UTF8[$key] = $value;
	}




	$_CUSTOM_TITLE = "";
 	$html = "";
 	$title = "";
 	$submenumap = "";
 	$nouvelles = "";

 	$DB = new DB(DB_HOSTNAME_EXTRANET, DB_USER_EXTRANET, DB_PASSWORD_EXTRANET, DB_NAME_EXTRANET);


 	if ($_GET["etude_dd"] == 1) {
 		$DB->query(
			"SELECT ".
			"	e.nom AS nom, ".
			"	e.courriel AS courriel, ".
			"	es.ville AS ville, ".
			"	es.code_postal AS code_postal ".
			"FROM ".
			"	`etudes` AS e, ".
			"	`etudes_succursales` AS es ".
			"WHERE ".
			"	es.etudes_key = e.key ".
			"	AND es.actif = '1' ".
			"	AND e.actif = '1' ".
			"ORDER BY ".
			"	ville ASC, ".
			"	nom ASC, ".
			"	code_postal ASC;"
		);

 		$dd = "";
		$succursale_ville_prev = "";
		while ($record = $DB->next_record()) {
			$etude_nom = $DB->getField('nom');
			$etude_courriel = $DB->getField('courriel');
			$succursale_ville = $DB->getField('ville');
			$succursale_code_postal = $DB->getField('code_postal');

			if ($succursale_ville != $succursale_ville_prev) {
				if ($succursale_ville_prev != "") {
					$dd .=  "</optgroup>";
				}
				$dd .= "<optgroup label=\"" . toHtml($succursale_ville) . "\">";
			}

			$succursale_ville_prev = $succursale_ville;

			//if (getorpost('etude') == $select_value) {
			//	$selected = "selected=\"selected\"";
			//} else {
			//	$selected = "";
			//}

			$dd .= "<option value=\"" . $etude_courriel . "\" " . $selected . ">" . toHtml($etude_nom . " [" . $succursale_code_postal . "]") . "</option>";
		}

		echo "<option value=\"\"></option>" . $dd;

 		exit;
 	}


 	add_filter('body_class','dynamic_body_class');

 	if (count($_POST) > 0) {
 		$title = __("Trouver un <span>notaire</span>");
 		$html .= "<h2>" . __("Recherche") . "</h2>";

 		if ($_POST['scity'] != "") {
 			$html = getEtudesByKeyword($_POST["scity"]);
 		} else if ($_POST["cp"] != "") {
 			$html = getEtudesByKeyword($_POST["scity"]);
 		} else if ($_POST["city"] != "") {
 			//ON CONNAIT LA VILLE
 			$html = getEtudesByCity($_POST["city"]);
 		} else {
 			$html .= getEtudesList();
 		}

 		if (trim($html) == "") {
 			$html = "<p>Votre requête n'a retourné aucune étude.</p>";
 		}
 	} else if (count($_GET) > 0) {
 		if ($_GET["p"] == "equipe") {
 			//ON CONNAIT L'ETUDE ET ON VEUT L'EQUIPE
 			$html = getEtudeEquipe(getEtudeIDByURL($_GET["e"]));
 		} else if ($_GET["p"] == "demande") {

 			$html = getDemandeInfo($_GET["e"]);

 		} else if ($_GET["p"] == "rendez-vous") {

 			$html = getDemandeInfo($_GET["e"], "rendez-vous");

 		} else if ($_GET["e"] != "") {
 			//ON CONNAIT L'ETUDE
 			$html = getEtude(getEtudeIDByURL($_GET["e"]));
 		} else if ($_GET["v"] != "") {
 			//ON CONNAIT LA VILLE
 			$html = getEtudesByCity($_GET["v"]);

 		} else if ($_GET["l"] == 1) {
 			//ACCES PAR LISTE
 			$title = __("Trouver un <span>notaire</span>");
 			$html .= "<h2>" . __("Accès par liste") . "</h2>";

 			$html .= getEtudesList();
 		} else if ($_GET["c"] == 1) {
 			//ACCES PAR CARTE



 			if ($_GET["p"] == "rv") {
 				$title = __("Prendre <span>rendez-vous</span>");
 				$html .= "<h2>" . __("Accès par carte") . "</h2>";
 				$html .= getEtudesMap("rv");
 			} else if ($_GET["p"] == "di") {
 				$title = __("Demande <span>d'information</span>");
 				$html .= "<h2>" . __("Accès par carte") . "</h2>";
 				$html .= getEtudesMap("di");
 			} else if ($_GET["p"] == "stage") {
 			 	$title = __("Prendre <span>rendez-vous</span>");
 				$html .= "<h2>" . __("Accès par carte") . "</h2>";
 				$html .= getEtudesMap("rv");
 			} else {
 				$title = __("Trouver un <span>notaire</span>");
 				$html .= "<h2>" . __("Accès par carte") . "</h2>";
 				$html .= getEtudesMap();
 			}
 		}
 	} else {

 	}

	$dynamic_class = "page-dynamique";

	get_header();

	echo
		"<div id=\"primary\" class=\"content-area\">\n".
		"	<div id=\"content\" class=\"site-content\" role=\"main\">\n".
		"		<div class=\"page\">\n".
		"			<header class=\"entry-header\">\n".
		"				<h1 class=\"entry-title\">" . $title . "</h1>\n".
		"			</header>\n".
		"			<div class=\"entry-content etude\">\n".
		$html .
		"			</div>\n".
		"		</div>\n".
		"	</div> \n".
		"</div>\n";

 	get_sidebar();
	get_footer();

	function dynamic_body_class() {
		$demo2018 = "";
        //if (isset($_COOKIE["DEMO2018"])) {
            $classes[] = "demo2018";
        //}

		if (count($_POST) > 0) {
	 		$classes[] = "recherche";
	 		if ($_POST['scity'] != "") {
	 			$classes[] = "q-city";
	 		} else if ($_POST['cp'] != "") {
	 			$classes[] = "q-city";
	 		} else if ($_POST["city"] != "") {
	 			$classes[] = "city";
	 		} else {
	 			$classes[] = "empty";
	 		}
	 	} else if (count($_GET) > 0) {
	 		if ($_GET["p"] == "equipe") {
	 			$classes[] = "equipe";
	 		} else if ($_GET["p"] == "rendez-vous") {
	 			$classes[] = "demande-rv";
	 		} else if ($_GET["p"] == "demande") {
	 			$classes[] = "demande-info";
	 		} else if ($_GET["e"] != "") {
	 			$classes[] = "etude";
	 		} else if ($_GET["v"] != "") {
	 			$classes[] = "city";
	 		} else if ($_GET["l"] == 1) {
	 			$classes[] = "city-list";
	 		} else if ($_GET["c"] == 1) {
	 			$classes[] = "map";
	 		}
	 	}
	 	return $classes;
	}

	function getDemandeInfo($etude, $form = "") {
		global $DB, $_CUSTOM_TITLE, $title, $nouvelles, $submenumap;

		$DB->query(
			"SELECT e.*, es.ville ".
			"FROM etudes e ".
			"INNER JOIN etudes_succursales es ON e.key = es.etudes_key ".
			"WHERE e.nombre_notaires_associes > 0 ".
			"AND es.siege_social = '1' ".
			"AND es.actif = '1' ".
			"AND e.actif = '1' "
		);


		$rows = $DB->getNumRows();
		$email = "";
		$title_h2 = "";
		$etude_id = "";

		$etude_url = "";
		$equipe_url = "";
		$notaires_demande_url = "";
		$html = "";
		$description = "";

		while ($DB->next_record()) {
			if (addslashes(strtolower($etude)) == asciionly($DB->getField("nom"))) {

				if (trim($etude_url . $equipe_url . $notaires_demande_url) == "") {
					$etude_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom"));
					$equipe_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "/equipe";
					$notaires_demande_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "/demande-information";
	 				$html .= "<h5>" . __("Notre <span>Étude</span>") . "</h5>";
				}

				$_CUSTOM_TITLE = $DB->getField('nom') . " ";

				if ($form == "rendez-vous") {
					$title = "Prendre rendez-vous";
				} else {
					$title = "Demande d'information";
				}

				$title_h2 = $DB->getField("nom");
				$email = $DB->getField("courriel");
				$etude_id = $DB->getField("key");
			}
		}

		if (trim($email) == "") { $email = "info@pmeinter.com"; }

		ob_start();
		if ($form == "rendez-vous") {
			gravity_form(4, false, true, false, array('etude' => $etude, 'etude_courriel' => $email), true);
			$webform = ob_get_contents();
		} else {
			gravity_form(3, false, true, false, array('etude' => $etude, 'etude_courriel' => $email), true);
			$webform = ob_get_contents();
		}
		ob_end_clean();

		if ($form == "rendez-vous") {
			$html = preg_replace("/action=\'([^\']+?)\'/s", "action=\"/rendez-vous/\"", $webform);
		} else {
			$html = preg_replace("/action=\'([^\']+?)\'/s", "action=\"/demande-dinformation/\"", $webform);
		}

		$html = "<h2>" . $title_h2 . "</h2>" . $html;

		$submenumap =
			"<ul class=\"navigationEtude\">".
			" <li id=\"smetude\"><a href=\"" . $etude_url . "\">" . __("Étude") . "</a></li>".
			" <li id=\"smequipe\"><a href=\"" . $equipe_url . "\">" . __("Équipe") . "</a></li>".
			" <li id=\"smdemande\"><a href=\"" . $notaires_demande_url . "\">" . __("Demande d'information") . "</a></li>".
			"</ul>";


		$nouvelles = getEtudeNouvelles($etude_id);


		return $html;
	}

	function getEtudeEquipe($etude_id) {
		global $DB, $submenumap, $title, $nouvelles;

		$nouvelles = getEtudeNouvelles($etude_id);


		$DB->query(
			"SELECT DISTINCT ".
			"	et.nom AS etude, ".
			"	em.key AS employe_key, ".
			"	em.prenom AS prenom, ".
			"	em.nom AS nom, ".
			"	em.courriel AS courriel, ".
			"	em.description AS description, ".
			"	em.image AS image, ".
			"	em.annee_debut_pratique AS annee_debut_pratique, ".
			"	em.expertises_droit_affaires AS expertises_droit_affaires, ".
			"	em.expertises_droit_personne AS expertises_droit_personne, ".
			"	em.expertises_droit_immobilier AS expertises_droit_immobilier, ".
			"	em.expertises_sectorielles AS expertises_sectorielles, ".
			"	em.succursale AS succursale, ".
			"	fo.abbreviation_m AS fonction, ".
			//"	fo.key AS fonction_key, ".
			"	REPLACE(REPLACE(CONCAT_WS(em.prenom, em.nom, ' '), 'É', 'E'), 'é', 'e') AS fullname ".
			"FROM ".
			"	`employes` AS em, ".
			"	`etudes` AS et, ".
			"	`etudes_succursales` AS et_su, ".
			"	`employes_etudes_succursales` AS em_et_su, ".
			"	`fonctions` AS fo, ".
			"	`employes_fonctions` AS em_fo ".
			"WHERE ".
			"	et.key = '" . $etude_id . "' ".
			"	AND et_su.etudes_key = et.key ".
			"	AND em_et_su.etudes_succursales_key = et_su.key ".
			"	AND em_et_su.employes_key = em.key ".
			"	AND em_fo.employes_key = em.key ".
			"	AND em_fo.fonctions_key = fo.key ".
			"	AND fo.key IN ('8','15') ".
			"	AND et.actif = '1' ".
			"	AND et_su.actif = '1' ".
			"	AND em.actif = '1' ".
			"ORDER BY ".
			"	fullname ASC;"
		);

		$employes = array();

		$html .= "<h5>" . __("Notre <span>Étude</span>") . "</h5>";

		while ($DB->next_record()) {

			$etude = toHtml($DB->getField('etude'));
			$employe_key = toHtml($DB->getField('employe_key'));
			$prenom = toHtml($DB->getField('prenom'));
			$nom = toHtml($DB->getField('nom'));
			$courriel = toHtml($DB->getField('courriel'));
			$notes_biographiques = stripslashes($DB->getField('description'));
			$annee_debut_pratique = toHtml($DB->getField('annee_debut_pratique'));
			$annee_debut_pratique_avocat = toHtml($DB->getField('annee_debut_pratique_avocat'));
			$expertises_droit_affaires = stripslashes($DB->getField('expertises_droit_affaires'));
			$expertises_droit_personne = stripslashes($DB->getField('expertises_droit_personne'));
			$expertises_droit_immobilier = stripslashes($DB->getField('expertises_droit_immobilier'));
			$expertises_sectorielles = stripslashes($DB->getField('expertises_sectorielles'));
			$succursale = toHtml($DB->getField('succursale'));
			$image_file = toHtml($DB->getField('image'));
			$fonction = toHtml($DB->getField('fonction'));




			//$fonction_key = $DB->getField('fonction_key');


			$title =  $etude;

			if ($employe_key != $employe_key_prev) {
				$employes[$employe_key]['id'] = "employe_" . $employe_key;
				$employes[$employe_key]['nom'] = $fonction . " " . $prenom . " " . $nom;
				$employes[$employe_key]['courriel'] = $courriel;
				$employes[$employe_key]['notes_biographiques'] = $notes_biographiques;
				$employes[$employe_key]['image_url'] = EXTRANET_DOCS_EMPLOYES_IMG_URL . $image_file;

				if (!url_exists($employes[$employe_key]['image_url'])) {
					$employes[$employe_key]['image_url'] = preg_replace("/\.jpg/", ".JPG", $employes[$employe_key]['image_url']);
				}


				$employes[$employe_key]['description'] = "";

				if ($courriel != "") {
					$employes[$employe_key]['email'] .= "<div class=\"email\"><a href=\"mailto:" . $courriel . "\" alt=\"Courriel de l'employ&eacute;\">" . $courriel . "</a></div>";
				}

				if ($succursale != "") {
					$employes[$employe_key]['description'] .= "<p><strong>Succursale attitr&eacute;e&nbsp;:</strong> " . $succursale . "</p>";
				}

				$DB->query(
					"SELECT fonctions_key ".
					"FROM `employes_fonctions` ".
					"WHERE employes_key = '" . $employe_key . "' "
				);
				$fonctions = array();
				while ($DB->next_record()) {
					$fonctions[$DB->getField("fonctions_key")] = 1;
				}

				if ($annee_debut_pratique != "" && $fonctions[8] == 1) {
					$employes[$employe_key]['debut_pratique'] .= "<p class=\"debut-pratique\"><strong>Notaire depuis&nbsp;:</strong> " . $annee_debut_pratique . "</p>";
				}



				if ($annee_debut_pratique_avocat != "" && $fonctions[15] == 1) {
					$employes[$employe_key]['debut_pratique'] .= "<p class=\"debut-pratique\"><strong>Avocat(e), conseiller(e) juridique depuis&nbsp;:</strong> " . $annee_debut_pratique_avocat . "</p>";
				} else if ($fonctions[15] == 1) {
					$employes[$employe_key]['debut_pratique'] .= "<p class=\"debut-pratique\"><strong>Avocat(e), conseiller(e) juridique</strong> </p>";
				}

				if ($expertises_droit_affaires != "") {
					$employes[$employe_key]['affaires'] .= "<p class=\"affaires\"><strong>Droit des affaires&nbsp;:</strong><br />" . $expertises_droit_affaires . "</p>";
				}
				if ($expertises_droit_personne != "") {
					$employes[$employe_key]['personnes'] .= "<p class=\"personnes\"><strong>Droit de la personne&nbsp;:</strong><br />" . $expertises_droit_personne . "</p>";
				}
				if ($expertises_droit_immobilier != "") {
					$employes[$employe_key]['immobilier'] .= "<p class=\"immobilier\"><strong>Droit immobilier&nbsp;:</strong><br />" . $expertises_droit_immobilier . "</p>";
				}
				if ($expertises_sectorielles != "") {
					$employes[$employe_key]['sectoriel'] .= "<p class=\"sectoriel\"><strong>Expertises sectorielles&nbsp;:</strong><br />" . $expertises_sectorielles . "</p>";
				}

				$html .=
					"<div class=\"boxEquipeList\"><a href=\"#" . $employes[$employe_key]['id'] . "\">" . $employes[$employe_key]['nom'] . "</a></div>";
			}
			$employe_key_prev = $employe_key;
		}

		if (is_array($employes) && count($employes) > 0) {

			$html .=
				"<div class=\"separator\">&nbsp;</div>";

			foreach ($employes as $employe_key => $info) {


				$html .=
					"<!-- ===================== Description membre ============= -->\n".
					"<div id=\"" . $info['id'] . "\" class=\"equipe01\">\n".
					(preg_match("/\.[a-z]+$/i", $info['image_url']) ? "	<div class=\"imgEquipe\"><img width=\"126\" height=\"180\" src=\"" . $info['image_url'] . "\" alt=\"" . $info['nom'] . "\"></div>\n" : "").
					"	<h3><a name=\"equipe01\"></a>" . $info['nom'] . "</h3>\n".
					"	<h4>Notes biographiques</h4>".
					"	". $info['email'] . "\n".
					"	". $info['debut_pratique'] . "\n".
					"	". $info['affaires'] . "\n".
					"	". $info['personnes'] . "\n".
					"	". $info['immobilier'] . "\n".
					"	". $info['sectoriel'] . "\n";

				if (strlen($info['notes_biographiques']) > 16) {

					if (strpos($info['notes_biographiques'], '<p>') === false) {
						$info['notes_biographiques'] = '<p>' . $info['notes_biographiques'] . '</p>';
					}

					$html .=
						"	<div class=\"box\">\n".
						"		<span class=\"close\">X</span>\n".
						"		<h3><a name=\"equipe01\"></a>" . $info['nom'] . "</h3>\n".
						"		<h4>Notes biographiques</h4>".
						"		<div class=\"bio\">" . $info['notes_biographiques'] . "</div>\n".
						"	</div>\n";
				}

				$html .=
					"<br clear=\"all\" /></div>".
					"<!-- ===================== fin Description membre ============= -->";


			}
		}

		return $html;
	}


	function getEtude($etude_id) {
		global $DB, $submenumap, $title, $nouvelles;

		$DB->query(
			"SELECT e.*, es.* ".
			"FROM etudes e ".
			"INNER JOIN etudes_succursales es ON e.key = es.etudes_key ".
			"WHERE e.nombre_notaires_associes > 0 ".
			"AND e.key = '" . $etude_id . "' ".
			"AND es.siege_social = '1' ".
			"AND es.actif = '1' ".
			"AND e.actif = '1' ".
			"ORDER BY nom ASC"
		);

		$etude_url = "";
		$equipe_url = "";
		$notaires_demande_url = "";
		$html = "";
		$description = "";

		while ($DB->next_record()) {
			if (trim($etude_url . $equipe_url . $notaires_demande_url) == "") {
				$etude_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom"));
				$equipe_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "/equipe";
				$notaires_demande_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "/demande-information";
 				$html .= "<h5>" . __("Notre <span>Étude</span>") . "</h5>";
			}

			$etude = toHtml($DB->getField('nom'));
			$title = $etude;
			$adresse = toHtml($DB->getField('adresse'));
			$ville = toHtml($DB->getField('ville'));
			$province = toHtml($DB->getField('province'));
			$code_postal = toHtml($DB->getField('code_postal'));
			$telephone1 = toHtml(str_replace(array("(", ")"), "", $DB->getField('telephone1')));
			$telephone2 = toHtml(str_replace(array("(", ")"), "", $DB->getField('telephone2')));
			$telecopieur = toHtml(str_replace(array("(", ")"), "", $DB->getField('telecopieur')));
			$courriel = toHtml($DB->getField('courriel'));

			$html .=
				"<div class=\"address\">\n".
				"	<p class=\"address\">".
						$adresse . "<br />".
						$ville . " (" . $province . ")&nbsp;&nbsp;" . $code_postal . "<br />".
						"<a href=\"mailto:" . $courriel . "\">" . $courriel . "</a>".
				"	</p>";

			if ($telephone1 != "" || $telephone2 != "") {
				$html .=
					"	<div class=\"tel\">\n".
					"		<h4>" . __("Téléphone") . "</h4>".
					"		<p>";
				if ($telephone1 != "") $html .= $telephone1 . "<br/>";
				if ($telephone2 != "") $html .= $telephone2 . "<br/>";
				$html .=
					"		</p>\n".
					"	</div>\n";
			}

			if ($telecopieur != "") {
				$html .=
					"	<div class=\"tel\">\n".
					"		<h4>" . __("Télécopieur") . "</h4>\n".
					"		<p>" . $telecopieur . "</p>\n".
					"	</div>\n";
			}

			$html .= "<br clear=\"all\" /></div>";

			$etude_description = stripslashes($DB->getField('description'));
			$image_file = toHtml($DB->getField('image'));
			$image_description_1 = toHtml($DB->getField('image_description_1'));
			$image_description_2 = toHtml($DB->getField('image_description_2'));
			$image_description_3 = toHtml($DB->getField('image_description_3'));
			$image_description = "";

			if ($image_description_1 != "") {
				$image_description .= "<p><strong>Premi&egrave;re rang&eacute;e&nbsp;:</strong><br />" . $image_description_1 . "</p>";
			}
			if ($image_description_2 != "") {
				$image_description .= "<p><strong>Deuxi&egrave;me rang&eacute;e&nbsp;:</strong><br />" . $image_description_2 . "</p>";
			}
			if ($image_description_3 != "") {
				$image_description .= "<p><strong>Troisi&egrave;me rang&eacute;e&nbsp;:</strong><br />" . $image_description_3 . "</p>";
			}

			if ($etude_description != "" || $image_file != "") {
				if ($image_file != "") {
					$image_url = EXTRANET_DOCS_ETUDES_IMG_URL . $image_file;

					if (!url_exists($image_url)) {
						$image_url = preg_replace("/\.jpg/", ".JPG", $image_url);
					}


					$description .= "<div class=\"photo\"><img src=\"" . $image_url . "\" width=\"296\" alt=\"Photo de l'&eacute;quipe\" /></div>";
				}
				$description .= "<div class=\"photo_equipe\">" . $image_description . "</div>";
				$description .= "<br clear=\"all\" />";
				if ($etude_description != "") {
					$description .=	"	<div class=\"description\">" . $etude_description . "</div>";
				}
			}
		}

		$DB->query(
			"SELECT e.*, es.* ".
			"FROM etudes e ".
			"INNER JOIN etudes_succursales es ON e.key = es.etudes_key ".
			"WHERE e.nombre_notaires_associes > 0 ".
			"AND e.key = '" . $etude_id . "' ".
			"AND es.siege_social = '0' ".
			"AND es.actif = '1' ".
			"ORDER BY nom ASC"
		);

		$html .= "<div class=\"autres-adresses\">";
		if ($DB->getNumRows() > 0) {
			$html .= "<h2>" . __("Autres <span>Adresses</span>") . "</h2>";
		}

		while ($DB->next_record()) {
			$adresse = toHtml($DB->getField('adresse'));
			$ville = toHtml($DB->getField('ville'));
			$province = toHtml($DB->getField('province'));
			$code_postal = toHtml($DB->getField('code_postal'));
			$telephone1 = toHtml(str_replace(array("(", ")"), "", $DB->getField('telephone1')));
			$telephone2 = toHtml(str_replace(array("(", ")"), "", $DB->getField('telephone2')));
			$telecopieur = toHtml(str_replace(array("(", ")"), "", $DB->getField('telecopieur')));
			$courriel = toHtml($DB->getField('courriel'));

			$html .=
				"<div class=\"address\">\n".
				"	<p class=\"address\">" . $adresse . "<br />" . $ville . " (" . $province . ")&nbsp;&nbsp;" . $code_postal . "<br/>";

			if ($courriel != "") $html .= "<a href=\"mailto:" . $courriel . "\">" . $courriel . "</a>";
			$html .= "	</p>";

			$html .= "	<div class=\"phone\">\n";
			if ($telephone1 != "" || $telephone2 != "") {
				$html .=
					"		<h4 class=\"tel\">" . __("Téléphone") . "</h4>".
					"		<p class=\"tel\">";
				if ($telephone1 != "") $html .= $telephone1 . "<br/>";
				if ($telephone2 != "") $html .= $telephone2 . "<br/>";
				if ($telephone1 == "" && $telephone2 == "") $html .= __("non disponible") . "<br/>";
				$html .= "		</p>";
			}

			$html .=
				"	</div>\n".
				"	<div class=\"fax\">\n";

			if ($telecopieur != "") {
				$html .=
					"			<h4 class=\"tel\">" . __("Télécopieur") . "</h4>\n".
					"			<p class=\"fax\">" . $telecopieur . "</p>";
			}
			$html .=
				"	</div>".
				"</div>\n";
		}

		$html .= "</div>";

		$html .= $description;

		$submenumap =
			"<ul class=\"navigationEtude\">".
			" <li id=\"smetude\"><a href=\"" . $etude_url . "\">" . __("Étude") . "</a></li>".
			" <li id=\"smequipe\"><a href=\"" . $equipe_url . "\">" . __("Équipe") . "</a></li>".
			" <li id=\"smdemande\"><a href=\"" . $notaires_demande_url . "\">" . __("Demande d'information") . "</a></li>".
			"</ul>";


		$nouvelles = getEtudeNouvelles($etude_id);

		return $html;
	}


	function getEtudeNouvelles($etude_id) {
		global $DB, $submenumap, $title, $nouvelles, $videos, $videos_ss;

		$DB->query(
			"SELECT e.*, es.* ".
			"FROM etudes e ".
			"INNER JOIN etudes_succursales es ON e.key = es.etudes_key ".
			"WHERE e.nombre_notaires_associes > 0 ".
			"AND e.key = '" . $etude_id . "' ".
			"AND es.siege_social = '1' ".
			"AND es.actif = '1' ".
			"AND e.actif = '1' ".
			"ORDER BY nom ASC"
		);

		$nouvelles_array = array();
		while ($DB->next_record()) {
			if (trim($etude_url . $equipe_url . $notaires_demande_url) == "") {
				$etude_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom"));
				$equipe_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "/equipe";
				$notaires_demande_url = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "/demande-information";
			}

			for ($n = 1; $n <= NB_NOUVELLES; $n++) {
				if ($DB->getField('nouvelle_' . $n . '_visible')) {
					$nouvelles_array[$n]['titre'] = $DB->getField('nouvelle_' . $n . '_titre');
					$nouvelles_array[$n]['doc'] = $DB->getField('nouvelle_' . $n . '_doc');
					$nouvelles_array[$n]['video'] = $DB->getField('nouvelle_' . $n . '_video');
				}
			}
		}

		if ($_GET["p"] == "equipe") {
			if ($_SERVER["REQUEST_URI"] != $equipe_url) {
				header("Location:". $equipe_url, TRUE, 301);
			}
		} else if ($_GET["p"] != "") {

		} else if ($_GET["e"] != "") {
			if ($_SERVER["REQUEST_URI"] != $etude_url) {
				header("Location:". $etude_url, TRUE, 301);
			}
		}

		$submenumap =
			"<ul class=\"navigationEtude\">".
			" <li id=\"smetude\"><a href=\"" . $etude_url . "\">" . __("Étude") . "</a></li>".
			" <li id=\"smequipe\"><a href=\"" . $equipe_url . "\">" . __("Équipe") . "</a></li>".
			" <li id=\"smdemande\"><a href=\"" . $notaires_demande_url . "\">" . __("Demande d'information") . "</a></li>".
			"</ul>";

		$videos = array();

		if (count($nouvelles_array) > 0) {
			$nouvelles .= "<ul>";
			foreach ($nouvelles_array as $nb => $data) {

				$nouvelle_url = EXTRANET_DOCS_ETUDES_NOUVELLES_URL . $etude_id . "/" . $data['doc'];

				if (preg_match("/(http:\/\/)?(www\.[a-z0-9\-]+\.[a-z]+)/",$data['titre'])) {
					$nouvelle_url = preg_replace("/.*?(http:\/\/)?(www\.[a-z0-9\-]+\.[a-z]+).*?/", "http://$2", $data['titre']);
					$data['doc'] = ".";
				}

				if (trim($data['video']) != "") {
					$nouvelles .= "";


					if (trim($data['doc']) != "") {
						$videos[] = toHtml($data['titre']) . "<br /><iframe width=\"298\" height=\"168\" src=\"https://www.youtube.com/embed/" . preg_replace("/^.*?(v=|\.be\/)([^\&]+).*?$/", "$2", $data["video"]) . "\" frameborder=\"0\" allowfullscreen></iframe><br />\n".
							"<a href=\"" . $nouvelle_url . "\" target=\"_blank\" style=\"color: #5c777f;\">Notes PDF</a>\n";
					} else {
						$videos[] = toHtml($data['titre']) . "<br /><iframe width=\"298\" height=\"168\" src=\"https://www.youtube.com/embed/" . preg_replace("/^.*?(v=|\.be\/)([^\&]+).*?$/", "$2", $data["video"]) . "\" frameborder=\"0\" allowfullscreen></iframe>\n";
					}
				} else if (trim($data['doc']) != "") {
					$nouvelles .= "<li><a href=\"" . $nouvelle_url . "\" target=\"_blank\">" . toHtml($data['titre']) . "</a></li>";

				} else {
					$nouvelles .= "<li>" . toHtml($data['titre']) . "</li>";
				}
			}
			$nouvelles .= "</ul>";
		}


		$DB->query(
			"SELECT e.* ".
			"FROM etudes e ".
			"WHERE e.key = '46' "
		);

		$nouvelles_array = array();
		while ($DB->next_record()) {
			for ($n = 1; $n <= NB_NOUVELLES; $n++) {
				if ($DB->getField('nouvelle_' . $n . '_visible')) {
					$nouvelles_array[$n]['titre'] = $DB->getField('nouvelle_' . $n . '_titre');
					$nouvelles_array[$n]['doc'] = $DB->getField('nouvelle_' . $n . '_doc');
					$nouvelles_array[$n]['video'] = $DB->getField('nouvelle_' . $n . '_video');
				}
			}
		}

		$videos_ss = array();

		if (count($nouvelles_array) > 0) {
			foreach ($nouvelles_array as $nb => $data) {

				$nouvelle_url = EXTRANET_DOCS_ETUDES_NOUVELLES_URL . "46" . "/" . $data['doc'];

				if (preg_match("/(http:\/\/)?(www\.[a-z0-9\-]+\.[a-z]+)/",$data['titre'])) {
					$nouvelle_url = preg_replace("/.*?(http:\/\/)?(www\.[a-z0-9\-]+\.[a-z]+).*?/", "http://$2", $data['titre']);
					$data['doc'] = ".";
				}

				if (trim($data['video']) != "") {
					//$videos_ss[] = toHtml($data['titre']) . "<br /><iframe width=\"298\" height=\"168\" src=\"https://www.youtube.com/embed/" . preg_replace("/^.*?(v=|\.be\/)([^\&]+).*?$/", "$2", $data["video"]) . "\" frameborder=\"0\" allowfullscreen></iframe>\n";

					if (trim($data['doc']) != "") {
						$videos_ss[] = toHtml($data['titre']) . "<br /><iframe width=\"298\" height=\"168\" src=\"https://www.youtube.com/embed/" . preg_replace("/^.*?(v=|\.be\/)([^\&]+).*?$/", "$2", $data["video"]) . "\" frameborder=\"0\" allowfullscreen></iframe><br />\n".
							"<a href=\"" . $nouvelle_url . "\" target=\"_blank\" style=\"color: #5c777f;\">Notes PDF</a>\n";
					} else {
						$videos_ss[] = toHtml($data['titre']) . "<br /><iframe width=\"298\" height=\"168\" src=\"https://www.youtube.com/embed/" . preg_replace("/^.*?(v=|\.be\/)([^\&]+).*?$/", "$2", $data["video"]) . "\" frameborder=\"0\" allowfullscreen></iframe>\n";
					}

				}
			}
		}


		return $nouvelles;

	}

	function getEtudeIDByURL($url) {
		global $DB;

		$DB->query(
			"SELECT e.* ".
			"FROM etudes e ".
			"WHERE e.actif > 0 ".
			"ORDER BY nom ASC"
		);

		$key = "";
		while ($DB->next_record()) {
			if ($url == asciionly($DB->getField("nom"))) {
				$key = $DB->getField("key");
			}
			echo "<!--" . $url . " != " . asciionly($DB->getField("nom")) . " = " . $key . "-->\n";
		}


		return $key;
	}

	function getEtudesList() {
		global $DB, $_CUSTOM_TITLE;

		$DB->query(
			"SELECT e.*, es.ville ".
			"FROM etudes e ".
			"INNER JOIN etudes_succursales es ON es.etudes_key = e.key ".
			"WHERE e.nombre_notaires_associes > 0 ".
			"AND e.actif = 1 ".
			"ORDER BY nom ASC"
		);

		$etudes = array();
		$html = "<ul class=\"etude-list\">";
		while ($DB->next_record()) {
			if (!isset($etudes["e" . $DB->getField("key")])) {
				$html .= "<li><a href=\"" . __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "\">" . $DB->getField("nom") . "</a></li>";
				$etudes["e" . $DB->getField("key")] = 1;
			}
		}
		$html .= "</ul>";

		return $html;
	}

	function getEtudesMap($page = "") {
		global $DB, $_CUSTOM_TITLE;

		$DB->query(
			"SELECT e.*, es.ville ".
			"FROM etudes e ".
			"INNER JOIN etudes_succursales es ON es.etudes_key = e.key ".
			"WHERE e.nombre_notaires_associes > 0 ".
			"AND es.actif = '1' ".
			"AND e.actif = '1' ".
			"ORDER BY nom ASC"
		);

		$etudes = array();
		$html = "<ul class=\"etude-list\">";
		while ($DB->next_record()) {
			if (!isset($etudes["e" . $DB->getField("key")])) {
				$html .= "<li><a href=\"" . __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom")) . "" . ($page == "rv" ? "/rendez-vous" : ($page == "di" ? "/demande-information" : "")) . "\">" . $DB->getField("nom") . "</a></li>";
				$etudes["e" . $DB->getField("key")] = 1;
			}
		}
		$html .= "</ul>";

		$cities = split(" - ", getCityList("", $page));

		$html =
			"<div class=\"col_city\">\n".
			"	<ul>";
		foreach ($cities AS $city) {
			$html .= "<li>" . $city . "</li>";
		}
		$html .= "</ul></div>";

		$coords = getCoordsArray();

		$map = "";
		foreach ($coords AS $x => $yarray) {
			foreach ($yarray AS $y => $etude) {
				if ($etude['visible'] == 1) {
					$map .=
						"<div class=\"etude\" style=\"position: absolute; top: " . $y . "px; left:" . $x . "px;\">\n".
						"	<a href=\"" . __("/notaires-") . asciionly($coords[$x][$y]['ville']) . "/" . asciionly($coords[$x][$y]['nom']) . "" . ($page == "rv" ? "/rendez-vous" : ($page == "di" ? "/demande-information" : "")) . "\"><img class=\"marker\" src=\"/wp-content/themes/pmeinter/img/target-map.png\" alt=\"\" /></a>\n".
						"	<div class=\"info\">\n".
						"		<h4>" . str_replace("&#039;", "\'", stripslashes($coords[$x][$y]['nom'])) . "</h4>\n".
						"		<p>" . str_replace("&#039;", "\'", stripslashes($coords[$x][$y]['info'])) . "</p>\n".
						"		<p><a href=\"" . __("/notaires-") . asciionly($coords[$x][$y]['ville']) . "/" . asciionly($coords[$x][$y]['nom']) . "" . ($page == "rv" ? "/rendez-vous" : ($page == "di" ? "/demande-information" : "")) . "\">" . stripslashes($coords[$x][$y]['nom']) . "</a></p>".
						"	</div>\n".
						"</div>";
				}
			}
		}

		$html .=
			"<div class=\"map\">\n".
			"<form action=\"" . __("/notaires") . "\" method=\"post\">\n".
			"	<h3>Recherche par nom (&Eacute;tude ou notaire)</h3>".
			"	<input type=\"text\" id=\"sidescity\" name=\"scity\" value=\"\" />\n".
			"	<input type=\"submit\" id=\"mapsubmit\" value=\"OK\" />\n".
			"</form>\n".
			"<img class=\"map\" src=\"/wp-content/themes/pmeinter/img/map.jpg\" alt=\"Carte du Quebec\" />" . $map . "</div>";

		return $html;
	}


	function getEtudesByCity($city) {
		global $DB, $_CUSTOM_TITLE, $title, $nouvelles;

		$DB->query(
			"SELECT e.*, es.* ".
			"FROM etudes_succursales es INNER JOIN etudes e ON es.etudes_key = e.key ".
			"WHERE e.nombre_notaires_associes > 0 ".
			"AND e.actif = '1' ".
			//"AND es.siege_social = '1' ".
			"AND es.actif = '1' "
		);

		$etudes = array();
		$rows = $DB->getNumRows();

		$nb_etudes = 0;
		$lien_etude = "";
		$liens = array();
		$html = "<ul class=\"etude-list\">";
		while ($DB->next_record()) {

			if (addslashes(strtolower(asciionly($city))) == asciionly($DB->getField("ville"))) {
				if ($rows == 1) {
					$_CUSTOM_TITLE = $DB->getField('ville') . " " . $DB->getField('nom') . " ";
					$title = $DB->getField("ville");
				} else {
					$_CUSTOM_TITLE = $DB->getField('ville') . " ";
					$title = $DB->getField("ville");
				}

				if (!isset($etudes["e" . $DB->getField("key")])) {
					$lien_etude = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom"));

					if ($_GET["p"] == "di") {
						$lien_etude .= "#contact";//"/demande-information";
					} else if ($_GET["p"] == "rv") {
						$lien_etude .= "#contact";//"/rendez-vous";
					}

					$html .= "<li><a href=\"" . $lien_etude . "\">" . $DB->getField("nom") . "</a> <p class=\"adresse\">" . stripcslashes($DB->getField("adresse")) . ", " . $DB->getField("ville") . "</p></li>";
					$etudes["e" . $DB->getField("key")] = 1;
					$nb_etudes++;

					$liens[$lien_etude]++;
				}
			} else if (strtolower(asciionly($city)) != 'laval' && preg_match("/" . addslashes(strtolower(asciionly($city))) . "\s*.*?$/i", asciionly($DB->getField("ville")))) {
				if ($rows == 1) {
					$_CUSTOM_TITLE = $DB->getField('ville') . " " . $DB->getField('nom') . " ";
					$title = $DB->getField("ville");
				} else {
					$_CUSTOM_TITLE = $DB->getField('ville') . " ";
					$title = $DB->getField("ville");
				}

				if (!isset($etudes["e" . $DB->getField("key")])) {
					$lien_etude = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("nom"));

					if ($_GET["p"] == "di") {
						$lien_etude .= "#contact";//"/demande-information";
					} else if ($_GET["p"] == "rv") {
						$lien_etude .= "#contact";//"/rendez-vous";
					}

					$html .= "<li><a href=\"" . $lien_etude . "\">" . $DB->getField("nom") . "</a> <p class=\"adresse\">" . stripcslashes($DB->getField("adresse")) . ", " . $DB->getField("ville") . "</p></li>";
					$etudes["e" . $DB->getField("key")] = 1;
					$nb_etudes++;

					$liens[$lien_etude]++;
				}
			}


		}
		$html .= "</ul>";

		$keys = array_keys($liens);
		if ($nb_etudes == 1 || count($liens) == 1 || (count($liens) == 2 && preg_match("/gatineau/is", $keys[0]))) {
			header("Location: " . $lien_etude);
			exit;
		}

		return $html;
	}


	function getEtudesByKeyword($keyword) {
		global $NORMALIZED_SORTING_TABLE_UTF8, $DB, $_CUSTOM_TITLE, $title, $nouvelles;

		$_CUSTOM_TITLE = "Recherche : " . stripslashes(stripslashes($_POST["cp"]) . " " . $_POST["scity"]);
		$title = "Recherche : " . stripslashes(stripslashes($_POST["cp"]) . " " . $_POST["scity"]);

		if ($_POST["cp"] != "") {
			$keyword .= " " . $_POST["cp"];
		}


		$normalized_search_string = strtolower(strtr(trim(stripslashes($keyword)), $NORMALIZED_SORTING_TABLE_UTF8));
		$normalized_search_terms = explode(' ', $normalized_search_string);


		if (strlen($normalized_search_string) > 0) {

			$DB->query(
				"SELECT DISTINCT ".
				"	em.key AS employe_key, ".
				"	em.nom AS employe_nom, ".
				"	em.prenom AS employe_prenom, ".
				"	em.description AS employe_description, ".
				"	em.courriel AS employe_courriel, ".
				"	em.expertises_droit_affaires AS employe_droit_affaires, ".
				"	em.expertises_droit_personne AS employe_droit_personne, ".
				"	em.expertises_droit_immobilier AS employe_droit_immobilier, ".
				"	em.expertises_sectorielles AS employe_expertises_sectorielles, ".
				"	et.key AS etude_key, ".
				"	et.nom AS etude_nom, ".
				"	et.description AS etude_description ".
				"FROM ".
				"	`employes` AS em, ".
				"	`etudes` AS et, ".
				"	`etudes_succursales` AS et_su, ".
				"	`employes_etudes_succursales` AS em_et_su, ".
				"	`fonctions` AS fo, ".
				"	`employes_fonctions` AS em_fo ".
				"WHERE ".
				"	em.actif = '1' ".
				"	AND et_su.etudes_key = et.key ".
				"	AND em_et_su.etudes_succursales_key = et_su.key ".
				"	AND em_et_su.employes_key = em.key ".
				"	AND em_fo.employes_key = em.key ".
				"	AND em_fo.fonctions_key = fo.key ".
				"	AND fo.key IN ('8','15') ".
				"	AND et.actif = '1' ".
				"	AND et_su.actif = '1' ".
				"ORDER BY ".
				"	em.nom ASC;"
			);

			$result = array();
			$i = 0;
			while ($record = $DB->next_record()) {
				$result[$i]['employe_key'] = $DB->getField('employe_key');
				$result[$i]['employe_nom'] = $DB->getField('employe_nom');
				$result[$i]['employe_description'] = $DB->getField('employe_description');
				$result[$i]['employe_prenom'] = $DB->getField('employe_prenom');
				$result[$i]['employe_courriel'] = $DB->getField('employe_courriel');
				$result[$i]['employe_droit_affaires'] = $DB->getField('employe_droit_affaires');
				$result[$i]['employe_droit_personne'] = $DB->getField('employe_droit_personne');
				$result[$i]['employe_droit_immobilier'] = $DB->getField('employe_droit_immobilier');
				$result[$i]['employe_expertises_sectorielles'] = $DB->getField('employe_expertises_sectorielles');
				$result[$i]['etude_key'] = $DB->getField('etude_key');
				$result[$i]['etude_nom'] = $DB->getField('etude_nom');
				$result[$i]['etude_description'] = $DB->getField('etude_description');
				$result[$i]['etude_ville'] = '';
				$result[$i]['etude_villes'] = '';

				$DB->query(
					"SELECT ".
					"	et_su.ville AS etude_ville, ".
					"	et_su.siege_social AS siege_social ".
					"FROM ".
					"	`etudes_succursales` AS et_su ".
					"WHERE ".
					"	et_su.etudes_key = '" . $result[$i]['etude_key'] . "' AND et_su.actif = '1' ".
					"ORDER BY et_su.ville;"
				);

				while ($record2 = $DB->next_record()) {
					if ($DB->getField('siege_social') == 1) {
						$result[$i]['etude_ville'] = $DB->getField('etude_ville');
					}
					if (!preg_match("/" . $DB->getField('etude_ville') . "/", $result[$i]['etude_villes'])) {
						$result[$i]['etude_villes'] .= $DB->getField('etude_ville') . ', ';
					}

				}

				$i++;
			}

			if (is_array($result) && count($result) > 0) {

				$HTML .= "<table id=\"searchresult\" cellspacing=\"0\" cellpadding=\"0\">\n";

				$done = array();
				foreach ($result as $i => &$info) {

					for($i = 0; $i < count($normalized_search_terms); $i++) {
						$term = $normalized_search_terms[$i];
						$normalized_result_notaire = strtolower(strtr(stripslashes(($info['employe_prenom'] . " " . $info['employe_nom'])), $NORMALIZED_SORTING_TABLE_UTF8));
						$normalized_result_etude = strtolower(strtr(stripslashes(($info['etude_nom'])), $NORMALIZED_SORTING_TABLE_UTF8));
						$normalized_result_ville = strtolower(strtr(stripslashes(($info['etude_ville'])), $NORMALIZED_SORTING_TABLE_UTF8));
						$normalized_result_villes = strtolower(strtr(stripslashes(($info['etude_villes'])), $NORMALIZED_SORTING_TABLE_UTF8));

						$normalized_result_eda = strtolower(strtr(stripslashes(($info['employe_droit_affaires'])), $NORMALIZED_SORTING_TABLE_UTF8));
						$normalized_result_edp = strtolower(strtr(stripslashes(($info['employe_droit_personne'])), $NORMALIZED_SORTING_TABLE_UTF8));
						$normalized_result_edi = strtolower(strtr(stripslashes(($info['employe_droit_immobilier'])), $NORMALIZED_SORTING_TABLE_UTF8));
						$normalized_result_ees = strtolower(strtr(stripslashes(($info['employe_expertises_sectorielles'])), $NORMALIZED_SORTING_TABLE_UTF8));
						//$normalized_result_eda = strtolower(strtr(stripslashes(($info['employe_droit_affaires'])), $NORMALIZED_SORTING_TABLE_UTF8));

						$normalized_result_etude_desc = strtolower(strtr(stripslashes(($info['etude_description'])), $NORMALIZED_SORTING_TABLE_UTF8));
						$normalized_result_employe_desc = strtolower(strtr(stripslashes(($info['employe_description'])), $NORMALIZED_SORTING_TABLE_UTF8));

						if (!in_array($info['employe_key'], $done)) {

							if (
								strpos($normalized_result_notaire, $term) !== false
								|| strpos($normalized_result_etude, $term) !== false
								|| strpos($normalized_result_ville, $term) !== false
								|| strpos($normalized_result_villes, $term) !== false


								|| strpos($normalized_result_eda, $term) !== false
								|| strpos($normalized_result_edp, $term) !== false
								|| strpos($normalized_result_edi, $term) !== false
								|| strpos($normalized_result_ees, $term) !== false

								|| strpos($normalized_result_etude_desc, $term) !== false
								|| strpos($normalized_result_employe_desc, $term) !== false


								|| (strpos($normalized_search_string, 'affaire') !== false && trim($info['employe_droit_affaires']) !== '')
								|| (strpos($normalized_search_string, 'personne') !== false && trim($info['employe_droit_personne']) !== '')
								|| (strpos($normalized_search_string, 'immobilie') !== false && trim($info['employe_droit_immobilier']) !== '')
								|| (strpos($normalized_search_string, 'sectoriel') !== false && trim($info['employe_expertises_sectorielles']) !== '')
							) {

								if ($info['etude_key'] == 24) {
									//$ville = "Boucherville, Longueuil, Montr&eacute;al";
									$ville = toHtml(preg_replace("/\,\s+$/", "", $info['etude_villes']));
								} else {
									//$ville = toHtml($info['etude_ville']);
									$ville = toHtml(preg_replace("/\,\s+$/", "", $info['etude_villes']));
								}

								$lien_etude = __("/notaires-") . asciionly($info['etude_ville']) . "/" . asciionly($info['etude_nom']);

								if (($i+1) == count($normalized_search_terms)) {

									$HTML .= "<tr>\n";
									$HTML .= "<td width=\"200px\"><a href=\"" . $lien_etude . "/equipe#employe_" . $info['employe_key'] . "\">Me " . toHtml($info['employe_nom']) . ", " . toHtml($info['employe_prenom']) . "</a><br />&nbsp;</td>\n";
									$HTML .= "<td><a href=\"" . $lien_etude . "\">" . toHtml($info['etude_nom']) . "</a><br />" . $ville . "</td>";
									$HTML .= "</tr>";

									$nb_etudes++;

									$done[] = $info['employe_key'];
								}

							} else {
								$done[] = $info['employe_key'];
							}
						}
					}
				}

				$HTML .= "</table>\n";

			}

			if (count($done) == 0) {
				$HTML .= "<span style=\"margin-left:12px;\">Aucun r&eacute;sultat.</span>\n";
			}

		} else {
			$HTML .= "<span style=\"margin-left:12px;\">Aucun r&eacute;sultat.</span>\n";
		}



/*


		$normalized_search_string = strtolower(strtr(trim(stripslashes($keyword)), $NORMALIZED_SORTING_TABLE_UTF8));
		$query = $normalized_search_string;

		//$normalized_search_string_array = explode(" ", $normalized_search_string);

		$nb_etudes = 0;
		$lien_etude = "";
		$resultats = array();

		if (strlen($normalized_search_string) > 0) {

			$DB->query(
				"SELECT DISTINCT ".
				"	em.key AS employe_key, ".
				"	em.nom AS employe_nom, ".
				"	em.prenom AS employe_prenom, ".
				"	em.courriel AS employe_courriel, ".
				"	em.expertises_droit_affaires AS employe_droit_affaires, ".
				"	em.expertises_droit_personne AS employe_droit_personne, ".
				"	em.expertises_droit_immobilier AS employe_droit_immobilier, ".
				"	em.expertises_sectorielles AS employe_expertises_sectorielles, ".
				"	et.key AS etude_key, ".
				"	et.nom AS etude_nom, ".
				"	et_su.ville, ".
				"	et_su.adresse ".
				"FROM ".
				"	`employes` AS em, ".
				"	`etudes` AS et, ".
				"	`etudes_succursales` AS et_su, ".
				"	`employes_etudes_succursales` AS em_et_su, ".
				"	`fonctions` AS fo, ".
				"	`employes_fonctions` AS em_fo ".
				"WHERE ".
				"	em.actif = '1' ".
				"	AND et_su.etudes_key = et.key ".
				"	AND em_et_su.etudes_succursales_key = et_su.key ".
				"	AND em_et_su.employes_key = em.key ".
				"	AND em_fo.employes_key = em.key ".
				"	AND em_fo.fonctions_key = fo.key ".
				"	AND fo.key = '8' ".
				"	AND et.actif = '1' ".
				"	AND et_su.actif = '1' ".
				"	AND et_su.actif = '1' ".
				"ORDER BY ".
				"	em.nom ASC;"
			);

			$address = array();
			$html = "";
			while ($record = $DB->next_record()) {
				//$normalized_result_notaire = strtolower(strtr(stripslashes(utf8_decode($DB->getField('employe_prenom') . " " . $DB->getField('employe_nom'))), $NORMALIZED_SORTING_TABLE_UTF8));
				//$normalized_result_etude = strtolower(strtr(stripslashes(utf8_decode($DB->getField('etude_nom'))), $NORMALIZED_SORTING_TABLE_UTF8));
				//$normalized_result_ville = strtolower(strtr(stripslashes(utf8_decode($DB->getField('etude_ville'))), $NORMALIZED_SORTING_TABLE_UTF8));
				//$normalized_result_villes = strtolower(strtr(stripslashes(utf8_decode($DB->getField('etude_villes'))), $NORMALIZED_SORTING_TABLE_UTF8));


				$content = "";
				$content .= $DB->getField('employe_key');
				$content .= " " . $DB->getField('employe_nom');
				$content .= " " . $DB->getField('employe_prenom');
				$content .= " " . $DB->getField('employe_courriel');
				$content .= " " . $DB->getField('employe_droit_affaires');
				$content .= " " . $DB->getField('employe_droit_personne');
				$content .= " " . $DB->getField('employe_droit_immobilier');
				$content .= " " . $DB->getField('employe_expertises_sectorielles');
				$content .= " " . $DB->getField('ville');
				$content .= " " . $DB->getField('adresse');
				$content .= " " . $DB->getField('etude_nom');

				$normalized_result_notaire = strtolower(strtr(trim(stripslashes($content)), $NORMALIZED_SORTING_TABLE_UTF8));

				//foreach ($normalized_search_string_array as $query) {

					//echo $query . "<br />";

					if (strpos($normalized_result_notaire, $query) !== false) {

						$resultats[$query][$DB->getField("ville").$DB->getField("adresse")] = 1;
						if ($address[$DB->getField("ville").$DB->getField("adresse")] != 1) {
							$lien_etude = __("/notaires-") . asciionly($DB->getField("ville")) . "/" . asciionly($DB->getField("etude_nom"));
							$html .= "<ul class=\"etude-list\">";
							$html .= "<li><a href=\"" . $lien_etude . "\">" . $DB->getField("etude_nom") . "</a> <p class=\"adresse\">" . $DB->getField("adresse") . ", " . $DB->getField("ville") . "</p></li>";
							$html .= "</ul>";
							$nb_etudes++;
							$address[$DB->getField("ville").$DB->getField("adresse")] = 1;
						}
					}
				//}
			}
		}
*/
		if ($nb_etudes == 1) {
			header("Location: ". $lien_etude);
			exit;
		}

		return $HTML;
	}



	function getCoordsArray() {
		global $DB;

		$coordsArray = array();

		$DB->query(
			"SELECT e.nom AS nom, em.coord_x AS coord_x, em.coord_y AS coord_y, em.info AS info, es.ville ".
			"FROM `etudes` AS e ".
			"INNER JOIN `etudes_map` AS em ON em.etudes_key = e.key ".
			"INNER JOIN `etudes_succursales` AS es ON es.etudes_key = e.key ".
			"WHERE e.actif = '1' ".
			"AND em.visible = '1' ".
			"ORDER BY e.key ASC;"
		);

		while ($record = $DB->next_record()) {
			$x = ceil($DB->getField('coord_x') * 12.1);
			$y = ceil($DB->getField('coord_y') * 12.8);
			$nom = $DB->getField('nom');
			$info = $DB->getField('info');
			$ville = $DB->getField('ville');

			$coordsArray[$x][$y]['visible'] = 1;
			$coordsArray[$x][$y]['nom'] = $nom;
			$coordsArray[$x][$y]['info'] = $info;
			$coordsArray[$x][$y]['ville'] = $ville;
		}

		return $coordsArray;
	}

	function url_exists($url) {
		$file_headers = @get_headers($url);

		if(preg_match("/404 /", $file_headers[0])){
		      return false;
		} else if (preg_match("/302 /", $file_headers[0]) && preg_match("/404 /", $file_headers[7])) {
		    return false;
		} else {
		    return true;
		}
	}
?>
