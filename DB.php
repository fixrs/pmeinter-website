<?PHP

	/*
		Copyright (C) 2004 Jean-Philippe Ricard, QuiboWeb Inc.
		WWW.QUIBOWEB.CA

		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	*/

	$str['eng']['error_query'] = "The query has failed : ";
	$str['eng']['error_connect'] = "Error connecting to the database : ";
	$str['eng']['error_select_db'] = "Unable to select specified database : ";
	$str['eng']['error_close'] = "Error closing database link : ";

	/**
     *
     * @version 1.1
     * @author Jean-Philippe Ricard <ricardjp@quiboweb.ca>
	 * @package	Quibo
     */
	class DB {

		var $str; // The array used to display messages
		var $db; // The DB link
		var $dbname; // The DB Name
		var $result = array(); // The result set returned by the last query
		var $row = array(); // The actual working row of the result set

		function DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS = "MySQLi") {
			global $str;
			$this->str = $str['eng'];
			$this->dbname = $DB_NAME;

			if ($DBMS == "MySQLi") {
				$this->db = new MySQL_i($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME);
			} else {
				$this->db = new MySQL($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME);
			}
		}

		function getField($field) {
			$last_result = count($this->db->result)-1;
			if ($last_result < 0) { $last_result = 0; }

			$last_row = count($this->db->row[$last_result])-1;
			if ($last_row < 0) { $last_row = 0; }

			return $this->db->row[$last_result][$last_row][$field];
			//return addslashes($this->db->row[$last_result][$last_row][$field]);
		}

		function reset() {
			if (strtolower(gettype($this->result)) == "array") {
				array_pop($this->result);
			}

			if (strtolower(gettype($this->row)) == "array") {
				array_pop($this->row);
			}

			if (strtolower(gettype($this->db->result)) == "array") {
				array_pop($this->db->result);
			}

			if (strtolower(gettype($this->db->row)) == "array") {
				array_pop($this->db->row);
			}
		}

		/* These functions MUST be overrided by children class */

		function query($query) {
			return $this->db->query($query);
		}

		function next_record() {
			return $this->db->next_record();
		}

		function getRow() {
			$last_result = count($this->db->result)-1;
			if ($last_result < 0) { $last_result = 0; }
			$last_row = count($this->db->row[$last_result])-1;
			if ($last_row < 0) { $last_row = 0; }

			return $this->db->row[$last_result][$last_row];
		}

		function getNumRows() {
			return $this->db->getNumRows();
		}

		function num_rows() {
			return $this->db->num_rows();
		}

		function getInsertId() {
			return $this->db->getInsertId();
		}

		function getQueryInfo() {
			return $this->db->getQueryInfo();
		}

		function num_fields() {
			return $this->db->num_fields();
		}

		function getFieldName($i) {
			return $this->db->getFieldName($i);
		}

		function getFieldDesc($table) {
			return mysql_query("DESCRIBE `" . $table . "`");
		}

		function close() {
			return $this->db->close();
		}

	}

	/**
     *
     * @version 1.1
     * @author Jean-Philippe Ricard <ricardjp@quiboweb.ca>
	 * @package	Quibo
     */
	class MySQL extends DB {

		function MySQL($dbhost, $dbuser, $dbpass, $dbname) {
			$this->str = $GLOBALS['str']['eng'];

			$this->db = mysql_connect($dbhost, $dbuser, $dbpass);// or die($this->str['error_connect'] . mysql_error($this->db));
			mysql_select_db($dbname, $this->db) or die($this->str['error_select_db'] . mysql_error($this->db));
			return $this->db;
		}

		function query($query) {
			@$this->result[count($this->result)] = mysql_query($query, $this->db);

			if (!$this->result[count($this->result)-1]) {
				@die($this->str["error_query"] . mysql_error($this->db));
			}

			return $this->result[count($this->result)-1];
		}

		function close() {
			parent::reset();
			if (!mysql_close($this->db)) {
				die($this->str['error_close'] . mysql_error($this->db));
			}
		}

		function next_record() {
			$last_result = count($this->result)-1;
			if ($last_result < 0) { return FALSE; }

			$last_row = count($this->row[$last_result])-1;

			$this->row[$last_result][$last_row + 1] = mysql_fetch_array($this->result[$last_result]);

			if (!$this->row[$last_result][$last_row + 1]) {
				parent::reset();
				return FALSE;
			} else {
				return $this->row[$last_result][$last_row + 1];
			}
		}

		function getNumRows() {
			return mysql_num_rows($this->result[count($this->result)-1]);
		}

		function getNumFields() {
			return mysql_num_fields($this->result[count($this->result)-1]);
		}

		function getInsertId() {
			return mysql_insert_id($this->db);
		}

		function getQueryInfo() {
			return mysql_info($this->db);
		}

		function getFieldName($i) {
			return mysql_field_name($this->result[count($this->result)-1], $i);
		}

		function getFieldDesc($field) {
			return mysql_query("DESCRIBE `" . $field . "`");
		}

		function getAffectedRows() {
			return mysql_affected_rows($this->db);
		}

		function getFieldsList($table) {
			return mysql_query("SHOW COLUMNS FROM `" . $table . "`");
		}

		function getTableList($like) {
			return mysql_query("SHOW TABLES FROM `" . $this->dbname . "` LIKE " . $like);
		}

	}

	/**
     *
     * @version 1.1
     * @author Francois Viens <viensf@quiboweb.ca>
	 * @package	Quibo
     */
	class MySQL_i extends DB {

		function MySQL_i($dbhost, $dbuser, $dbpass, $dbname) {
			$this->str = $GLOBALS['str']['eng'];

			$this->db = mysqli_connect($dbhost, $dbuser, $dbpass);// or die($this->str['error_connect'] . mysql_error($this->db));
			mysqli_select_db($this->db, $dbname) or die($this->str['error_select_db'] . mysqli_connect_error() . " " . mysqli_connect_errno());
			return $this->db;
		}


		// function query($query) {
		// 	@$this->result[count($this->result)] = mysqli_query($query, $this->db);

		// 	if (!$this->result[count($this->result)-1]) {
		// 		@die($this->str["error_query"] . mysqli_error($this->db));
		// 	}

		// 	return $this->result[count($this->result)-1];
		// }

		public function query($query) {

	        $nextResult = count($this->result);
	        $this->result[$nextResult] = mysqli_query($this->db, $query);
	        $this->qq = $query;
	        if (!$this->result[count($this->result) - 1]) {
	            throw new DatabaseException('Query Error: ' . mysqli_error($this->db), mysqli_errno($this->db));
	        }


	        return $this->result[count($this->result) - 1];
	    }

	    public function insert($query) {
	        mysqli_query($this->db, $query);
	    }

	    public function update($query) {
	        mysqli_query($this->db, $query);
	    }



		function close() {
			parent::reset();
			if (!mysqli_close($this->db)) {
				die($this->str['error_close'] . mysqli_error($this->db));
			}
		}

		function next_record() {
			// $last_result = count($this->result)-1;
			// if ($last_result < 0) { return FALSE; }

			// $last_row = count($this->row[$last_result])-1;

			// $this->row[$last_result][$last_row + 1] = mysqli_fetch_array($this->result[$last_result]);

			// if (!$this->row[$last_result][$last_row + 1]) {
			// 	parent::reset();
			// 	return FALSE;
			// } else {
			// 	return $this->row[$last_result][$last_row + 1];
			// }

			$lastResult = count($this->result) - 1;

	        if ($lastResult < 0) {
	            $this->reset();
	            return null;
	        }

	        $lastRow = -1;
	        if (isset($this->row[$lastResult])) {
	            $lastRow += count($this->row[$lastResult]);
	        }

	        if( $this->result[$lastResult] instanceof \mysqli_result) {
	            $this->row[$lastResult][$lastRow + 1] = mysqli_fetch_array($this->result[$lastResult]);
	        }

	        if (!$this->row[$lastResult][$lastRow + 1]) {
	            $this->reset();
	            return null;
	        }

	        return $this->row[$lastResult][$lastRow + 1];
		}

		function getNumRows() {
			return mysqli_num_rows($this->result[count($this->result)-1]);
		}

		function getNumFields() {
			//return mysqli_field_count($this->db);
			$lastResult = count($this->result) - 1;
        	return mysqli_num_fields($this->result[$lastResult]);
		}

		function getInsertId() {
			return mysqli_insert_id($this->db);
		}

		function getQueryInfo() {
			return mysqli_info($this->db);
		}

		function getFieldName($i) {
			return mysqli_fetch_field_direct($this->result[count($this->result)-1], $i);
		}

		function getFieldDesc($field) {
			return mysqli_query("DESCRIBE `" . $field . "`");
		}

		function getAffectedRows() {
			return mysqli_affected_rows($this->db);
		}

		function getFieldsList($table) {
			return mysqli_query("SHOW COLUMNS FROM `" . $table . "`");
		}

		function getTableList($like) {
			return mysqli_query("SHOW TABLES FROM `" . $this->dbname . "` LIKE " . $like);
		}

		public function escape($value) {
	        return mysqli_real_escape_string($this->db, $value);
	    }

	    public function toInt($value) {
	        if (is_null($value)) {
	            return null;
	        }
	        return intval($value);
	    }
	    /**
	     * Convert an integer to a valid SQL value
	     * @param integer
	     * @return string
	     */
	    public function fromInt($value) {
	        if (is_null($value)) {
	            return 'NULL';
	        }
	        return "'" . $this->escape(intval($value)) . "'";
	    }

	    /**
	     * Convert the field value to a float
	     * @param string
	     * @return float|null
	     */
	    public function toFloat($value) {
	        if (is_null($value)) {
	            return null;
	        }
	        return floatval($value);
	    }
	    /**
	     * Convert a float to a valid SQL value
	     * @param float
	     * @return string
	     */
	    public function fromFloat($value) {
	        if (is_null($value)) {
	            return 'NULL';
	        }
	        return "'" . $this->escape(floatval($value)) . "'";
	    }

	    /**
	     * Convert the field value to a string
	     * @param string
	     * @return string|null
	     */
	    public function toString($value) {
	        if (is_null($value)) {
	            return null;
	        }
	        return $value;
	    }
	    /**
	     * Convert a string to a valid SQL value
	     * @param string
	     * @return string
	     */
	    public function fromString($value) {
	        if (is_null($value)) {
	            return 'NULL';
	        }
	        return "'" . $this->escape($value) . "'";
	    }

	    /**
	     * Convert the field value to an integer
	     * @param string
	     * @return integer|null
	     */

	    /**
	     * Convert the field value to an Unix Timestamp
	     * @param string
	     * @return integer|null
	     */
	    public function toTimestamp($value) {
	        if (is_null($value)) {
	            return null;
	        }
	        return strtotime($value);
	    }
	    /**
	     * Convert an Unix Timestamp to a valid SQL value
	     * @param integer
	     * @return string
	     */
	    public function fromTimestamp($value) {
	        if (is_null($value)) {
	            return 'NULL';
	        }
	        return "'" . $this->escape(date('Y-m-d H:i:s', $value)) . "'";
	    }

	    /**
	     * Convert the field value to a boolean
	     * @param string
	     * @return boolean|null
	     */
	    public function toBoolean($value) {
	        if (is_null($value)) {
	            return null;
	        }
	        settype($value, 'boolean');
	        return $value;
	    }
	    /**
	     * Convert a boolean to a valid SQL value
	     * @param boolean
	     * @return string
	     */
	    public function fromBoolean($value) {
	        if (is_null($value)) {
	            return 'NULL';
	        }
	        return ($value) ? 'TRUE' : 'FALSE';
	    }

	}


