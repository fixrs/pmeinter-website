
(function($,Edge,compId){var Composition=Edge.Composition,Symbol=Edge.Symbol;
//Edge symbol: 'stage'
(function(symbolName){Symbol.bindElementAction(compId,symbolName,"${_Replay}","click",function(sym,e){if(!sym.isPlaying()){sym.play(0);}});
//Edge binding end
Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",35683,function(sym,e){if(!sym.isPlaying()){sym.play(0);}});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_playsFR}","click",function(sym,e){sym.$("soundon").hide();sym.play();sym.play();});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_soundon}","click",function(sym,e){sym.$("gingle2")[0].muted=!sym.$("gingle2")[0].muted;sym.$("soundon").hide();sym.$("sound").show();});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_sound}","click",function(sym,e){sym.$("gingle2")[0].muted=!sym.$("gingle2")[0].muted;sym.$("sound").hide();sym.$("soundon").show();});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_playsEN}","click",function(sym,e){sym.$("Frame1_txt1").html("What can we <br />wish you <br />best for");sym.$("Frame1_txt2").html("the holiday season?");sym.$("Frame2_txt1").html("WE HOPE <br />TO SEE YOU <br />AND YOUR <br />LOVED ONES <br />BLESSED <br />WITH... ");sym.$("Frame3_txt1").html("... SUCCESS");sym.$("Frame3_txt2").html("... GOOD HEALTH");sym.$("Frame3_txt3").html("... AND HAPPINESS");sym.$("Frame4_txt1").html("We are thankful <br />to be worthy <br />of your confidence.");sym.$("Frame4_txt2").html("PLEASE KNOW <br />THAT WE APPRECIATE IT <br />AND TREASURE IT.");sym.$("Replay").html("Replay");$(sym.lookupSelector("Frm04_Seq04")).css('background-image','url(images/Frm04_Seq04_en.png)')
sym.$("soundon").hide();sym.play();});
//Edge binding end
})("stage");
//Edge symbol end:'stage'

//=========================================================

//Edge symbol: 'Preloader'
(function(symbolName){})("Preloader");
//Edge symbol end:'Preloader'
})(jQuery,AdobeEdge,"EDGE-3364329");